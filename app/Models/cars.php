<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cars extends Model
{
    use HasFactory;
    public function getNameAttribute($value){
        return $this->{'name_'.app()->getLocale()};
    }
    public function checkCarBelongToOffice(){
        return $this->hasMany('App\Models\rentalCars','id','car');
    }
    public function countryObj(){
        return $this->hasOne('\App\Models\cat_country','id','country');
    }
     public function brandObj(){
        return $this->hasOne('\App\Models\cat_brand','id','brand');
    }
    public function categoryObj(){
        return $this->hasOne('\App\Models\cat_category','id','category');
    }
     public function colorObj(){
        return $this->hasOne('\App\Models\carsColors','id','color');
    }
    
}
