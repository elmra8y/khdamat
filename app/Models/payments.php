<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class payments extends Model
{
    use HasFactory;
     public function rentalRequestObj(){
        return $this->hasOne('\App\Models\rentalBookRequests','id','request');
    }
     public function roadHelpRequestObj(){
        return $this->hasOne('\App\Models\roadHelpRequests','id','request');
    }
     public function carWashRequestObj(){
        return $this->hasOne('\App\Models\carWashRequests','id','request');
    }
      public function garageRequestObj(){
        return $this->hasOne('\App\Models\garage_Requests','id','request');
    }
        public function sparePartRequestObj(){
        return $this->hasOne('\App\Models\sparePart_requests','id','request');
    }
}
