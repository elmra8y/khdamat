<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rentalBookRequests extends Model
{
    use HasFactory;
     public function rentalCarObj(){
        return $this->hasOne('\App\Models\rentalCars','id','car');
    }
     public function userObj(){
        return $this->hasOne('\App\Models\User','id','user');
    }
     public function officeObj(){
        return $this->hasOne('\App\Models\rentalUsers','id','office');
    }
}
