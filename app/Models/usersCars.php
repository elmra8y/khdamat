<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class usersCars extends Model
{
    use HasFactory;
     public function carObj(){
        return $this->hasOne('\App\Models\cars','id','car');
    }
}
