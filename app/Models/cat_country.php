<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cat_country extends Model
{
    use HasFactory;
    protected $table = 'cat_country';
    public function getNameAttribute($value){
        return $this->{'name_'.app()->getLocale()};
    }
     public function brands(){
        return $this->hasMany('\App\Models\cat_brand','country','id');
    }
}
