<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cat_brand extends Model
{
    use HasFactory;
    public function getNameAttribute($value){
        return $this->{'name_'.app()->getLocale()};
    }
    public function categories(){
        return $this->hasMany('\App\Models\cat_category','brand','id');
    }
}
