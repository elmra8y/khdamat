<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class garage_request_messages extends Model
{
    use HasFactory;
    public function adminAsParentObj(){
        return $this->hasOne('\App\Models\admins','id','parent');
    }
    public function adminAsChildObj(){
        return $this->hasOne('\App\Models\admins','id','child');
    }
    public function userAsParentObj(){
        return $this->hasOne('\App\Models\User','id','parent');
    }
    public function userAsChildObj(){
        return $this->hasOne('\App\Models\User','id','child');
    }
}
