<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rentalCars extends Model
{
    use HasFactory;
    public function carObj(){
        return $this->hasOne('\App\Models\cars','id','car');
    }
    public function bookings(){
        return $this->hasMany('\App\Models\rentalBookRequests','id','car');
    }
      public function countryObj(){
        return $this->hasOne('\App\Models\cat_country','id','country');
    }
     public function brandObj(){
        return $this->hasOne('\App\Models\cat_brand','id','brand');
    }
    public function categoryObj(){
        return $this->hasOne('\App\Models\cat_category','id','category');
    }
     public function officeObj(){
        return $this->hasOne('\App\Models\rentalUsers','id','office');
    }
     public function colorObj(){
        return $this->hasOne('\App\Models\carsColors','id','color');
    }
    
}
