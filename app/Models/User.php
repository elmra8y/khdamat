<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Hash;
class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function Bookings(){
        return $this->hasMany('\App\Models\rentalBookRequests','user','id');
    }
     public function usersCars(){
        return $this->hasMany('\App\Models\usersCars','user','id');
    }
     public function roadHelpRequests(){
        return $this->hasMany('\App\Models\roadHelpRequests','user','id');
    }
     public function carwashRequests(){
        return $this->hasMany('\App\Models\carwashRequests','user','id');
    }
      public function garageRequests(){
        return $this->hasMany('\App\Models\garage_Requests','user','id');
    }
     public function sparePartRequests(){
        return $this->hasMany('\App\Models\sparePart_requests','user','id');
    }
     public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function setPasswordAttribute($password)
    {
        if ( !empty($password) ) {
            $this->attributes['password'] = Hash::make($password);
        }
    }
    
    
}
