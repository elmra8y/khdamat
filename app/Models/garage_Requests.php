<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class garage_Requests extends Model
{
    use HasFactory;
     public function userObj(){
        return $this->hasOne('\App\Models\User','id','user');
    }
     public function carObj(){
        return $this->hasOne('\App\Models\usersCars','id','car');
    }
    public function serviceObj(){
        return $this->hasOne('\App\Models\garage_services','id','service');
    }
}
