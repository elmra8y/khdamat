<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sparePart_requests extends Model
{
    use HasFactory;
       public function userObj(){
        return $this->hasOne('\App\Models\User','id','user');
    }
     public function carObj(){
        return $this->hasOne('\App\Models\usersCars','id','car');
    }
    public function serviceObj(){
        return $this->hasOne('\App\Models\garage_services','id','service');
    }
    public function cancellation(){
        return $this->hasOne('\App\Models\cancelled_requests','request_id','id');
    }
    
}
