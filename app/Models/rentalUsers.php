<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class rentalUsers extends Authenticatable
{
    use HasFactory;
     public function getNameAttribute($value){
        return $this->{'name_'.app()->getLocale()};
    }
     public function getTermsAttribute($value){
        return $this->{'terms_conditions_'.app()->getLocale()};
    }
}
