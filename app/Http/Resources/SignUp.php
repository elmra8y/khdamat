<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SignUp extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    
    public function toArray($request)
    {
         return [
            'error_flag' => 0,
            'message' => 'success',
            'result'=> [
                'access_token' => auth()->guard('api')->login($this->resource),
                'token_type'   => 'bearer',
                'expires_in'   => auth()->guard('api')->factory()->getTTL() * 60
            ]
        ];
    }
}
