<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
class UserSignInRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            'password' => 'required|min:3',
        ];
    }
    public function failedValidation( Validator $validator ) {
        throw new HttpResponseException( response()->json( [
             'error_flag'    => 1,
             'message'       => $validator->errors()->first(),
             'result'        => NULL,
                ] , 422 ) );
    }
     public function messages()
    {
        return [
            
        ];
    }
}
