<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class rentalAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!auth()->guard('rentals')->check()){
            return redirect()->route('admin.rental.login');
        }
        return $next($request);
    }
}
