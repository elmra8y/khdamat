<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserSignUpRequest;
use Illuminate\Http\Request;
use App\Http\Resources\SignUp as SignUpResource;
use App\Models\User;
use Hash;
class userSignUpController extends Controller
{
    public function index(UserSignUpRequest $request){
        $validated = $request->validated();
        $object = ['name','email','phone','password'];
        $createUser = User::create($request->all($object));
        return new SignUpResource($createUser);
    }
}
