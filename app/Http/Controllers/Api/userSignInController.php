<?php

namespace App\Http\Controllers\Api;
use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserSignInRequest;
use App\Http\Resources\SignIn as SignInResource;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
class userSignInController extends Controller
{
     public function index(UserSignInRequest $request){
        $validated = $request->validated();
        $credentials = $request->only('email', 'password');
        $jwt_token = null;
         $user = auth()->guard('api')->attempt($credentials);
         if (!$user) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return new SignInResource([
            'access_token' => $user,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->guard('api')->factory()->getTTL() * 60
        ]);
    }
}
