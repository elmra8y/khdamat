<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\cars;
use App\Models\rentalCars;
use App\Models\rentalBookRequests;
use App\Models\payments;
use App\Models\usersCars;
use App\Models\roadHelpServices;
use App\Models\carWashServices;
use App\Models\garage_services;
use App\Models\User;
use App\Models\roadHelpRequests;
use App\Models\carWashRequests;
use App\Models\garage_Requests;
use App\Models\garage_request_messages;
use App\Models\sparePart_requests;
use Illuminate\Support\Str;
use Validator;
use Hash;
use \Carbon\Carbon;
use Route;
class front extends Controller
{
    public function office($id)
    {
      $cars = rentalCars::all();
      return view('office', compact('cars'));
    }

    
    public function searchCar(Request $request){
        $country = $request->get('country');
        $brand = $request->get('brand');
        $category = $request->get('category');
        $year = $request->get('year');
        $color = $request->get('color');
        $where = array();
        if($country){
            array_push($where,['country',$country]);
        }
        if($brand){
            array_push($where,['brand',$brand]);
        }
        if($category){
            array_push($where,['category',$category]);
        }
        if($year){
            array_push($where,['year',$year]);
        }
        if($color){
            array_push($where,['color',$color]);
        }
        $countries = cars::get()->unique('country');
        $brands = cars::get()->unique('brand');
        $deafultCars = rentalCars::with('carObj')->with('countryObj')->with('categoryObj')->with('brandObj');
        if(count($where) > 0){
          $deafultCars = $deafultCars->where($where)->paginate(9); 
        }
        else{
          $deafultCars = $deafultCars->paginate(9);
        }
        if($request->ajax()){
             return response()->json(['status' => 'done','data'=> $deafultCars],200);
        }else{
            return view('rental.searchCar',compact('countries','brands','deafultCars'));
        }
    }
    public function singleCar(Request $request,$id){
         $car = rentalCars::find($id);
        if(!$car){return abort(404);}
         $deafultCars = rentalCars::with('carObj')->with('sizeObj')->with('categoryObj')->with('brandObj')->with('groupObj')->where('office',$car->office)->whereNotIn('id',[$id])->paginate(9);
         if($request->ajax()){
             return response()->json(['status' => 'done','data'=> $deafultCars],200);
        }else{
            return view('rental.singleCar',compact('car','deafultCars'));
        }
         
    }
      public function bookCarStepOne(Request $request,$id){
          //return $request->all();
           $messages = [
           'car.required' => __('front.rental.booking.car_required'),
           'car.exists' => __('front.rental.booking.car_exists'),
           'from.required' => __('front.rental.booking.from_required'),
           'from.date_format' => __('front.rental.booking.from_date_format'),
           'from.after_or_equal' => __('front.rental.booking.from_after_or_equal'),
           'to.required' => __('front.rental.booking.to_required'),
           'to.date_format' => __('front.rental.booking.to_date_format'),
           'to.after' => __('front.rental.booking.to_after'),
           'bookingTimeDelivery.required' => __('front.rental.booking.bookingTimeDelivery_required'),
           'bookingTimeReturn.required' => __('front.rental.booking.bookingTimeReturn_required'),
             
         ];
         $request['car'] = $id;
         $rules = [
             'car' => 'required|exists:rental_cars,id',
             'from' => 'required|date_format:Y-m-d|after_or_equal:'.Carbon::now()->format('Y-m-d'),
             'to' => 'required|date_format:Y-m-d|after:from',
             'bookingTimeDelivery' => 'required',
             'bookingTimeReturn' => 'required',
             'lat1' => 'nullable',
             'lng1' => 'nullable',
             'lat2' => 'nullable',
             'lng2' => 'nullable',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
          if(!auth()->guard('users')->check()){
              return back()->with('error',__('sign.pleaseLoginFirst'));
          }
           $check_rentalBookRequest = rentalBookRequests::where(['user' => auth()->guard('users')->user()->id , 'office' => rentalCars::find($request->car)->office ,'car' => $request->car])->first();
          if($check_rentalBookRequest){
             $new_request =  $check_rentalBookRequest; 
             return view('rental.bookFirstStep',compact('new_request'));
          }
          $fromDate = Carbon::parse($request->from);
          $toDate = Carbon::parse($request->to);
          $diffrenceDate = $fromDate->diffInDays($toDate);
          $new_request = new rentalBookRequests();
          $new_request->request_type = 'rental';
          $new_request->request_date_from = $request->from;
          $new_request->request_date_to = $request->to;
          $new_request->request_datetime_start = $request->bookingTimeDelivery;
          $new_request->request_datetime_back = $request->bookingTimeReturn;
//          $new_request->delivery_address = $request->bookingDeliveryAddress;
//          $new_request->back_delivery_address = $request->bookingreturnCarAddress;
          $new_request->delivery_status = 'init';
          $new_request->delivery_address_lng = $request->lng1;
          $new_request->delivery_address_lat = $request->lat1;
          $new_request->back_delivery_address_lng = $request->lng2;
          $new_request->back_delivery_address_lat = $request->lat2;
          $new_request->car = $request->car;
          $new_request->days = $diffrenceDate;
          $new_request->user = auth()->guard('users')->user()->id;
          $new_request->office = rentalCars::find($request->car)->office;
          $new_request->save();
         return view('rental.bookFirstStep',compact('new_request'));
    }
    public function bookCarStepTwo($id){
       $requestReview = rentalBookRequests::find($id);
        if(!$requestReview){
            return abort(404);    
        }
          if($requestReview->delivery_status != 'paid'){
            $requestReview->delivery_status = 'review';
            $requestReview->save();
            }
            $new_request = $requestReview;
            return view('rental.bookFirstStep',compact('new_request'));
    }
     public function bookCarlastStep($id){
       $requestReview = rentalBookRequests::find($id);
        if(!$requestReview){
            return abort(404);    
        }
            $new_request = $requestReview;
            return view('rental.bookFirstStep',compact('new_request'));
    }
    
    public function bookCarStepThree($id){
             $requestConfirm = rentalBookRequests::find($id);
        if(!$requestConfirm){
            return abort(404);    
        }
            $requestConfirm->delivery_status = 'confirm';
            $requestConfirm->save();
            $new_request = $requestConfirm;
          return view('rental.bookFirstStep',compact('new_request'));
    }
    public function bookCarConfirmandPay(Request $request,$id){
         $rentalBookRequest = rentalBookRequests::find($id);
         if(!$rentalBookRequest){return abort(404);}
         $rentalBookRequestCheckRelatedUser = rentalBookRequests::where(['id' => $id , 'user' => auth()->guard('users')->user()->id])->first();
         if(!$rentalBookRequestCheckRelatedUser){return abort(403);}
          $messages = [
              'paymentType.required' => __('front.roadhelp.paymentType_required') ,
              'paymentType.in' => __('front.roadhelp.paymentType_in') ,
             
         ];
         $rules = [
             'paymentType' => 'required|in:keynet,cash',
             
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withErrors($validator);
         }
         if($request->paymentType == 'cash'){
             $rentalBookRequest->delivery_status = 'cash'; //mean this request confirmed by cash , 3 mean paid by kenet
             $rentalBookRequest->save();
             $new_rentalBookRequestCash = new payments();
             $new_rentalBookRequestCash->request = $rentalBookRequest->id;
             $new_rentalBookRequestCash->status = 'CREATED';
             $new_rentalBookRequestCash->type = 'rental';
             $new_rentalBookRequestCash->invoice = 'cash';
             $new_rentalBookRequestCash->save();
             return redirect()->route('rental.book.success',$rentalBookRequest->id);
         }
            $newRentalPayment = new payments();
            $newRentalPayment->request =  $rentalBookRequest->id;
            $newRentalPayment->status =  'CREATED';
            $newRentalPayment->type =  'rental';
            $newRentalPayment->save();
            $fullPrice = $rentalBookRequest->days * $rentalBookRequest->rentalCarObj->price;
            $data = array("desc" => 'desc' , 'paymentID' => $newRentalPayment->id , 'name' => auth()->guard('users')->user()->name , 'price' => $fullPrice,'email' => auth()->guard('users')->user()->email);
            $res =  $this->initialPayment($data);
            $newRentalPayment->invoice = $res['id'];
            $newRentalPayment->save();
            return redirect($res['url']);
         
     }
    
    
    public function signup(){
        return view('sign.register');
    }
    public function Dosignup(Request $request){
         $messages = [
           
             
         ];
         $rules = [
             'name' => 'required|required',
             'email' => 'required|unique:users,email',
             'phone' => 'required|unique:users,phone',
             'password' => 'required|string|min:8',
           
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
        $new_User = new User();
        $new_User->name = $request->name;
        $new_User->email = $request->email;
        $new_User->phone = $request->phone;
        $new_User->password = $request->password;
        $new_User->save();
        auth()->guard('users')->login($new_User);
        return redirect()->route('myaccount');
        if(session()->has('previous_route')){
            $route = session()->get('previous_route');
            session()->forget('previous_route');
                  return redirect()->route($route);
              }
        return back()->with('message',__('sign.accountCreated',['signNow' => '<a href="'.route('login.show').'">'.__('sign.loginNow').'</a>']));
    }
       public function login(){
        return view('sign.login');
    }
    public function Dologin(Request $request){
         $messages = [
           
             
         ];
         $rules = [
             
             'email' => 'required|exists:users,email',
             'password' => 'required|string',
           
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
        auth()->guard('users')->attempt(['email'=>$request->email , 'password' => $request->password]);
          if(auth()->guard('users')->check()){
              if(session()->has('previous_route')){
            $route = session()->get('previous_route');
            session()->forget('previous_route');
                  return redirect()->route($route);
              }
              return redirect()->route('myaccount');
          }else{
            return back()->with('error',__('sign.passwordNotcorrect'));
          }
    }
    
    protected function initialPayment($data){
      
            $curl = curl_init();
            $carbon_after_10 = Carbon::now()->addDays(10)->timestamp;
            $carbon_after_20 = Carbon::now()->addDays(12)->timestamp;
            curl_setopt_array($curl, array(
              CURLOPT_URL => env('Payment_url'),
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS =>'{
          "draft": false,
          "due": '.$carbon_after_10.'000,
          "expiry": '.$carbon_after_20.'000,
          "description": "test invoice",
          "mode": "INVOICE",
          "note": "test note",
          "notifications": {
            "channels": [
              "SMS",
              "EMAIL"
            ],
            "dispatch": true
          },
          "currencies": [
            "KWD"
          ],
          "metadata": {
            "paymentID": "'.$data['price'].'"
          },
          "customer": {
            "email": "'.$data['email'].'",
            "first_name": "'.$data['name'].'"
          },
          "order": {
            "amount": '.$data['price'].',
            "currency": "KWD",
            "items": [
              {
                "amount": '.$data['price'].',
                "currency": "KWD",
                "description": "test",
                "image": "",
                "name": "'.$data['desc'].'",
                "quantity": 1
              }
            ]
          },
          "payment_methods": [
            ""
          ],
          "post": {
            "url": "'.route('ipnListener').'"
          },
          "redirect": {
            "url": "'.route('verifyPayment').'"
          },
          "reference": {
            "invoice": "INV_00001",
            "order": "ORD_00001"
          }
          }',
              CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . env('TAP_KEY'),
                "content-type: application/json"
              ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {

               $json = json_decode($response, true); 
                return $json;

            }
    }
    public function verifyPayment(Request $request){
            $messages = [
            'tap_id.required' => __('booking.tap_idRequired'),
            'tap_id.exists' => __('booking.tap_idexists'),
            
        ];
        $validator = Validator::make($request->all(),[
            'tap_id' => 'required|exists:payments,invoice',
        ],$messages);
        if($validator->fails()){
             return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
        }
        $res = $this->verifyInvoice($request->tap_id);
        if($res['status'] == 'PAID'){
          $updatePayment = payments::where(['invoice' => $request->tap_id])->first();
          $updatePayment->status = 'PAID';    
          $updatePayment->save();
            if($updatePayment->type == 'rental'){
                $updatePayment->rentalRequestObj->delivery_status = 'paid';
                $updatePayment->rentalRequestObj->save();
                return redirect()->route('rental.book.success',$updatePayment->rentalRequestObj->id);
            }
            if($updatePayment->type == 'roadhelp'){
                $updatePayment->roadHelpRequestObj->status = 3;
                $updatePayment->roadHelpRequestObj->save();
                return redirect()->route('roadhelp.request.success',$updatePayment->roadHelpRequestObj->id);
            }
            if($updatePayment->type == 'carwash'){
                $updatePayment->carWashRequestObj->status = 3;
                $updatePayment->carWashRequestObj->save();
                return redirect()->route('carwash.request.success',$updatePayment->carWashRequestObj->id);
            }
             if($updatePayment->type == 'garage'){
                $updatePayment->garageRequestObj->status = 4;
                $updatePayment->garageRequestObj->save();
                return redirect()->route('garage.request.success',$updatePayment->garageRequestObj->id);
            }
             if($updatePayment->type == 'spare_parts'){
                $updatePayment->sparePartRequestObj->status = 3;
                $updatePayment->sparePartRequestObj->save();
                return redirect()->route('Spare_parts.request.success',$updatePayment->sparePartRequestObj->id);
            }
            
        }
    }
    public function ipnListener(Request $request){
            $newRentalPayment = new payments();
            $newRentalPayment->request =  1;
            $newRentalPayment->status =  serialize($request);
            $newRentalPayment->type =  'rental';
            $newRentalPayment->save();
        }
    
    protected function verifyInvoice($invoice){
         $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => env('Payment_url') .'/'.$invoice,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "{}",
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer " . env('TAP_KEY')
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
             $json = json_decode($response, true); 
            return $json;
        }
    }
    public function myaccount(){
        return view('profile.account');
    }
     public function myaccountChangePassword(){
        return view('profile.changepassword');
    }
    
    public function updateAccount(Request $request){
        $messages = [
           
             
         ];
         $rules = [
             'name' => 'required|required',
             'email' => 'required|unique:users,email,' . auth()->guard('users')->user()->id,
             'phone' => 'required|unique:users,phone,' . auth()->guard('users')->user()->id,
           
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
        
        auth()->guard('users')->user()->name = $request->name;
        auth()->guard('users')->user()->email = $request->email;
        auth()->guard('users')->user()->phone = $request->phone;
        auth()->guard('users')->user()->save();
        return back()->with('message',__('sign.accountUpdated'));
    }
    public function myaccountChangePasswordupdate(Request $request){
            $messages = [
                'confirm.same' => __('sign.newpasswordnotsame'),
                'current.min' => __('sign.currentpasswordmin'),
                'new.min' => __('sign.newpasswordmin'),
                'confirm.min' => __('sign.confirmpasswordmin'),
                'confirm.required' => __('sign.confirm_required'),
                'new.required' => __('sign.new_required'),
                'current.required' => __('sign.current_required'),
            
        ];
        $validator = Validator::make($request->all(),[
            'current' => ['required','min:8',function ($attribute, $value, $fail) {
            if (!Hash::check($value,auth()->guard('users')->user()->password)) {
                $fail(__('profile.currentPasswordNotCorrect'));
            }
        }],
            'new' => 'required|min:8',
            'confirm' => 'required|same:new|min:8',
        ],$messages);
        if($validator->fails()){
            return back()->withErrors($validator);
        }else{
            auth()->guard('users')->user()->password  = $request->confirm;
            auth()->guard('users')->user()->save();
            return back()->with('message',__('profile.passwordChengedSuccess'));
        }
     }
     public function bookedRentalCars(){
        return view('profile.bookedRentalCars');
    }
     public function logout(){
        auth()->guard('users')->logout();
        return redirect('/');
    }
     public function mycars(){
      return view('profile.mycars');
    }
     public function profileaddCar(){
        $sizes = cars::get()->unique('size');
        $brands = cars::get()->unique('brand');
        return view('profile.addCar',compact('sizes','brands'));
    }
    
    public function profilegetBrands(Request $request){
           if($request->ajax()){
             $messages = [
             
         ];
         $rules = [
             'country' => 'required|exists:cat_country,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
            $brands = cars::with('brandObj')->where(['country' => $request->country])->distinct()->get()->unique('brand');
            return response()->json(['status' => 'done' , 'brands' => $brands],200);
        }
    }
    public function profilegetCats(Request $request){
           if($request->ajax()){
             $messages = [
             'size.required' => __('profile.addCar.size_required') ,
             'size.exists' => __('profile.addCar.size_exists') ,
             'brand.required' => __('profile.addCar.brand_required') ,
             'brand.exists' => __('profile.addCar.brand_exists') ,
             
         ];
         $rules = [
             'brand' => 'required|exists:cat_brands,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
            $categories = cars::with('categoryObj')->where(['brand' => $request->brand])->distinct()->get()->unique('category');
            return response()->json(['status' => 'done' , 'categories' => $categories],200);
        }
    }
    public function profilegetGroups(Request $request){
          if($request->ajax()){
             $messages = [
             'size.required' => __('profile.addCar.size_required') ,
             'size.exists' => __('profile.addCar.size_exists') ,
             'brand.required' => __('profile.addCar.brand_required') ,
             'brand.exists' => __('profile.addCar.brand_exists') ,
             'category.required' => __('profile.addCar.category_required') ,
             'category.exists' => __('profile.addCar.category_exists') ,
             
         ];
         $rules = [
             'size' => 'required|exists:cat_sizes,id',
             'brand' => 'required|exists:cat_brands,id',
             'category' => 'required|exists:cat_categories,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
            $groups = cars::with('groupObj')->where(['size' => $request->size , 'brand' => $request->brand , 'category' => $request->category])->get()->unique('group');
            return response()->json(['status' => 'done' , 'groups' => $groups],200);
        }
    }
      public function profilegetYears(Request $request){
          if($request->ajax()){
             $messages = [
             'size.required' => __('profile.addCar.size_required') ,
             'size.exists' => __('profile.addCar.size_exists') ,
             'brand.required' => __('profile.addCar.brand_required') ,
             'brand.exists' => __('profile.addCar.brand_exists') ,
             'category.required' => __('profile.addCar.category_required') ,
             'category.exists' => __('profile.addCar.category_exists') ,
             'group.required' => __('profile.addCar.group_required') ,
             'group.exists' => __('profile.addCar.group_exists') ,
             
         ];
         $rules = [
             'size' => 'required|exists:cat_sizes,id',
             'brand' => 'required|exists:cat_brands,id',
             'category' => 'required|exists:cat_categories,id',
             'group' => 'required|exists:cat_groups,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
            $years = cars::where(['size' => $request->size , 'brand' => $request->brand , 'category' => $request->category , 'group' => $request->group])->get()->unique('year');
            return response()->json(['status' => 'done' , 'years' => $years],200);
        }
    }
    public function profilegetCars(Request $request){
          if($request->ajax()){
             $messages = [
             'size.required' => __('profile.addCar.size_required') ,
             'size.exists' => __('profile.addCar.size_exists') ,
             'brand.required' => __('profile.addCar.brand_required') ,
             'brand.exists' => __('profile.addCar.brand_exists') ,
             'category.required' => __('profile.addCar.category_required') ,
             'category.exists' => __('profile.addCar.category_exists') ,
             'group.required' => __('profile.addCar.group_required') ,
             'group.exists' => __('profile.addCar.group_exists') ,
             'year.required' => __('profile.addCar.year_required') ,
             'year.exists' => __('profile.addCar.year_exists') ,
             
         ];
         $rules = [
             'country' => 'required|exists:cat_country,id',
             'brand' => 'required|exists:cat_brands,id',
             'category' => 'required|exists:cat_categories,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
            $cars = cars::where(['country' => $request->country , 'brand' => $request->brand , 'category' => $request->category])->get();
            return response()->json(['status' => 'done' , 'cars' => $cars],200);
        }
    }
    public function profileInsertcar(Request $request){
          if($request->ajax()){
             $messages = [
              'size.required' => __('profile.addCar.size_required') ,
             'size.exists' => __('profile.addCar.size_exists') ,
             'brand.required' => __('profile.addCar.brand_required') ,
             'brand.exists' => __('profile.addCar.brand_exists') ,
             'category.required' => __('profile.addCar.category_required') ,
             'category.exists' => __('profile.addCar.category_exists') ,
             'group.required' => __('profile.addCar.group_required') ,
             'group.exists' => __('profile.addCar.group_exists') ,
             'year.required' => __('profile.addCar.year_required') ,
             'year.exists' => __('profile.addCar.year_exists') ,
             'car.required' => __('profile.addCar.car_required') ,
             'car.exists' => __('profile.addCar.car_exists') ,
             'car.unique' => __('profile.addCar.car_unique') ,
             
         ];
         $rules = [
             'country' => 'required|exists:cat_country,id',
             'brand' => 'required|exists:cars,brand',
             'category' => 'required|exists:cars,category',
             'year' => 'required',
             'color' => 'required|exists:cars_colors,id',
             'car' => 'required|exists:cars,id|unique:users_cars,car,NULL,id,user,'.auth()->guard('users')->user()->id,
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
                $car = cars::find($request->car);
                $new_usersCars =  new usersCars();
                $new_usersCars->user = auth()->guard('users')->user()->id;
                $new_usersCars->car = $car->id;
                $new_usersCars->country = $car->country;
                $new_usersCars->brand = $car->brand;
                $new_usersCars->category = $car->category;
                $new_usersCars->year = $car->year;
                $new_usersCars->color = $car->color;
                $new_usersCars->save();
                return response()->json(['status' => 'done' , 'message' => __('profile.carAdded')],200);
        }

    }
    public function roadHelp(){
        $roadHelpServices = roadHelpServices::all();
        $countries = cars::get()->unique('country');
        $brands = cars::get()->unique('brand');
        return view('roadside.addRequest',compact('roadHelpServices','countries','brands'));
    }
     public function roadHelpFirstRequest(Request $request){
             $messages = [
             'service.required' => __('front.roadhelp.service_required') ,
             'service.exists' => __('front.roadhelp.service_exists') ,
             'car.required' => __('front.roadhelp.car_required') ,
             'car.exists' => __('front.roadhelp.car_exists') ,
             
         ];
         $rules = [
             'service' => 'required|exists:road_help_services,id',
             'car' => 'required|exists:users_cars,id',
             'description' => 'nullable|string',
             'lat' => 'nullable|string',
             'lng' => 'nullable|string',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withErrors($validator);
         }
         $new_roadHelpRequests = new roadHelpRequests();
         $new_roadHelpRequests->user = auth()->guard('users')->user()->id;
         $new_roadHelpRequests->car = $request->car;
         $new_roadHelpRequests->type = 1;
         $new_roadHelpRequests->service = $request->service;
         $new_roadHelpRequests->description = $request->description;
         $new_roadHelpRequests->long = $request->lng;
         $new_roadHelpRequests->lat = $request->lat;
         $new_roadHelpRequests->save();
        return redirect()->route('roadhelp.request.first.show',$new_roadHelpRequests->id);
    }
     public function roadHelpFirstRequestShow($id){
         $roadHelpRequest = roadHelpRequests::find($id);
         if(!$roadHelpRequest){return abort(404);}
        return view('roadside.RequestFirst',compact('roadHelpRequest'));
    }
    public function roadHelpRequestReview($id){
        $roadHelpRequest = roadHelpRequests::find($id);
        if(!$roadHelpRequest){return abort(404);}
        if($roadHelpRequest->status == 1){
            return view('roadside.RequestFirst',compact('roadHelpRequest'));
        }
        $roadHelpRequest->status = 1 ;// 1 mean review ;
        $roadHelpRequest->save();
        return view('roadside.RequestFirst',compact('roadHelpRequest'));
    }
     public function roadHelpRequestConfirm(Request $request,$id){
         $roadHelpRequest = roadHelpRequests::find($id);
         if(!$roadHelpRequest){return abort(404);}
         $roadHelpRequestCheckRelatedUser = roadHelpRequests::where(['id' => $id , 'user' => auth()->guard('users')->user()->id])->first();
         if(!$roadHelpRequestCheckRelatedUser){return abort(403);}
          $messages = [
              'paymentType.required' => __('front.roadhelp.paymentType_required') ,
              'paymentType.in' => __('front.roadhelp.paymentType_in') ,
             
         ];
         $rules = [
             'paymentType' => 'required|in:keynet,cash',
             
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withErrors($validator);
         }
         if($request->paymentType == 'cash'){
             $roadHelpRequest->status = '2'; //mean this request confirmed by cash , 3 mean paid by kenet
             $roadHelpRequest->save();
             $new_roadHelpPaymentCash = new payments();
             $new_roadHelpPaymentCash->request = $roadHelpRequest->id;
             $new_roadHelpPaymentCash->status = 'CREATED';
             $new_roadHelpPaymentCash->type = 'roadhelp';
             $new_roadHelpPaymentCash->invoice = 'cash';
             $new_roadHelpPaymentCash->save();
             return redirect()->route('roadhelp.request.success',$roadHelpRequest->id);
         }
            $new_roadHelpPaymentKenet = new payments();
            $new_roadHelpPaymentKenet->request =  $roadHelpRequest->id;
            $new_roadHelpPaymentKenet->status =  'CREATED';
            $new_roadHelpPaymentKenet->type =  'roadhelp';
            $new_roadHelpPaymentKenet->save();
            $fullPrice = $roadHelpRequest->serviceObj->price;
            $data = array("desc" => 'desc' , 'paymentID' => $new_roadHelpPaymentKenet->id , 'name' => auth()->guard('users')->user()->name , 'price' => $fullPrice,'email' => auth()->guard('users')->user()->email);
            $res =  $this->initialPayment($data);
            $new_roadHelpPaymentKenet->invoice = $res['id'];
            $new_roadHelpPaymentKenet->save();
            return redirect($res['url']);
         
     }
     public function roadHelpRequestsuccess($id){
        $roadHelpRequest = roadHelpRequests::find($id);
        if(!$roadHelpRequest){return abort(404);}
        return view('roadside.RequestFirst',compact('roadHelpRequest'));
    }
    public function roadhelpmyRequests(){
  
        return view('profile.roadhelpRequests');
    }
    public function carwashmyRequests(){
  
        return view('profile.carwashmyRequests');
    }
      public function garagemyRequests(){
  
        return view('profile.garagemyRequests');
    }
     public function Spare_partsmyRequests(){
  
        return view('profile.Spare_partsmyRequests');
    }
    
    
    
    
    //////////////////
    
    
        public function carwash(){
        $carWashServices = carWashServices::all();
        $countries = cars::get()->unique('country');
        $brands = cars::get()->unique('brand');
        return view('carwash.addRequest',compact('carWashServices','countries','brands'));
    }
     public function carwashFirstRequest(Request $request){
             $messages = [
             'service.required' => __('front.roadhelp.service_required') ,
             'service.exists' => __('front.roadhelp.service_exists') ,
             'car.required' => __('front.roadhelp.car_required') ,
             'car.exists' => __('front.roadhelp.car_exists') ,
             
         ];
         $rules = [
             'service' => 'required|exists:car_wash_services,id',
             'car' => 'required|exists:users_cars,id',
             'description' => 'nullable|string',
             'lat' => 'nullable|string',
             'lng' => 'nullable|string',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withErrors($validator);
         }
         $new_carWashRequests= new carWashRequests();
         $new_carWashRequests->user = auth()->guard('users')->user()->id;
         $new_carWashRequests->car = $request->car;
         $new_carWashRequests->type = 1;
         $new_carWashRequests->service = $request->service;
         $new_carWashRequests->description = $request->description;
         $new_carWashRequests->long = $request->lng;
         $new_carWashRequests->lat = $request->lat;
         $new_carWashRequests->save();
        return redirect()->route('carwash.request.first.show',$new_carWashRequests->id);
    }
     public function carwashFirstRequestShow($id){
         $carWashRequest = carWashRequests::find($id);
         if(!$carWashRequest){return abort(404);}
        return view('carwash.RequestFirst',compact('carWashRequest'));
    }
    public function carwashRequestReview($id){
        $carWashRequest = carWashRequests::find($id);
        if(!$carWashRequest){return abort(404);}
        if($carWashRequest->status == 1){
            return view('carwash.RequestFirst',compact('carWashRequest'));
        }
        $carWashRequest->status = 1 ;// 1 mean review ;
        $carWashRequest->save();
        return view('carwash.RequestFirst',compact('carWashRequest'));
    }
     public function carwashRequestConfirm(Request $request,$id){
         $carWashRequest = carWashRequests::find($id);
         if(!$carWashRequest){return abort(404);}
         $carWashRequestCheckRelatedUser = carWashRequests::where(['id' => $id , 'user' => auth()->guard('users')->user()->id])->first();
         if(!$carWashRequestCheckRelatedUser){return abort(403);}
          $messages = [
              'paymentType.required' => __('front.roadhelp.paymentType_required') ,
              'paymentType.in' => __('front.roadhelp.paymentType_in') ,
             
         ];
         $rules = [
             'paymentType' => 'required|in:keynet,cash',
             
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withErrors($validator);
         }
         if($request->paymentType == 'cash'){
             $carWashRequest->status = '2'; //mean this request confirmed by cash , 3 mean paid by kenet
             $carWashRequest->save();
             $new_carWashRequestCash = new payments();
             $new_carWashRequestCash->request = $carWashRequest->id;
             $new_carWashRequestCash->status = 'CREATED';
             $new_carWashRequestCash->type = 'carwash';
             $new_carWashRequestCash->invoice = 'cash';
             $new_carWashRequestCash->save();
             return redirect()->route('carwash.request.success',$carWashRequest->id);
         }
            $new_carWashRequestKenet = new payments();
            $new_carWashRequestKenet->request =  $carWashRequest->id;
            $new_carWashRequestKenet->status =  'CREATED';
            $new_carWashRequestKenet->type =  'carwash';
            $new_carWashRequestKenet->save();
            $fullPrice = $carWashRequest->serviceObj->price;
            $data = array("desc" => 'desc' , 'paymentID' => $new_carWashRequestKenet->id , 'name' => auth()->guard('users')->user()->name , 'price' => $fullPrice,'email' => auth()->guard('users')->user()->email);
            $res =  $this->initialPayment($data);
            $new_carWashRequestKenet->invoice = $res['id'];
            $new_carWashRequestKenet->save();
            return redirect($res['url']);
         
     }
     public function carwashRequestsuccess($id){
        $carWashRequest = carWashRequests::find($id);
        if(!$carWashRequest){return abort(404);}
        return view('carwash.RequestFirst',compact('carWashRequest'));
    }
    //////////////////
    
      public function garage(){
        $garage_services = garage_services::all();
         $countries = cars::get()->unique('country');
        $brands = cars::get()->unique('brand');
        return view('garage.addRequest',compact('garage_services','countries','brands'));
    }
    
        public function garageFirstRequest(Request $request){
             $messages = [
             'service.required' => __('front.garage.service_required') ,
             'service.exists' => __('front.garage.service_exists') ,
             'car.required' => __('front.garage.car_required') ,
             'car.exists' => __('front.garage.car_exists') ,
             
         ];
         $rules = [
             'service' => 'required|exists:garage_services,id',
             'car' => 'required|exists:users_cars,id',
             'lat' => 'nullable|string',
             'lng' => 'nullable|string',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withErrors($validator);
         }
         $new_garage_Requests = new garage_Requests();
         $new_garage_Requests->user = auth()->guard('users')->user()->id;
         $new_garage_Requests->car = $request->car;
         $new_garage_Requests->type = 1;
         $new_garage_Requests->long = $request->lng;
         $new_garage_Requests->lat = $request->lat;
         $new_garage_Requests->service = $request->service;
         $new_garage_Requests->description = $request->description;
         $new_garage_Requests->save();
        return redirect()->route('garage.request.first.show',$new_garage_Requests->id);
    }
      public function garageFirstRequestShow($id){
         $garage_Request = garage_Requests::find($id);
         if(!$garage_Request){return abort(404);}
            if($garage_Request->status == 1){
             return redirect()->route('garage.request.follow',$garage_Request->id);
         }
        return view('garage.RequestFirst',compact('garage_Request'));
    }
     public function garageRequestFollow($id){
        $garage_Request = garage_Requests::find($id);
        if(!$garage_Request){return abort(404);}
        return view('garage.RequestFirst',compact('garage_Request'));
    }
    public function garageFirstRequestAddMessageText(Request $request){
        $messages = [
            
             
         ];
         $rules = [
             'message' => 'required|min:1|max:300',
             'requestid' => 'required|exists:garage__requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
        $garage_Request = garage_Requests::find($request->requestid);
        if($garage_Request->user == auth()->guard('users')->user()->id){
            $new_garage_request_message = new garage_request_messages();
            $new_garage_request_message->parent = auth()->guard('users')->user()->id;
            $new_garage_request_message->child = ($garage_Request->assignedAdmin) ? $garage_Request->assignedAdmin : null;
            $new_garage_request_message->parentModel = 'user';
            $new_garage_request_message->request = $garage_Request->id;
            $new_garage_request_message->message = $request->message;
            $new_garage_request_message->type = 'text';
            $new_garage_request_message->save();
            $new_garage_request_message['user_obj'] = auth()->guard('users')->user();
            $new_garage_request_message['user_obj']['image'] = asset('assets/users/'.auth()->guard('users')->user()->image);
            $new_garage_request_message['date'] = Carbon::parse($new_garage_request_message->created_at)->format('d / m / Y');
            return response()->json(['status' => 'done' , 'message' => __('front.garage.messageSent') ,'value' => $new_garage_request_message],200);
        }else{
            return abort(401);
        }
    }
    public function garageFirstRequestGetMessages(Request $request){
       
        $messages = [

         ];
         $rules = [
             'requestid' => 'required|exists:garage__requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
        $garage_Request = garage_Requests::find($request->requestid);
        if($garage_Request->user == auth()->guard('users')->user()->id){
             if($request->last == null || $request->last == ''){
          $request['last'] = 1;  
        }
           $messages =  garage_request_messages::with('adminAsParentObj')->where('id' ,'>' , $request->last)->where(['request'=> $request->requestid])->where(['parentModel' =>'admin'])->get();
            collect($messages)->map(function ($message){
                $message['adminAsParentObj']['image'] = asset('assets/users/'.$message['adminAsParentObj']['image']);
                $message['date'] = Carbon::parse($message->created_at)->format('d / m / Y');
                return $message;
            });
           $last =  garage_request_messages::where('id' ,'>' , $request->last)->where(['request'=> $request->requestid])->where(['parentModel' =>'admin'])->latest()->first();
            if(!$last){
                $last = null;
            }else{
                $last = $last->id;
            }
        return response()->json(['status' => 'done' , 'messages' => $messages , 'last' => $last],200);
        }
    }
    
        public function garageFirstRequestCheckFollowUp(Request $request){
        $messages = [    
         ];
         $rules = [
             'requestid' => 'required|exists:garage__requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
        $garage_Request = garage_Requests::find($request->requestid);
        if($garage_Request->user == auth()->guard('users')->user()->id){
            if($garage_Request->status == 1){
               return response()->json(['status' => 'done' , 'request_status' => $garage_Request->status],200); 
            }
            if($garage_Request->status == 2){
               return response()->json(['status' => 'payment' , 'request_status' => $garage_Request->status],200); 
            }
           
        }
    }
         public function garageRequestConfirm(Request $request,$id){
         $garage_Request = garage_Requests::find($id);
         if(!$garage_Request){return abort(404);}
         $garage_RequestCheckRelatedUser = garage_Requests::where(['id' => $id , 'user' => auth()->guard('users')->user()->id])->first();
         if(!$garage_RequestCheckRelatedUser){return abort(403);}
          $messages = [
              'paymentType.required' => __('front.garage.paymentType_required') ,
              'paymentType.in' => __('front.garage.paymentType_in') ,
             
         ];
         $rules = [
             'paymentType' => 'required|in:keynet,cash',
             
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withErrors($validator);
         }
         if($request->paymentType == 'cash'){
             $garage_Request->status = '3'; //mean this request confirmed by cash , 4 mean paid by kenet
             $garage_Request->save();
             $new_garage_RequestCash = new payments();
             $new_garage_RequestCash->request = $garage_Request->id;
             $new_garage_RequestCash->status = 'CREATED';
             $new_garage_RequestCash->type = 'carwash';
             $new_garage_RequestCash->invoice = 'cash';
             $new_garage_RequestCash->save();
             return redirect()->route('garage.request.success',$garage_Request->id);
         }
            $new_garage_RequestKenet = new payments();
            $new_garage_RequestKenet->request =  $garage_Request->id;
            $new_garage_RequestKenet->status =  'CREATED';
            $new_garage_RequestKenet->type =  'garage';
            $new_garage_RequestKenet->save();
            $fullPrice = $garage_Request->amount;
            $data = array("desc" => 'desc' , 'paymentID' => $new_garage_RequestKenet->id , 'name' => auth()->guard('users')->user()->name , 'price' => $fullPrice,'email' => auth()->guard('users')->user()->email);
            $res =  $this->initialPayment($data);
            $new_garage_RequestKenet->invoice = $res['id'];
            $new_garage_RequestKenet->save();
            return redirect($res['url']);
         
     }
    
    public function garageRequestsuccess($id){
        $garage_Request = garage_Requests::find($id);
        if(!$garage_Request){return abort(404);}
        return view('garage.RequestFirst',compact('garage_Request'));
    }
    public function Spare_parts(){
        $countries = cars::get()->unique('country');
        $brands = cars::get()->unique('brand');
        return view('Spare_parts.addRequest',compact('countries','brands'));
    }
    public function Spare_partsFirstRequest(Request $request){
             $messages = [
            
             'car.required' => __('front.garage.car_required') ,
             'car.exists' => __('front.garage.car_exists') ,
             
         ];
         $rules = [
             'car' => 'required|exists:users_cars,id',
             'spare_part_name' => 'required|string',
             'spare_part_WordPad' => 'required|file|mimes:jpeg,png,jpg|max:2000',
             'spare_part_photo' => 'nullable|file|mimes:jpeg,png,jpg|max:2000',
             'lat' => 'nullable|string',
             'lng' => 'nullable|string',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withErrors($validator);
         }
         $spare_part_WordPad_file = $request->file('spare_part_WordPad');
         $spare_part_WordPad_file_name = 'spare_part_WordPad-'.Str::slug($request->spare_part_name).'-'. time() .'.'.$spare_part_WordPad_file->getClientOriginalExtension();
         $spare_part_WordPad_file->move('assets/carsImages/spares', $spare_part_WordPad_file_name);
         $new_sparePart_requests = new sparePart_requests();
         if($request->file('spare_part_photo')){
            $spare_part_photo_file = $request->file('spare_part_photo');
            $spare_part_photo_file_name = 'spare_part_photo-'.Str::slug($request->spare_part_name).'-'. time() .'.'.$spare_part_photo_file->getClientOriginalExtension();
            $spare_part_photo_file->move('assets/carsImages/spares', $spare_part_photo_file_name);
         }
         
         $new_sparePart_requests->user = auth()->guard('users')->user()->id;
         $new_sparePart_requests->car = $request->car;
         $new_sparePart_requests->spartName = $request->spare_part_name;
         $new_sparePart_requests->spare_part_WordPad = $spare_part_WordPad_file_name;
         if($request->file('spare_part_photo')){
             $new_sparePart_requests->spare_part_photo = $spare_part_photo_file_name;
         }
         $new_sparePart_requests->save();
        return redirect()->route('Spare_parts.request.first.show',$new_sparePart_requests->id);
    }
        public function Spare_partsFirstRequestShow($id){
         $sparePart_request = sparePart_requests::find($id);
         if(!$sparePart_request){return abort(404);}
            if($sparePart_request->status == 1){
             return redirect()->route('Spare_parts.request.confirmOrPay',$sparePart_request->id);
         }
        return view('Spare_parts.RequestFirst',compact('sparePart_request'));
    }
      public function Spare_partsFirstRequestCheckFollowUp(Request $request){
        $messages = [    
         ];
         $rules = [
             'requestid' => 'required|exists:spare_part_requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
        $sparePart_request = sparePart_requests::find($request->requestid);
        if($sparePart_request->user == auth()->guard('users')->user()->id){
            if($sparePart_request->status == 1){
               return response()->json(['status' => 'done'],200); 
            }
            if($sparePart_request->status == 5){
               return response()->json(['status' => 'cancelled'],200); 
            }
           
        }
    }
    public function Spare_partsRequestConfirm($id){
         $sparePart_request = sparePart_requests::find($id);
        return view('Spare_parts.RequestFirst',compact('sparePart_request'));
    }
      public function Spare_partsRequestPay(Request $request,$id){
         $sparePart_request = sparePart_requests::find($id);
         if(!$sparePart_request){return abort(404);}
         $sparePart_requestCheckRelatedUser = sparePart_requests::where(['id' => $id , 'user' => auth()->guard('users')->user()->id])->first();
         if(!$sparePart_requestCheckRelatedUser){return abort(403);}
          $messages = [
              'paymentType.required' => __('front.Spare_parts.paymentType_required') ,
              'paymentType.in' => __('front.Spare_parts.paymentType_in') ,
             
         ];
         $rules = [
             'paymentType' => 'required|in:keynet,cash',
             
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withErrors($validator);
         }
         if($request->paymentType == 'cash'){
             $sparePart_request->status = '2'; //mean this request confirmed by cash , 3 mean paid by kenet
             $sparePart_request->save();
             $new_sparePart_RequestCash = new payments();
             $new_sparePart_RequestCash->request = $sparePart_request->id;
             $new_sparePart_RequestCash->status = 'CREATED';
             $new_sparePart_RequestCash->type = 'spare_parts';
             $new_sparePart_RequestCash->invoice = 'cash';
             $new_sparePart_RequestCash->save();
             return redirect()->route('Spare_parts.request.success',$sparePart_request->id);
         }
            $new_sparePart_RequestKenet = new payments();
            $new_sparePart_RequestKenet->request =  $sparePart_request->id;
            $new_sparePart_RequestKenet->status =  'CREATED';
            $new_sparePart_RequestKenet->type =  'spare_parts';
            $new_sparePart_RequestKenet->save();
            $fullPrice = $sparePart_request->amount;
            $data = array("desc" => 'desc' , 'paymentID' => $new_sparePart_RequestKenet->id , 'name' => auth()->guard('users')->user()->name , 'price' => $fullPrice,'email' => auth()->guard('users')->user()->email);
            $res =  $this->initialPayment($data);
            $new_sparePart_RequestKenet->invoice = $res['id'];
            $new_sparePart_RequestKenet->save();
            return redirect($res['url']);
         
     }
     public function Spare_partsRequestsuccess($id){
        $sparePart_request = sparePart_requests::find($id);
        if(!$sparePart_request){return abort(404);}
        return view('Spare_parts.RequestFirst',compact('sparePart_request'));
    }
    
    public function route_register(Request $request){
        if(Route::has($request->route)){
            session()->put('previous_route',$request->route);
            return response('done',200);
        }
    }
    
    
    
    public function sell(){
        return view('sell.main');
    }

    public function sellSingle(){
        return view('sell.single');
    }

    public function sellNew()
    {
        return view('profile.sell');
    }

    public function sellEdite()
    {
        return view('profile.sellEdite');
    }

    public function sellPackages()
    {
      return view('profile.packages');
    }

    public function sellAll()
    {
      return view('profile.all');
    }
}
