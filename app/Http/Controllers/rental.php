<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\cat_size;
use App\Models\rentalUsers;
use App\Models\rentalCars;
use App\Models\rentalBookRequests;
use App\Models\cars;
use Validator;
use \Carbon\Carbon;
class rental extends Controller
{   
    private $start_cars_models = 1975;
     public function login(){
       return view('admin.CarRental.login');
    }
       public function Dologin(Request $request){
         auth()->guard('rentals')->attempt(['email' => $request->email , 'password' => $request->password],$request->remember);
            if(auth()->guard('rentals')->check()){
                return response()->json(['status' => 'done','message'=> __('admin.successedSignin')],200);
            }else{
                $checkEmailFoundOrNot = rentalUsers::where(['email' => $request->email])->first();
                if($checkEmailFoundOrNot){
                    return response()->json(['status' => 'failed','message'=>__('admin.passwordNotCorrect')],200);
                }else{
                    return response()->json(['status' => 'failed','message'=>__('admin.emailNotCorrect')],200);
                }
                
            }
    }
    public function showcars(Request $request){
        $cars = rentalCars::where(['office' => auth()->guard('rentals')->user()->id])->get();
       return view('admin.CarRental.cars',compact('cars'));
    }
     public function addcars(){
       return view('admin.CarRental.addcars');
    }
        public function postcars(Request $request){
         $messages = [
             'car.required' => __('rental.cararr.car_required') ,
             'car.exists' => __('rental.cararr.car_exists') ,
             'price.required' => __('rental.cararr.price_required') ,
             'price.gt' => __('rental.cararr.price_gt') ,
             'content.required' => __('rental.cararr.content_required') ,
             'content.min' => __('rental.cararr.content_min') ,
         ];
         $rules = [
             'car' => 'required|exists:cars,id',
             'color' => 'required|exists:cars_colors,id',
             'price' => 'required|gt:1',
             'content' => 'required|min:20',
             'num' => 'required|min:1|numeric',
             'year' => 'required|in:'.implode(',',range($this->start_cars_models,Carbon::now()->format('Y'))),

         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $checkCar = rentalCars::where(['car' => $request->car ,'office' => auth()->guard('rentals')->user()->id])->first();
         if($checkCar){return back()->with('errorcar',__('rental.cararr.car_unique'));}
         $car = cars::find($request->car);
         $new_rentalCars = new rentalCars();
         $new_rentalCars->office = auth()->guard('rentals')->user()->id;
         $new_rentalCars->car = $car->id;
         $new_rentalCars->country = $car->size;
         $new_rentalCars->brand = $car->brand;
         $new_rentalCars->category = $car->category;
         $new_rentalCars->year = $request->year;
         $new_rentalCars->color = $request->color;
         $new_rentalCars->num = $request->num;
         $new_rentalCars->price = $request->price;
         $new_rentalCars->description = $request->content;
         $new_rentalCars->save();
         return back()->with('message',__('rental.cararr.addDoneMessage'));
    }
     public function editcars(Request $request,$id){
        $singlecar = rentalCars::find($id);
        if(!$singlecar){return abort(404);}
        $cars = rentalCars::where(['office' => auth()->guard('rentals')->user()->id])->get();
       return view('admin.CarRental.editcars',compact('cars','singlecar'));
    }
    public function updatecars(Request $request,$id){
         $singlecar = rentalCars::find($id);
        if(!$singlecar){return abort(404);}
         $messages = [
             'car.required' => __('rental.cararr.car_required') ,
             'car.exists' => __('rental.cararr.car_exists') ,
             'price.required' => __('rental.cararr.price_required') ,
             'price.gt' => __('rental.cararr.price_gt') ,
             'content.required' => __('rental.cararr.content_required') ,
             'content.min' => __('rental.cararr.content_min') ,
         ];
         $rules = [
             'car' => 'required|exists:cars,id',
             'color' => 'required|exists:cars_colors,id',
             'price' => 'required|gt:1',
             'content' => 'required|min:20',
             'status' => 'nullable|in:on,off',
             'num' => 'required|min:1|numeric',
             'year' => 'required|in:'.implode(',',range($this->start_cars_models,Carbon::now()->format('Y'))),
             

         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $car = cars::find($request->car);
         $singlecar->car = $car->id;
         $singlecar->country = $car->country;
         $singlecar->brand = $car->brand;
         $singlecar->category = $car->category;
         $singlecar->price = $request->price;
         $singlecar->year = $request->year;
         $singlecar->color = $request->color;
         $singlecar->num = $request->num;
         $singlecar->description = $request->content;
         $singlecar->status = ($request->status == 'on') ? 1 : 2;
         $singlecar->save();
         return back()->with('message',__('rental.cararr.editDoneMessage'));
    }
     public function bookings(){
         $bookings = rentalBookRequests::where(['office' => auth()->guard('rentals')->user()->id])->where(function($query){
                 $query->where('delivery_status','paid');
                 $query->orWhere('delivery_status','cash');
                 $query->orWhere('delivery_status','inprogress');
                 $query->orWhere('delivery_status','done');
             })->orderby('created_at','DESC')->get();
          return view('admin.CarRental.bookings',compact('bookings'));
     }
    public function bookingsView($id){
          $rentalBookRequest = rentalBookRequests::find($id);
        if(!$rentalBookRequest){return abort(404);}
        return view('admin.CarRental.requestView',compact('rentalBookRequest'));
        
    }
     public function requestChangeStatus(Request $request){
             $messages = [
             
         ];
         $rules = [
             'change' => 'required|exists:rental_book_requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
        $rentalBookRequest = rentalBookRequests::find($request->change);
        if($rentalBookRequest->delivery_status == 'cash'){
            $rentalBookRequest->delivery_status = 'inprogress';
            $rentalBookRequest->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.rental.RequestProgress')],200);
        }
          if($rentalBookRequest->delivery_status == 'inprogress'){
            $rentalBookRequest->delivery_status = 'done';
            $rentalBookRequest->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.rental.RequestDone')],200);
        }
    }
    
    
}
