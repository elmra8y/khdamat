<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\cat_country;
use App\Models\cat_brand;
use App\Models\cat_category;
use App\Models\cat_group;
use App\Models\cars;
use App\Models\admins;
use App\Models\rentalUsers;
use App\Models\roadHelpServices;
use App\Models\carWashServices;
use App\Models\garage_services;
use App\Models\roadHelpRequests;
use App\Models\garage_Requests;
use App\Models\rentalBookRequests;
use App\Models\carWashRequests;
use App\Models\cancelled_requests;
use App\Models\garage_request_messages;
use App\Models\sparePart_requests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;
use \Carbon\Carbon;
class admin extends Controller
{
     
     public function login(){
       return view('admin.Admin.login');
    }
     public function Dologin(Request $request){
         auth()->guard('admins')->attempt(['email' => $request->email , 'password' => $request->password],$request->remember);
            if(auth()->guard('admins')->check()){
                return response()->json(['status' => 'done','message'=> __('admin.successedSignin')],200);
            }else{
                $checkEmailFoundOrNot = admins::where(['email' => $request->email])->first();
                if($checkEmailFoundOrNot){
                    return response()->json(['status' => 'failed','message'=>__('admin.passwordNotCorrect')],200);
                }else{
                    return response()->json(['status' => 'failed','message'=>__('admin.emailNotCorrect')],200);
                }
                
            }
    }
    
     public function countries(){
       $cat_countries = cat_country::orderby('created_at','DESC')->get();
       return view('admin.Admin.countries',compact('cat_countries'));
    }
     public function addcountries(){
       return view('admin.Admin.addcountries');
    }
     public function postcountries(Request $request){
         $messages = [
             'name_en.required' => __('admin.rental.size.name_en_required') ,
             'name_en.min' => __('admin.rental.size.name_en_min') ,
             'name_en.max' => __('admin.rental.size.name_en_max') ,
             'name_ar.required' => __('admin.rental.size.name_ar_required') ,
             'name_ar.min' => __('admin.rental.size.name_ar_min') ,
             'name_ar.max' => __('admin.rental.size.name_ar_max') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $new_cat_country = new cat_country();
         $new_cat_country->name_en = $request->name_en;
         $new_cat_country->name_ar = $request->name_ar;
         $new_cat_country->save();
         return back()->with('message',__('admin.rental.country.addDoneMessage'));
    }
     public function editcountries($id = null){
         $cat_country = cat_country::find($id);
         if(!$cat_country){abort(404);}
         return view('admin.Admin.editcountries',compact('cat_country'));
    }
     public function updatecountries(Request $request,$id = null){
          $cat_country = cat_country::find($id);
          if(!$cat_country){abort(404);}
          $messages = [
             'name_en.required' => __('admin.rental.size.name_en_required') ,
             'name_en.min' => __('admin.rental.size.name_en_min') ,
             'name_en.max' => __('admin.rental.size.name_en_max') ,
             'name_ar.required' => __('admin.rental.size.name_ar_required') ,
             'name_ar.min' => __('admin.rental.size.name_ar_min') ,
             'name_ar.max' => __('admin.rental.size.name_ar_max') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $cat_country->name_en = $request->name_en;
         $cat_country->name_ar = $request->name_ar;
         $cat_country->save();
         return back()->with('message',__('admin.rental.country.editDoneMessage'));
    }
    /* brand */
    
     public function brands(){
       $brands = cat_brand::orderby('created_at','DESC')->get();
       return view('admin.Admin.brands',compact('brands'));
    }
     public function addbrands(){
       return view('admin.Admin.addbrands');
    }
     public function postbrands(Request $request){
         $messages = [
             'name_en.required' => __('admin.rental.brand.name_en_required') ,
             'name_en.min' => __('admin.rental.brand.name_en_min') ,
             'name_en.max' => __('admin.rental.brand.name_en_max') ,
             'name_ar.required' => __('admin.rental.brand.name_ar_required') ,
             'name_ar.min' => __('admin.rental.brand.name_ar_min') ,
             'name_ar.max' => __('admin.rental.brand.name_ar_max') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
             'country' => 'required|exists:cat_country,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $new_cat_brand = new cat_brand();
         $new_cat_brand->name_en = $request->name_en;
         $new_cat_brand->name_ar = $request->name_ar;
         $new_cat_brand->country = $request->country;
         $new_cat_brand->save();
         return back()->with('message',__('admin.rental.brand.addDoneMessage'));
    }
     public function editbrands($id = null){
         $cat_brand = cat_brand::find($id);
         if(!$cat_brand){abort(404);}
         return view('admin.Admin.editbrands',compact('cat_brand'));
    }
     public function updatebrands(Request $request,$id = null){
          $cat_brand = cat_brand::find($id);
          if(!$cat_brand){abort(404);}
          $messages = [
             'name_en.required' => __('admin.rental.brand.name_en_required') ,
             'name_en.min' => __('admin.rental.brand.name_en_min') ,
             'name_en.max' => __('admin.rental.brand.name_en_max') ,
             'name_ar.required' => __('admin.rental.brand.name_ar_required') ,
             'name_ar.min' => __('admin.rental.brand.name_ar_min') ,
             'name_ar.max' => __('admin.rental.brand.name_ar_max') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
             'country' => 'required|exists:cat_country,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $cat_brand->name_en = $request->name_en;
         $cat_brand->name_ar = $request->name_ar;
         $cat_brand->country = $request->country;
         $cat_brand->save();
         return back()->with('message',__('admin.rental.brand.editDoneMessage'));
    }
    /* category */
    
     public function categories(){
       $cat_categories = cat_category::orderby('created_at','DESC')->get();
       return view('admin.Admin.categories',compact('cat_categories'));
    }
     public function addcategories(){
       return view('admin.Admin.addcategories');
    }
     public function postcategories(Request $request){
         $messages = [
             'name_en.required' => __('admin.rental.category.name_en_required') ,
             'name_en.min' => __('admin.rental.category.name_en_min') ,
             'name_en.max' => __('admin.rental.category.name_en_max') ,
             'name_ar.required' => __('admin.rental.category.name_ar_required') ,
             'name_ar.min' => __('admin.rental.category.name_ar_min') ,
             'name_ar.max' => __('admin.rental.category.name_ar_max') ,
             'size.required' => __('admin.rental.category.size_required') ,
             'size.exists' => __('admin.rental.category.size_exists') ,
             'brand.required' => __('admin.rental.category.brand_required') ,
             'brand.exists' => __('admin.rental.category.brand_exists') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
             'brand' => 'required|exists:cat_brands,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $new_cat_category = new cat_category();
         $new_cat_category->name_en = $request->name_en;
         $new_cat_category->name_ar = $request->name_ar;
         $new_cat_category->brand = $request->brand;
         $new_cat_category->save();
         return back()->with('message',__('admin.rental.category.addDoneMessage'));
    }
     public function editcategories($id = null){
         $cat_category = cat_category::find($id);
         if(!$cat_category){abort(404);}
         return view('admin.Admin.editcategories',compact('cat_category'));
    }
     public function updatecategories(Request $request,$id = null){
          $cat_category = cat_category::find($id);
          if(!$cat_category){abort(404);}
          $messages = [
             'name_en.required' => __('admin.rental.category.name_en_required') ,
             'name_en.min' => __('admin.rental.category.name_en_min') ,
             'name_en.max' => __('admin.rental.category.name_en_max') ,
             'name_ar.required' => __('admin.rental.category.name_ar_required') ,
             'name_ar.min' => __('admin.rental.category.name_ar_min') ,
             'name_ar.max' => __('admin.rental.category.name_ar_max') ,
             'size.required' => __('admin.rental.category.size_required') ,
             'size.exists' => __('admin.rental.category.size_exists') ,
             'brand.required' => __('admin.rental.category.brand_required') ,
             'brand.exists' => __('admin.rental.category.brand_exists') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
             'brand' => 'required|exists:cat_brands,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $cat_category->name_en = $request->name_en;
         $cat_category->name_ar = $request->name_ar;
         $cat_category->brand = $request->brand;
         $cat_category->save();
         return back()->with('message',__('admin.rental.category.editDoneMessage'));
    }
     /* car */
    
     public function cars(){
       $cars = cars::orderby('created_at','DESC')->get();
       return view('admin.Admin.cars',compact('cars'));
    }
     public function addcars(){
       return view('admin.Admin.addcars');
    }
     public function postcars(Request $request){
         $messages = [
             'name_en.required' => __('admin.rental.car.name_en_required') ,
             'name_en.min' => __('admin.rental.car.name_en_min') ,
             'name_en.max' => __('admin.rental.car.name_en_max') ,
             'name_ar.required' => __('admin.rental.car.name_ar_required') ,
             'name_ar.min' => __('admin.rental.car.name_ar_min') ,
             'name_ar.max' => __('admin.rental.car.name_ar_max') ,
             'size.required' => __('admin.rental.car.size_required') ,
             'size.exists' => __('admin.rental.car.size_exists') ,
             'brand.required' => __('admin.rental.car.brand_required') ,
             'brand.exists' => __('admin.rental.car.brand_exists') ,
             'category.required' => __('admin.rental.car.category_required') ,
             'category.exists' => __('admin.rental.car.category_exists') ,
             'group.required' => __('admin.rental.car.group_required') ,
             'group.exists' => __('admin.rental.car.group_exists') ,
             'year.required' => __('admin.rental.car.year_required') ,
             'year.in' => __('admin.rental.car.year_in') ,
             'image.required' => __('admin.rental.car.image_required') ,
             'image.file' => __('admin.rental.car.image_file') ,
             'image.file' => __('admin.rental.car.image_file') ,
             'image.mimes' => __('admin.rental.car.image_mimes') ,
             'image.max' => __('admin.rental.car.image_max') ,
             
         ];
         $rules = [
             'name_en' => 'required|max:30',
             'name_ar' => 'required|max:30',
             'country' => 'required|exists:cat_country,id',
             'brand' => 'required|exists:cat_brands,id',
             'category' => 'required|exists:cat_categories,id',
             'image' => 'required|file|mimes:jpeg,png,jpg|max:2000',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
         $image = $request->file('image');
         $imageName = Str::slug($request->name_en).'-'. time() .'.'.$image->getClientOriginalExtension();
         $image->move('assets/carsImages/rental', $imageName);
         $new_car = new cars();
         $new_car->name_en = $request->name_en;
         $new_car->name_ar = $request->name_ar;
         $new_car->country = $request->country;
         $new_car->brand = $request->brand;
         $new_car->category = $request->category;
         $new_car->image = $imageName;
         $new_car->parent_model = 'Admin';
         $new_car->save();
         return response()->json(['status' => 'success','message'=> __('admin.rental.car.addDoneMessage')],200);
    }
     public function editcars($id = null){
         $car = cars::find($id);
         if(!$car){abort(404);}
         return view('admin.Admin.editcar',compact('car'));
    }
     public function updatecars(Request $request,$id = null){
          $car = cars::findorFail($id);
          $messages = [
             'name_en.required' => __('admin.rental.car.name_en_required') ,
             'name_en.min' => __('admin.rental.car.name_en_min') ,
             'name_en.max' => __('admin.rental.car.name_en_max') ,
             'name_ar.required' => __('admin.rental.car.name_ar_required') ,
             'name_ar.min' => __('admin.rental.car.name_ar_min') ,
             'name_ar.max' => __('admin.rental.car.name_ar_max') ,
             'size.required' => __('admin.rental.car.size_required') ,
             'size.exists' => __('admin.rental.car.size_exists') ,
             'brand.required' => __('admin.rental.car.brand_required') ,
             'brand.exists' => __('admin.rental.car.brand_exists') ,
             'category.required' => __('admin.rental.car.category_required') ,
             'category.exists' => __('admin.rental.car.category_exists') ,
             'group.required' => __('admin.rental.car.group_required') ,
             'group.exists' => __('admin.rental.car.group_exists') ,
             'year.required' => __('admin.rental.car.year_required') ,
             'year.in' => __('admin.rental.car.year_in') ,
             'image.required' => __('admin.rental.car.image_required') ,
             'image.file' => __('admin.rental.car.image_file') ,
             'image.file' => __('admin.rental.car.image_file') ,
             'image.mimes' => __('admin.rental.car.image_mimes') ,
             'image.max' => __('admin.rental.car.image_max') ,
             
         ];
         $rules = [
             'name_en' => 'required|max:30',
             'name_ar' => 'required|max:30',
             'country' => 'required|exists:cat_country,id',
             'brand' => 'required|exists:cat_brands,id',
             'category' => 'required|exists:cat_categories,id',
             'image' => 'nullable|file|mimes:jpeg,png,jpg|max:2000',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
         $car->name_en = $request->name_en;
         $car->name_ar = $request->name_ar;
         $car->country = $request->country;
         $car->brand = $request->brand;
         $car->category = $request->category;
         if($request->hasFile('image')){
             $image = $request->file('image');
             $imageName = Str::slug($request->name_en).'-'. time() .'.'.$image->getClientOriginalExtension();
             $image->move('assets/carsImages/rental', $imageName);
             $car->image = $imageName;
         }
         $car->parent_model = 'Admin';
         $car->save();
         return response()->json(['status' => 'success','message'=> __('admin.rental.car.editDoneMessage')],200);
    }
    
     public function getBrandsChildren(Request $request){
        if($request->ajax()){
             $messages = [
             'size.required' => __('admin.rental.group.size_required') ,
             'size.exists' => __('admin.rental.group.size_exists') ,
             'brand.required' => __('admin.rental.group.brand_required') ,
             'brand.exists' => __('admin.rental.group.brand_exists') ,
             
         ];
         $rules = [
             'country' => 'required|exists:cat_country,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
            $brands = cat_brand::where(['country' => $request->country])->get();
            return response()->json($brands,200);
        }
    }
     public function getCategoriesChildren(Request $request){
        if($request->ajax()){
             $messages = [
             'size.required' => __('admin.rental.group.size_required') ,
             'size.exists' => __('admin.rental.group.size_exists') ,
             'brand.required' => __('admin.rental.group.brand_required') ,
             'brand.exists' => __('admin.rental.group.brand_exists') ,
             'category.required' => __('admin.rental.group.category_required') ,
             'category.exists' => __('admin.rental.group.category_exists') ,
             
         ];
         $rules = [
             'brand' => 'required|exists:cat_brands,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
            $categories = cat_category::where(['brand' => $request->brand])->get();
            return response()->json($categories,200);
        }
    }
    
       /* office */
    
     public function offices(){
       $offices = rentalUsers::orderby('created_at','DESC')->get();
       return view('admin.Admin.offices',compact('offices'));
    }
     public function addoffices(){
       return view('admin.Admin.addoffices');
    }
     public function postoffices(Request $request){
         $messages = [
             'name_en.required' => __('admin.rental.office.name_en_required') ,
             'name_en.min' => __('admin.rental.office.name_en_min') ,
             'name_en.max' => __('admin.rental.office.name_en_max') ,
             'name_ar.required' => __('admin.rental.office.name_ar_required') ,
             'name_ar.min' => __('admin.rental.office.name_ar_min') ,
             'name_ar.max' => __('admin.rental.office.name_ar_max') ,
             'email.required' => __('admin.rental.office.email_required') ,
             'email.email' => __('admin.rental.office.email_email') ,
             'email.unique' => __('admin.rental.office.email_unique') ,
             'password.required' => __('admin.rental.office.password_required') ,
             'password.min' => __('admin.rental.office.password_min') ,
             'Confirmpassword.same' => __('admin.rental.office.password_same') ,
             'Confirmpassword.required' => __('admin.rental.office.Confirmpassword_required') ,
             'Confirmpassword.min' => __('admin.rental.office.Confirmpassword_min') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
             'email' => 'required|email|unique:rental_users,email',
             'terms_en' => 'required|string',
             'terms_ar' => 'required|string',
             'password' => 'required|min:8',
             'Confirmpassword' => 'required|min:8|same:password',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $new_rentalUsers = new rentalUsers();
         $new_rentalUsers->name_en = $request->name_en;
         $new_rentalUsers->name_ar = $request->name_ar;
         $new_rentalUsers->email = $request->email;
         $new_rentalUsers->terms_conditions_en = $request->terms_en;
         $new_rentalUsers->terms_conditions_ar = $request->terms_ar;
         $new_rentalUsers->password = Hash::make($request->password);
         $new_rentalUsers->save();
         return back()->with('message',__('admin.rental.office.addDoneMessage'));
    }
     public function editoffices($id = null){
         $rentalUsers = rentalUsers::find($id);
         if(!$rentalUsers){abort(404);}
         return view('admin.Admin.editoffices',compact('rentalUsers'));
    }
     public function updateoffices(Request $request,$id = null){
         $rentalUsers = rentalUsers::find($id);
         if(!$rentalUsers){abort(404);}
           $messages = [
             'name_en.required' => __('admin.rental.office.name_en_required') ,
             'name_en.min' => __('admin.rental.office.name_en_min') ,
             'name_en.max' => __('admin.rental.office.name_en_max') ,
             'name_ar.required' => __('admin.rental.office.name_ar_required') ,
             'name_ar.min' => __('admin.rental.office.name_ar_min') ,
             'name_ar.max' => __('admin.rental.office.name_ar_max') ,
             'email.required' => __('admin.rental.office.email_required') ,
             'email.email' => __('admin.rental.office.email_email') ,
             'email.unique' => __('admin.rental.office.email_unique') ,
             'password.required' => __('admin.rental.office.password_required') ,
             'password.min' => __('admin.rental.office.password_min') ,
             'Confirmpassword.same' => __('admin.rental.office.password_same') ,
             'Confirmpassword.required' => __('admin.rental.office.Confirmpassword_required') ,
             'Confirmpassword.min' => __('admin.rental.office.Confirmpassword_min') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
             'email' => 'required|email|unique:rental_users,email,'.$id.',id',
             'terms_en' => 'required|string',
             'terms_ar' => 'required|string',
             'password' => 'nullable|min:8',
             'Confirmpassword' => 'required_with:password|same:password',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $rentalUsers->name_en = $request->name_en;
         $rentalUsers->name_ar = $request->name_ar;
         $rentalUsers->email = $request->email;
         $rentalUsers->terms_conditions_en = $request->terms_en;
         $rentalUsers->terms_conditions_ar = $request->terms_ar;
         if($request->password){
            $rentalUsers->password = Hash::make($request->password); 
         }
         $rentalUsers->save();
         return back()->with('message',__('admin.rental.office.editDoneMessage'));
    }
     public function roadhelpServicesGet(){
         $roadHelpServices  = roadHelpServices::orderby('created_at','DESC')->get();
         return view('admin.Admin.roadhelpServicesGet',compact('roadHelpServices'));
     }
    public function roadhelpServicesAdd(){
         return view('admin.Admin.roadhelpServicesAdd');
     }
    
    public function roadhelpServicesAddInsert(Request $request){
          $messages = [
             'name_en.required' => __('admin.roadhelp.name_en_required') ,
             'name_en.min' => __('admin.roadhelp.name_en_min') ,
             'name_en.max' => __('admin.roadhelp.name_en_max') ,
             'name_ar.required' => __('admin.roadhelp.name_ar_required') ,
             'name_ar.min' => __('admin.roadhelp.name_ar_min') ,
             'name_ar.max' => __('admin.roadhelp.name_ar_max') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
             'price' => 'required|numeric|min:1',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $new_roadHelpServices = new roadHelpServices();
         $new_roadHelpServices->name_en = $request->name_en;
         $new_roadHelpServices->name_ar = $request->name_ar;
         $new_roadHelpServices->price = $request->price;
         $new_roadHelpServices->save();
         return back()->with('message',__('admin.roadhelp.addDoneMessage'));
    }
       public function roadhelprequestsGet(){
         $roadHelpRequests  = roadHelpRequests::orderby('created_at','DESC')->get();
         return view('admin.Admin.roadhelprequestsGet',compact('roadHelpRequests'));
     }
      public function roadhelprequestView($id){
         $roadHelpRequest  = roadHelpRequests::find($id);
         return view('admin.Admin.roadhelprequestView',compact('roadHelpRequest'));
     }
    public function roadhelprequestChangeStatus(Request $request){
             $messages = [
             
         ];
         $rules = [
             'change' => 'required|exists:road_help_requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
        $roadHelpRequest = roadHelpRequests::find($request->change);
        if($roadHelpRequest->status == 0){
            $roadHelpRequest->status = 1;
            $roadHelpRequest->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.roadhelp.doneRequest')],200);
        }
         if($roadHelpRequest->status == 2 || $roadHelpRequest->status == 3){
            $roadHelpRequest->status = 4;
            $roadHelpRequest->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.roadhelp.finishedRequest')],200);
        }
    }
    
    
    ////////////////////
     public function carwashServicesGet(){
         $carWashServices  = carWashServices::orderby('created_at','DESC')->get();
         return view('admin.Admin.carwashServicesGet',compact('carWashServices'));
     }
    public function carwashServicesAdd(){
         return view('admin.Admin.carwashServicesAdd');
     }
    
    public function carwashServicesAddInsert(Request $request){
          $messages = [
             'name_en.required' => __('admin.roadhelp.name_en_required') ,
             'name_en.min' => __('admin.roadhelp.name_en_min') ,
             'name_en.max' => __('admin.roadhelp.name_en_max') ,
             'name_ar.required' => __('admin.roadhelp.name_ar_required') ,
             'name_ar.min' => __('admin.roadhelp.name_ar_min') ,
             'name_ar.max' => __('admin.roadhelp.name_ar_max') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
             'price' => 'required|numeric|min:1',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $new_carWashServices = new carWashServices();
         $new_carWashServices->name_en = $request->name_en;
         $new_carWashServices->name_ar = $request->name_ar;
         $new_carWashServices->price = $request->price;
         $new_carWashServices->save();
         return back()->with('message',__('admin.roadhelp.addDoneMessage'));
    }
       public function carwashrequestsGet(){
         $carWashRequests  = carWashRequests::orderby('created_at','DESC')->get();
         return view('admin.Admin.carwashrequestsGet',compact('carWashRequests'));
     }
      public function carwashrequestView($id){
         $carWashRequest  = carWashRequests::find($id);
         return view('admin.Admin.carwashrequestView',compact('carWashRequest'));
     }
    public function carwashrequestChangeStatus(Request $request){
             $messages = [
             
         ];
         $rules = [
             'change' => 'required|exists:car_wash_requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
        $carWashRequest = carWashRequests::find($request->change);
        if($carWashRequest->status == 0){
            $carWashRequest->status = 1;
            $carWashRequest->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.roadhelp.doneRequest')],200);
        }
         if($carWashRequest->status == 2 || $carWashRequest->status == 3){
            $carWashRequest->status = 4;
            $carWashRequest->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.roadhelp.finishedRequest')],200);
        }
    }
    /////////////////////////////////
     public function garageServicesGet(){
         $garage_services  = garage_services::orderby('created_at','DESC')->get();
         return view('admin.Admin.garageServicesGet',compact('garage_services'));
     }
    public function garageServicesAdd(){
         return view('admin.Admin.garageServicesAdd');
     }
    
    public function garageServicesAddInsert(Request $request){
          $messages = [
             'name_en.required' => __('admin.roadhelp.name_en_required') ,
             'name_en.min' => __('admin.roadhelp.name_en_min') ,
             'name_en.max' => __('admin.roadhelp.name_en_max') ,
             'name_ar.required' => __('admin.roadhelp.name_ar_required') ,
             'name_ar.min' => __('admin.roadhelp.name_ar_min') ,
             'name_ar.max' => __('admin.roadhelp.name_ar_max') ,
             
         ];
         $rules = [
             'name_en' => 'required|min:3|max:30',
             'name_ar' => 'required|min:3|max:30',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
         $new_garage_services = new garage_services();
         $new_garage_services->name_en = $request->name_en;
         $new_garage_services->name_ar = $request->name_ar;
         $new_garage_services->save();
         return back()->with('message',__('admin.garage.addDoneMessage'));
    }
       public function garagerequestsGet(){
         $garage_Requests  = garage_Requests::orderby('created_at','DESC')->get();
         return view('admin.Admin.garagerequestsGet',compact('garage_Requests'));
     }
      public function garagerequestView($id){
         $garage_Request  = garage_Requests::find($id);
         return view('admin.Admin.garagerequestView',compact('garage_Request'));
     }
    public function garagerequestChangeStatus(Request $request){
             $messages = [
             
         ];
         $rules = [
             'change' => 'required|exists:garage__requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return back()->withInput()->withErrors($validator);
         }
        $garage_Request = garage_Requests::find($request->change);
        if($garage_Request->status == 0){
            $garage_Request->status = 1;
            $garage_Request->assignedAdmin = auth()->guard('admins')->user()->id;
            $garage_Request->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.garage.Follow_up_request') , 'code' => 1],200);
        }
         if($garage_Request->status == 3 || $garage_Request->status == 4){
            $garage_Request->status = 5;
            $garage_Request->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.garage.finishedRequest')],200);
        }
    }
    
        public function garageFirstRequestAddMessageText(Request $request){
        $messages = [
            
             
         ];
         $rules = [
             'message' => 'required|min:1|max:300',
             'requestid' => 'required|exists:garage__requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
            $garage_Request = garage_Requests::find($request->requestid);
            $new_garage_request_message = new garage_request_messages();
            $new_garage_request_message->parent = auth()->guard('admins')->user()->id;
            $new_garage_request_message->child = $garage_Request->user;
            $new_garage_request_message->parentModel = 'admin';
            $new_garage_request_message->request = $garage_Request->id;
            $new_garage_request_message->message = $request->message;
            $new_garage_request_message->type = 'text';
            $new_garage_request_message->save();
            $new_garage_request_message['user_obj'] = auth()->guard('admins')->user();
            $new_garage_request_message['user_obj']['image'] = asset('assets/users/'.auth()->guard('admins')->user()->image);
            $new_garage_request_message['date'] = Carbon::parse($new_garage_request_message->created_at)->format('d / m / Y');
            return response()->json(['status' => 'done' , 'message' => __('front.garage.messageSent') ,'value' => $new_garage_request_message],200);
        
    }
    public function garageFirstRequestGetMessages(Request $request){
        $messages = [
            
             
         ];
         $rules = [

             'requestid' => 'required|exists:garage__requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
        $garage_Request = garage_Requests::find($request->requestid);
             if($request->last == null  || $request->last == ''){
          $request['last'] = 1;  
        }
           $messages =  garage_request_messages::with('userAsParentObj')->where('id' ,'>' , $request->last)->where(['request'=> $request->requestid])->where(['parentModel' =>'user'])->get();
            collect($messages)->map(function ($message){
                $message['userAsParentObj']['image'] = asset('assets/users/'.$message['userAsParentObj']['image']);
                $message['date'] = Carbon::parse($message->created_at)->format('d / m / Y');
                return $message;
            });
           $last =  garage_request_messages::where('id' ,'>' , $request->last)->where(['request'=> $request->requestid])->where(['parentModel' =>'user'])->latest()->first();
            if(!$last){
                $last = null;
            }else{
                $last = $last->id;
            }
        return response()->json(['status' => 'done' , 'messages' => $messages , 'last' => $last],200);
        
    }
    public function garageFirstRequestassignPrice(Request $request){
        $messages = [
            
             
         ];
         $rules = [

             'amount' => 'required|min:1',
             'requestid' => 'required|exists:garage__requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
        $garage_Request = garage_Requests::find($request->requestid);
        if($garage_Request->amount == null){
            $garage_Request->amount = $request->amount;
            $garage_Request->status = 2; // mean wait to user payment action
            $garage_Request->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.garage.garageService_waitUserPayAction')],200);
        }
    
    }
       public function Spare_partsrequestsGet(){
         $sparePart_requests  = sparePart_requests::orderby('created_at','DESC')->get();
         return view('admin.Admin.sparePartrequestsGet',compact('sparePart_requests'));
     }
    public function Spare_partsrequestView($id){
         $sparePart_request  = sparePart_requests::find($id);
         return view('admin.Admin.Spare_partsrequestView',compact('sparePart_request'));
     }
    
     public function Spare_partsFirstRequestassignPrice(Request $request){
        $messages = [
            
             
         ];
         $rules = [

             'amount' => 'required|min:1',
             'requestid' => 'required|exists:spare_part_requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
        $sparePart_request = sparePart_requests::find($request->requestid);
        if($sparePart_request->amount == null){
            $sparePart_request->amount = $request->amount;
            $sparePart_request->status = 1; // mean wait to user payment action
            $sparePart_request->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.Spare_parts.Spare_parts_waitUserPayAction')],200);
        }
    
    }
     public function Spare_partsFirstRequestcancel(Request $request){
        $messages = [
            
             
         ];
         $rules = [

             'reason' => 'required|min:20|string',
             'requestid' => 'required|exists:spare_part_requests,id',
         ];
         $validator = Validator::make($request->all(),$rules,$messages);
         if($validator->fails()){
            return response()->json(['status' => 'failed','errors'=>$validator->errors()->all()],200);
         }
        $sparePart_request = sparePart_requests::find($request->requestid);
        if($sparePart_request){
            $sparePart_request->status = 5; // mean request is cancelled
            $sparePart_request->save();
            $new_cancelled_requests = new cancelled_requests();
            $new_cancelled_requests->request_id = $sparePart_request->id;
            $new_cancelled_requests->request_type = 'spares';
            $new_cancelled_requests->reason = $request->reason;
            $new_cancelled_requests->save();
            return response()->json(['status' => 'done' , 'message' => __('admin.Spare_parts.Spare_parts_request_cancelled')],200);
        }
    
    }
    
    public function getOfficeRequests($id = null){
         $bookings = rentalBookRequests::where(['office' => $id])->where(function($query){
                 $query->where('delivery_status','paid');
                 $query->orWhere('delivery_status','cash');
                 $query->orWhere('delivery_status','inprogress');
                 $query->orWhere('delivery_status','done');
             })->orderby('created_at','DESC')->get();
          return view('admin.Admin.bookings',compact('bookings'));
    }
    
    
    
    
    
    
    
    
}
