<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\front;

Route::get('/', function () {
    return view('index');
});
Route::get('/services', [front::class, 'services'])->name('front.services.show');
Route::get('/office/{id}', [front::class, 'office']);
Route::group(['prefix' => 'car/rental'], function () {
    Route::get('/', [front::class, 'searchCar'])->name('rental.show');
    Route::get('/{id?}', [front::class, 'singleCar'])->name('rental.show.single.car');
    Route::post('/book/init/{id}', [front::class, 'bookCarStepOne'])->name('rental.book.car.firstStep');
    Route::get('/book/review/{id}', [front::class, 'bookCarStepTwo'])->name('rental.book.stepTwo');
    Route::get('/book/confirm/{id}', [front::class, 'bookCarStepThree'])->name('rental.book.stepThree');
    Route::post('/book/confirm/{id}', [front::class, 'bookCarConfirmandPay'])->name('rental.book.confirmandPay');
    Route::get('/book/success/{id}', [front::class, 'bookCarlastStep'])->name('rental.book.success');
});
Route::post('ipnPaymentLisenter', [front::class, 'paymentListen'])->name('ipnListener');
Route::get('verifyPayment', [front::class, 'verifyPayment'])->name('verifyPayment');
Route::get('signup', [front::class, 'signup'])->name('signup.show');
Route::post('signup', [front::class, 'Dosignup'])->name('signup.post');
Route::get('login', [front::class, 'login'])->name('login.show');
Route::post('login', [front::class, 'Dologin'])->name('login.post');
Route::get('logout', [front::class, 'logout'])->name('logout');
Route::post('route/register', [front::class, 'route_register'])->name('register.route.before.login');

Route::group(['prefix' => 'profile'], function () {
    Route::get('mycars', [front::class, 'mycars'])->name('mycars');
    Route::get('', [front::class, 'myaccount'])->name('myaccount');
    Route::post('update', [front::class, 'updateAccount'])->name('updateAccount');
    Route::get('password/change', [front::class, 'myaccountChangePassword'])->name('myaccountChangePassword');
    Route::post('password/update', [front::class, 'myaccountChangePasswordupdate'])->name('myaccountChangePassword.update');
    Route::get('rental/booked', [front::class, 'bookedRentalCars'])->name('bookedRentalCars');
    Route::get('addCar', [front::class, 'profileaddCar'])->name('profile.addCar');
    Route::post('addCar', [front::class, 'profileInsertcar'])->name('profile.insertcar');
    Route::post('getBrands', [front::class, 'profilegetBrands'])->name('profile.get.brands');
    Route::post('getCats', [front::class, 'profilegetCats'])->name('profile.get.cats');
    Route::post('getGroups', [front::class, 'profilegetGroups'])->name('profile.get.groups');
    Route::post('getYears', [front::class, 'profilegetYears'])->name('profile.get.years');
    Route::post('getCars', [front::class, 'profilegetCars'])->name('profile.get.cars');
    Route::get('roadHelp/requests', [front::class, 'roadhelpmyRequests'])->name('roadhelp.myRequests');
    Route::get('carwash/requests', [front::class, 'carwashmyRequests'])->name('carwash.myRequests');
    Route::get('garage/requests', [front::class, 'garagemyRequests'])->name('garage.myRequests');
    Route::get('Spare_parts/requests', [front::class, 'Spare_partsmyRequests'])->name('Spare_parts.myRequests');
    Route::get('sell/new', [front::class, 'sellNew'])->name('sellNew');
    Route::get('sell/edite', [front::class, 'sellEdite'])->name('sellEdite');
    Route::get('sell/single', [front::class, 'sellSingle'])->name('sellSingle');
    Route::get('sell/packages', [front::class, 'sellPackages'])->name('sellPackages');
    Route::get('sell/all', [front::class, 'sellAll'])->name('sellAll');
});

Route::group(['prefix' => 'road'], function () {
    Route::get('help', [front::class, 'roadHelp'])->name('road.help.get');
    Route::post('help', [front::class, 'roadHelpFirstRequest'])->name('roadhelp.request.first');
    Route::get('help/request/{id}', [front::class, 'roadHelpFirstRequestShow'])->name('roadhelp.request.first.show');
    Route::get('help/request/review/{id}', [front::class, 'roadHelpRequestReview'])->name('roadhelp.request.review');
    Route::post('help/request/confirm/{id}', [front::class, 'roadHelpRequestConfirm'])->name('roadhelp.request.confirm');
    Route::get('help/request/success/{id}', [front::class, 'roadHelpRequestsuccess'])->name('roadhelp.request.success');

});

Route::group(['prefix' => 'carwash'], function () {
    Route::get('', [front::class, 'carwash'])->name('carwash.get');
    Route::post('', [front::class, 'carwashFirstRequest'])->name('carwash.request.first');
    Route::get('request/{id}', [front::class, 'carwashFirstRequestShow'])->name('carwash.request.first.show');
    Route::get('request/review/{id}', [front::class, 'carwashRequestReview'])->name('carwash.request.review');
    Route::post('request/confirm/{id}', [front::class, 'carwashRequestConfirm'])->name('carwash.request.confirm');
    Route::get('request/success/{id}', [front::class, 'carwashRequestsuccess'])->name('carwash.request.success');

});
Route::group(['prefix' => 'garage'], function () {
    Route::get('', [front::class, 'garage'])->name('garage.get');
    Route::post('', [front::class, 'garageFirstRequest'])->name('garage.request.first');
    Route::post('request/send/message/text', [front::class, 'garageFirstRequestAddMessageText'])->name('garage.request.folow-up.send.message');
    Route::post('request/get/Messages', [front::class, 'garageFirstRequestGetMessages'])->name('garage.request.folow-up.getMessages');
    Route::post('request/follow-up/check', [front::class, 'garageFirstRequestCheckFollowUp'])->name('garage.request.folow-up.check');
    Route::get('request/{id}', [front::class, 'garageFirstRequestShow'])->name('garage.request.first.show');
    Route::get('request/follow-up/{id}', [front::class, 'garageRequestFollow'])->name('garage.request.follow');
    Route::post('request/confirm/{id}', [front::class, 'garageRequestConfirm'])->name('garage.request.confirmOrPay');
    Route::get('request/success/{id}', [front::class, 'garageRequestsuccess'])->name('garage.request.success');

});

Route::group(['prefix' => 'Spare_parts'], function () {
    Route::get('', [front::class, 'Spare_parts'])->name('Spare_parts.get');
    Route::post('', [front::class, 'Spare_partsFirstRequest'])->name('Spare_parts.request.first');
    Route::get('request/{id}', [front::class, 'Spare_partsFirstRequestShow'])->name('Spare_parts.request.first.show');
    Route::get('request/confirm/{id}', [front::class, 'Spare_partsRequestConfirm'])->name('Spare_parts.request.confirmOrPay');
    Route::post('request/follow-up/check', [front::class, 'Spare_partsFirstRequestCheckFollowUp'])->name('Spare_parts.request.folow-up.check');
    Route::post('request/confirm/{id}', [front::class, 'Spare_partsRequestPay'])->name('Spare_parts.request.Pay');
    Route::get('request/success/{id}', [front::class, 'Spare_partsRequestsuccess'])->name('Spare_parts.request.success');
});

Route::group(['prefix' => 'sell'], function () {
    Route::get('', [front::class, 'sell'])->name('sell.get');
    Route::get('/cars', [front::class, 'sellSingle'])->name('sell.single');
});
