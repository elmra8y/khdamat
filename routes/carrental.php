<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\rental;
Route::get('login',[rental::class ,'login'])->name('admin.rental.login');
Route::post('login',[rental::class ,'Dologin'])->name('admin.rental.login.post');
Route::group(['prefix' => 'admin' , 'middleware' => 'rentalAuth'],function(){
    Route::get('/', function () {
    return view('admin.CarRental.index');
    })->name('admin.rental.main');
    Route::get('cars/add',[rental::class, 'addcars'])->name('rental.cars.add');
    Route::get('cars/show',[rental::class, 'showcars'])->name('rental.cars.show');
    Route::post('cars/add',[rental::class, 'postcars'])->name('rental.cars.post');
    Route::get('cars/edit/{id?}',[rental::class, 'editcars'])->name('rental.cars.edit');
    Route::post('cars/edit/{id?}',[rental::class, 'updatecars'])->name('rental.cars.update');
    Route::get('bookings',[rental::class, 'bookings'])->name('rental.bookings.show');
    Route::get('bookings/view/{id}',[rental::class, 'bookingsView'])->name('rental.bookings.view');
    Route::post('bookings/change',[rental::class, 'requestChangeStatus'])->name('rental.requestChangeStatus');
});

