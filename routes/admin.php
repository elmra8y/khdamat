<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\admin;
use App\Http\Middleware\adminAuth;
use Illuminate\Support\Facades\Hash;
Route::get('login',[admin::class ,'login'])->name('admin.login');
Route::post('login',[admin::class ,'Dologin'])->name('admin.login.post');
Route::get('/password', function () {
    return Hash::make('123456');
    })->name('admin.main');
Route::group(['middleware' => 'adminAuth'],function (){
    Route::get('/', function () {
    return view('admin.Admin.index');
    })->name('admin.main');

Route::group(['prefix' => 'carRental'],function (){
    /* sizes part start */
    Route::get('countries',[admin::class, 'countries'])->name('countries.show');
    Route::get('countries/add',[admin::class, 'addcountries'])->name('countries.add');
    Route::post('countries/add',[admin::class, 'postcountries'])->name('countries.post');
    Route::get('countries/edit/{id?}',[admin::class, 'editcountries'])->name('countries.edit');
    Route::post('countries/edit/{id?}',[admin::class, 'updatecountries'])->name('countries.update');
    /* sizes part end */
    /* brand part start */
    Route::get('brands',[admin::class, 'brands'])->name('brands.show');
    Route::get('brands/add',[admin::class, 'addbrands'])->name('brands.add');
    Route::post('brands/add',[admin::class, 'postbrands'])->name('brands.post');
    Route::get('brands/edit/{id?}',[admin::class, 'editbrands'])->name('brands.edit');
    Route::post('brands/edit/{id?}',[admin::class, 'updatebrands'])->name('brands.update');
    /* brand part end */
    /* category part start */
    Route::get('categories',[admin::class, 'categories'])->name('categories.show');
    Route::get('categories/add',[admin::class, 'addcategories'])->name('categories.add');
    Route::post('categories/add',[admin::class, 'postcategories'])->name('categories.post');
    Route::get('categories/edit/{id?}',[admin::class, 'editcategories'])->name('categories.edit');
    Route::post('categories/edit/{id?}',[admin::class, 'updatecategories'])->name('categories.update');
    /* category part end */
   /* group part start */
    Route::get('groups',[admin::class, 'groups'])->name('groups.show');
    Route::get('groups/add',[admin::class, 'addgroups'])->name('groups.add');
    Route::post('groups/add',[admin::class, 'postgroups'])->name('groups.post');
    Route::get('groups/edit/{id?}',[admin::class, 'editgroups'])->name('groups.edit');
    Route::post('groups/edit/{id?}',[admin::class, 'updategroups'])->name('groups.update');
    /* group part end */
    /* group part start */
    Route::get('cars',[admin::class, 'cars'])->name('cars.show');
    Route::get('cars/add',[admin::class, 'addcars'])->name('cars.add');
    Route::post('cars/add',[admin::class, 'postcars'])->name('cars.post');
    Route::get('cars/edit/{id?}',[admin::class, 'editcars'])->name('cars.edit');
    Route::post('cars/edit/{id?}',[admin::class, 'updatecars'])->name('cars.update');
    Route::post('get/brands/categories',[admin::class, 'getBrandsChildren'])->name('admin.rental.getBrandsChildren');
    Route::post('get/categories/groups',[admin::class, 'getCategoriesChildren'])->name('admin.rental.getCategoriesChildren');
    /* group part end */
      /* group part start */
    Route::get('offices',[admin::class, 'offices'])->name('offices.show');
    Route::get('offices/add',[admin::class, 'addoffices'])->name('offices.add');
    Route::post('offices/add',[admin::class, 'postoffices'])->name('offices.post');
    Route::get('offices/edit/{id?}',[admin::class, 'editoffices'])->name('offices.edit');
    Route::post('offices/edit/{id?}',[admin::class, 'updateoffices'])->name('offices.update');
    Route::get('offices/requests/{id?}',[admin::class, 'getOfficeRequests'])->name('offices.requests');
    /* group part end */
    });
    Route::group(['prefix' => 'Roadhelp'],function (){
         Route::get('services',[admin::class, 'roadhelpServicesGet'])->name('admin.roadhelp.services');
         Route::get('/add/services',[admin::class, 'roadhelpServicesAdd'])->name('admin.roadhelp.addService');
         Route::post('/add/services',[admin::class, 'roadhelpServicesAddInsert'])->name('admin.roadhelp.addNewService');
         Route::get('/requests',[admin::class, 'roadhelprequestsGet'])->name('admin.roadhelp.requests.show');
         Route::get('/requests/view/{id}',[admin::class, 'roadhelprequestView'])->name('admin.roadhelp.view.request');
         Route::post('/requests/updateStatus',[admin::class, 'roadhelprequestChangeStatus'])->name('admin.roadhelp.requestChangeStatus');
    });
     Route::group(['prefix' => 'CarWash'],function (){
         Route::get('services',[admin::class, 'carwashServicesGet'])->name('admin.carwash.services');
         Route::get('/add/services',[admin::class, 'carwashServicesAdd'])->name('admin.carwash.addService');
         Route::post('/add/services',[admin::class, 'carwashServicesAddInsert'])->name('admin.carwash.addNewService');
         Route::get('/requests',[admin::class, 'carwashrequestsGet'])->name('admin.carwash.requests.show');
         Route::get('/requests/view/{id}',[admin::class, 'carwashrequestView'])->name('admin.carwash.view.request');
         Route::post('/requests/updateStatus',[admin::class, 'carwashrequestChangeStatus'])->name('admin.carwash.requestChangeStatus');
    });
       Route::group(['prefix' => 'garage'],function (){
         Route::get('services',[admin::class, 'garageServicesGet'])->name('admin.garage.services');
         Route::get('/add/services',[admin::class, 'garageServicesAdd'])->name('admin.garage.addService');
         Route::post('/add/services',[admin::class, 'garageServicesAddInsert'])->name('admin.garage.addNewService');
         Route::get('/requests',[admin::class, 'garagerequestsGet'])->name('admin.garage.requests.show');
         Route::get('/requests/view/{id}',[admin::class, 'garagerequestView'])->name('admin.garage.view.request');
         Route::post('/requests/updateStatus',[admin::class, 'garagerequestChangeStatus'])->name('admin.garage.requestChangeStatus');
         Route::post('request/send/message/text',[admin::class , 'garageFirstRequestAddMessageText'])->name('admin.garage.request.folow-up.send.message');
         Route::post('request/get/Messages',[admin::class , 'garageFirstRequestGetMessages'])->name('admin.garage.request.folow-up.getMessages');
         Route::post('request/add/price',[admin::class , 'garageFirstRequestassignPrice'])->name('admin.garage.request.assignPrice');
    });
    
          Route::group(['prefix' => 'Spare_parts'],function (){
         Route::get('services',[admin::class, 'carwashServicesGet'])->name('admin.carwash.services');
         Route::get('/add/services',[admin::class, 'carwashServicesAdd'])->name('admin.carwash.addService');
         Route::post('/add/services',[admin::class, 'carwashServicesAddInsert'])->name('admin.carwash.addNewService');
         Route::get('/requests',[admin::class, 'Spare_partsrequestsGet'])->name('admin.Spare_parts.requests.show');
         Route::get('/requests/view/{id}',[admin::class, 'Spare_partsrequestView'])->name('admin.Spare_parts.view.request');
         Route::post('/requests/updateStatus',[admin::class, 'carwashrequestChangeStatus'])->name('admin.carwash.requestChangeStatus');
         Route::post('request/add/price',[admin::class , 'Spare_partsFirstRequestassignPrice'])->name('admin.Spare_parts.request.assignPrice');
         Route::post('request/cancel',[admin::class , 'Spare_partsFirstRequestcancel'])->name('admin.Spare_parts.request.cancel');
    });
        });
Route::get('password',function(){
    return \Hash::check('111111111','$2y$10$JRth7p5w8FLYsVcNvaak0uGtwvz6FLgesgRlt23NbrlEdLDBa1uTy');
});


