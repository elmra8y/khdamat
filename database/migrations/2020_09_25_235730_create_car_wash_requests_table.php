<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarWashRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_wash_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user')->nullable();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('car')->nullable();
            $table->foreign('car')->references('id')->on('users_cars')->onDelete('cascade');
            $table->string('type'); //if 0 mean custom service type , 1 mean have service from admin services
            $table->unsignedBigInteger('service')->nullable();
            $table->foreign('service')->references('id')->on('car_wash_services')->onDelete('cascade');
            $table->string('status')->default(0); // 0 mean inital request
            $table->longText('description')->nullable();
            $table->decimal('long', 10, 7)->nullable();
            $table->decimal('lat', 10, 7)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_wash_requests');
    }
}
