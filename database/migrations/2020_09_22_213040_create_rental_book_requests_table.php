<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalBookRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_book_requests', function (Blueprint $table) {
            $table->id();
            $table->enum('request_type',['rental']);
            $table->date('request_date_from')->nullable();
            $table->date('request_date_to')->nullable();
            $table->time('request_datetime_start')->nullable();
            $table->time('request_datetime_back')->nullable();
            $table->unsignedBigInteger('car')->nullable();
            $table->foreign('car')->references('id')->on('rental_cars')->onDelete('cascade');
            $table->unsignedBigInteger('user')->nullable();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('office')->nullable();
            $table->foreign('office')->references('id')->on('rental_users')->onDelete('cascade');
            $table->string('delivery_status')->default('init');
            $table->decimal('delivery_address_lng', 10, 7)->nullable();
            $table->decimal('delivery_address_lat', 10, 7)->nullable();
            $table->decimal('back_delivery_address_lng', 10, 7)->nullable();
            $table->decimal('back_delivery_address_lat', 10, 7)->nullable();
            $table->string('delivery_address')->nullable();
            $table->string('back_delivery_address')->nullable();
            $table->integer('days')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_book_requests');
    }
}
