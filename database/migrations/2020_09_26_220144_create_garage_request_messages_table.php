<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGarageRequestMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garage_request_messages', function (Blueprint $table) {
            $table->id();
            $table->string('parent');
            $table->string('child');
            $table->string('parentModel');
            $table->unsignedBigInteger('request')->nullable();
            $table->foreign('request')->references('id')->on('garage__requests')->onDelete('cascade');
            $table->string('message');
            $table->string('type');  // text , image , video  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('garage_request_messages');
    }
}
