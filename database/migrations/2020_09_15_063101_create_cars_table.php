<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('name_en');
            $table->string('name_ar');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('country')->nullable();
            $table->foreign('country')->references('id')->on('cat_country')->onDelete('cascade');
            $table->unsignedBigInteger('brand')->nullable();
            $table->foreign('brand')->references('id')->on('cat_brands')->onDelete('cascade');
            $table->unsignedBigInteger('category')->nullable();
            $table->foreign('category')->references('id')->on('cat_categories')->onDelete('cascade');
            $table->string('parent')->nullable();
            $table->string('parent_model')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
