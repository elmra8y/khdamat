<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentalCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_cars', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('car')->nullable();
            $table->foreign('car')->references('id')->on('cars')->onDelete('cascade');
            $table->unsignedBigInteger('office')->nullable();
            $table->foreign('office')->references('id')->on('rental_users')->onDelete('cascade');
            $table->unsignedBigInteger('country')->nullable();
            $table->foreign('country')->references('id')->on('cat_country')->onDelete('cascade');
            $table->unsignedBigInteger('brand')->nullable();
            $table->foreign('brand')->references('id')->on('cat_brands')->onDelete('cascade');
            $table->unsignedBigInteger('category')->nullable();
            $table->foreign('category')->references('id')->on('cat_categories')->onDelete('cascade');
            $table->unsignedBigInteger('color')->nullable();
            $table->foreign('color')->references('id')->on('cars_colors')->onDelete('cascade');
            $table->string('year')->nullable();
            $table->float('price');
            $table->longText('description');
            $table->integer('num');
            $table->enum('status',['available','notavailable'])->default('available');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_cars');
    }
}
