<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CreateAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\admins::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => \Hash::make('123456'),
            
        ]);
    }
}
