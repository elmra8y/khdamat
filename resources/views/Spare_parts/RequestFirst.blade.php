@include('rental.header')
@include('rental.nav')

          <style type="text/css">
    .active .bs-stepper-circle{
      background-color: var(--main-color-light)
    }
  </style>
   
      <div class="container">
        <div class="order_details_box">
        


          <div class="row">
            <div class="bs-stepper linear w-100">
              <div class="bs-stepper-header" role="tablist">
                <!-- your steps here -->
                <div class="step" data-target="#init-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="init-part" id="init-part-trigger">
                    <span class="bs-stepper-circle">1</span>
                  </button>
                </div>
                
                <div class="line"></div>
                <div class="step" data-target="#review-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="secont-part" id="review-part-trigger">
                    <span class="bs-stepper-circle">2</span>
                  </button>
                </div>
                
                <div class="line"></div>
                <div class="step" data-target="#confirm-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="secont-part" id="confirm-part-trigger">
                    <span class="bs-stepper-circle">3</span>
                  </button>
                </div>
              </div>

              <div class="bs-stepper-content p-0">
                <!-- your steps content here -->

                  @if($sparePart_request->status == 1)
            <form action="{{route('Spare_parts.request.Pay',$sparePart_request->id)}}" method="post">
              @csrf()
          @endif
                <div id="init-part" class="content active dstepper-block" role="tabpanel" aria-labelledby="init-part-trigger">
                  @if($sparePart_request->status == 0 || $sparePart_request->status == 1 || $sparePart_request->status == 2)
                <h4 class="order_details_txt">تفاصيل الطلب</h4>
            <p class="service_type">{{__('front.Spare_parts.service')}} : <span>{{__('front.Spare_parts.serviceTitle')}}</span></p>
                <p class="car_info">{{__('front.Spare_parts.mycars')}} : <span>{{$sparePart_request->carObj->carObj->name}}</span></p>
             <p class="car_info">{{__('front.Spare_parts.spare_part_name')}} : <span>{{$sparePart_request->spartName}}</span></p>
                <p class="order_date">{{__('front.Spare_parts.requestDate')}} : <span>{{\Carbon\Carbon::parse($sparePart_request->created_at)->format('d / m / Y')}}</span></p>
             @if($sparePart_request->status == 1) <p class="service_type">{{__('front.Spare_parts.servicePrice')}} : <span>{{$sparePart_request->amount}} / {{__('front.KWD')}}</span></p> @endif
             @if($sparePart_request->status == 3) <p class="service_type">{{__('front.Spare_parts.servicePricePaid')}} : <span>{{$sparePart_request->amount}} / {{__('front.KWD')}}</span></p> @endif
            @if($sparePart_request->status == 0)    
            <p class="order_waitadminResponse"> <i class="fa fa-spinner fa-spin"></i> <span>{{__('front.Spare_parts.waitForResponse')}}</span></p>
            @endif
                 @if($sparePart_request->status == 1) <p class="service_type">{{__('front.Spare_parts.paymentWay')}} :</p>
            <div class="paymentChoise">
              <div class="form-check">
              <input class="form-check-input" type="radio" name="paymentType" id="keynet" value="keynet" checked>
              <label class="form-check-label" for="keynet">
                {{__('front.Spare_parts.kenet')}}
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="paymentType" id="cash" value="cash">
              <label class="form-check-label" for="cash">
                {{__('front.Spare_parts.cash')}}
              </label>
            </div>
            </div>
            @endif
                        @elseif($sparePart_request->status  != 0 && $sparePart_request->status  != 1 && $sparePart_request->status  != 5)
                      <h4 class="order_details_txt">اكتمال الطلب</h4>
                     <div class="rental_conditions">تم اكتمال طلبك بنجاح .</div>
                    @elseif($sparePart_request->status == 5)
                      <h4 class="order_details_txt">اكتمال الطلب</h4>
                     <div class="rental_conditions">تم الغاء الطلب .</div>
                  @endif
                  <!-- STATUS -->
                  <div class="col-12 steps">
                    <div class="mb-2 d-flex align-items-center">
                      <span class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="var(--main-color-light)" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_material-done" data-name="Icon material-done" d="M10.827,19.241l-4.3-4.3L5.1,16.377,10.827,22.1,23.1,9.832,21.668,8.4Z" transform="translate(1465.9 314.6)" fill="var(--main-color-light)"/>
                          </g>
                        </svg>
                           
                      </span>
                      <span class="mx-2"></span>
                      <span class="h5">
                        {{__('front.rental.request_received')}}
                      </span>
                    </div>

                     <div class="mb-2 d-flex align-items-center">
                    <span class="icon">
                         @if($sparePart_request->status  != 0)
                          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="var(--main-color-light)" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_material-done" data-name="Icon material-done" d="M10.827,19.241l-4.3-4.3L5.1,16.377,10.827,22.1,23.1,9.832,21.668,8.4Z" transform="translate(1465.9 314.6)" fill="var(--main-color-light)"/>
                          </g>
                        </svg>
                          @else
                           <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="#4a4d51" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_open-question-mark" data-name="Icon open-question-mark" d="M4.97,0A5.2,5.2,0,0,0,1.187,1.328,4.39,4.39,0,0,0,0,3.9l2.012.262a2.438,2.438,0,0,1,.624-1.388A2.988,2.988,0,0,1,4.97,2.012,3.55,3.55,0,0,1,7.425,2.7a1.682,1.682,0,0,1,.563,1.328c0,1.67-.684,2.133-1.69,3.018a5.735,5.735,0,0,0-2.334,4.527v.5H5.976v-.5c0-1.67.624-2.133,1.63-3.018A5.77,5.77,0,0,0,10,4.024,3.865,3.865,0,0,0,8.813,1.187,5.561,5.561,0,0,0,4.97,0ZM3.964,14.085V16.1H5.976V14.085Z" transform="translate(1475 322)" fill="#4a4d51"/>
                          </g>
                        </svg>
                          @endif
                      </span>
                      <span class="mx-2"></span>
                      <span class="h5">
                        {{__('front.rental.request_confirm_and_pay')}}
                      </span>
                    </div>
                       <div class="mb-2 d-flex align-items-center">
                      <span class="icon">
                           @if($sparePart_request->status  != 0 && $sparePart_request->status  != 1)
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="var(--main-color-light)" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_material-done" data-name="Icon material-done" d="M10.827,19.241l-4.3-4.3L5.1,16.377,10.827,22.1,23.1,9.832,21.668,8.4Z" transform="translate(1465.9 314.6)" fill="var(--main-color-light)"/>
                          </g>
                        </svg>
                         @else
                           <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="#4a4d51" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_open-question-mark" data-name="Icon open-question-mark" d="M4.97,0A5.2,5.2,0,0,0,1.187,1.328,4.39,4.39,0,0,0,0,3.9l2.012.262a2.438,2.438,0,0,1,.624-1.388A2.988,2.988,0,0,1,4.97,2.012,3.55,3.55,0,0,1,7.425,2.7a1.682,1.682,0,0,1,.563,1.328c0,1.67-.684,2.133-1.69,3.018a5.735,5.735,0,0,0-2.334,4.527v.5H5.976v-.5c0-1.67.624-2.133,1.63-3.018A5.77,5.77,0,0,0,10,4.024,3.865,3.865,0,0,0,8.813,1.187,5.561,5.561,0,0,0,4.97,0ZM3.964,14.085V16.1H5.976V14.085Z" transform="translate(1475 322)" fill="#4a4d51"/>
                          </g>
                        </svg>
                          @endif
                      </span>
                      <span class="mx-2"></span>
                      <span class="h5">
                        {{__('front.rental.request_complete')}}
                      </span>
                    </div>
                  </div>
                  <!-- END STATUS -->

                  <div class="col-12 d-flex justify-content-center">
                      @if($sparePart_request->status  == 1)
                          <button type="submit" class="btn mt-5 px-5 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</button>
                      @endif
                  </div>
                </div>
                <!-- End Init Part -->



          @if($sparePart_request->status == 1)
            </form>
          @endif
        </div>
      </div>
          </div>
            </div></div></div>
     @extends('rental.footer')
@section('script')
<script>
    $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
</script>
@if($sparePart_request->status == 0)
<script>
 setInterval(function(){
         $.ajax({
        url: "{!! route('Spare_parts.request.folow-up.check') !!}",
        type: 'post',
        dataType: 'json',
        data: {requestid : {!! $sparePart_request->id !!}},
        success: function(data) {
           
            if(data.status == 'done'){
                window.location.href = "{!! route('Spare_parts.request.confirmOrPay',$sparePart_request->id) !!}";
            }
            if(data.status == 'cancelled'){
                $('.order_waitadminResponse').remove();
                $('.steps').remove();
                $('.order_date').after('<p class="order_date">'+"{!! __('front.Spare_parts.request_cancelled') !!}"+'</p>');
            }
         }
         });
    },3000);
</script>
@endif
@endsection
