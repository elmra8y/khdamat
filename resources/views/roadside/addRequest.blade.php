@include('rental.header')
        
    @include('rental.nav')

    <div class="home full position-relative">
      <div class="overlay">
        <div class="overlay_content">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-8 col-md-10 animate__animated animate__fadeInRight">
                <div class="py-4 px-4 my-5 my-lg-0 bg-white text-right" style="border-radius: 30px">
                  <div class="col-12 px-0">
                    <h6 class="mb-3 mt-3 font-weight-bold text-dark px-2">{{__('front.roadhelp.serviceTitle')}}</h6>
                  </div>
                  <div class="px-2">
                    <div class="col-12 px-0 mb-3 mt-4">
                      <form method="POST" action="{{route('roadhelp.request.first')}}">
                        {{csrf_field()}}
                        @if($errors->any())
                        <div class="alert alert-danger danger-errors">
                        @foreach ($errors->all() as $error)
                          <p>{{ $error }}</p>
                        @endforeach
                        </div>
                        @endif
                        @if(session()->has('message'))
                        <div class="alert alert-success danger-errors">
                          <p>{{session()->get('message')}}</p>
                        </div>
                        @endif


                        <div class="bs-stepper linear">
                          <div class="bs-stepper-header" role="tablist">
                            <!-- your steps here -->
                            <div class="step" data-target="#first-part">
                              <button type="button" class="step-trigger d-none" role="tab" aria-controls="first-part" id="first-part-trigger">
                                <span class="bs-stepper-circle">1</span>
                              </button>
                            </div>
                            <div class="line d-none"></div>
                            <div class="step" data-target="#second-part">
                              <button type="button" class="step-trigger d-none" role="tab" aria-controls="secont-part" id="secont-part-trigger">
                                <span class="bs-stepper-circle">2</span>
                              </button>
                            </div>
                          </div>

                          <div class="bs-stepper-content p-0">
                            <!-- your steps content here -->

                            <div id="first-part" class="content" role="tabpanel" aria-labelledby="first-part-trigger">
                              <div class="form-group">
                                <label for="car">{{__('front.roadhelp.mycars')}}</label>
                                <select class="form-control" id="car" name="car">
                                  <option>{{__('front.roadhelp.chooseChar')}}</option>
                                  @foreach(auth()->guard('users')->user()->usersCars as $car)
                                  <option value="{{$car->id}}">{{$car->carObj->name}}</option>
                                  @endforeach
                                  <option value="addcarToProfile">{{__('front.roadhelp.addNewCar')}}</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="service">{{__('front.roadhelp.serviceType')}}</label>
                                <select class="form-control" id="service" name="service">
                                  <option>{{__('front.roadhelp.chooseServiceType')}}</option>
                                @foreach($roadHelpServices as $service)
                                  <option value="{{$service->id}}">{{$service->name}}</option>
                                @endforeach
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="serviceDesc">{{__('front.roadhelp.requestDesc')}}</label>
                                  <textarea class="form-control" id="serviceDesc" rows="6" name="description"></textarea>
                              </div>

                              <div class="form-group d-flex justify-content-end">
                                <button type="button" class="btn btn-sm  px-3 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</button>
                              </div>
                              <div id="car_response" style="display:none"></div>
                            </div>

                            <div id="second-part" class="content" role="tabpanel" aria-labelledby="second-part-trigger">
                              <fieldset class="row mb-3">
                                <legend class="col-12 h6 mb-0 font-weight-bold">{{__('front.rental.Address')}}</legend>
                                <div class="col-lg-4">
                                  <div class="form-group">
                                    <label for="bookingTimeReturn">{{__('front.rental.Governorate')}}</label>
                                    <input type="text" class="form-control"  name="Governorate">
                                  </div>
                                </div>  
                                <div class="col-lg-4">
                                  <div class="form-group">
                                    <label for="bookingTimeReturn">{{__('front.rental.Region')}}</label>
                                    <input type="text" class="form-control"  name="Region">
                                  </div>
                                </div>
                        
                           
                                <div class="col-lg-4">
                                  <div class="form-group">
                                    <label for="bookingTimeReturn">{{__('front.rental.piece')}}</label>
                                    <input type="text" class="form-control"  name="piece">
                                  </div>
                                </div>
                        
                           
                                <div class="col-lg-4">
                                  <div class="form-group">
                                    <label for="bookingTimeReturn">{{__('front.rental.street')}}</label>
                                    <input type="text" class="form-control"  name="street">
                                  </div>
                                </div>
                               
                                <div class="col-lg-4">
                                  <div class="form-group">
                                    <label for="bookingTimeReturn">{{__('front.rental.Avenue')}}</label>
                                    <input type="text" class="form-control"  name="Avenue">
                                  </div>
                                </div>

                                <div class="col-lg-4">
                                  <div class="form-group">
                                    <label for="bookingTimeReturn">{{__('front.rental.Home')}}</label>
                                    <input type="text" class="form-control"  name="Home">
                                  </div>
                                </div>

                                <div class="col-lg-12">
                                  <div class="form-group">
                                    <label for="bookingTimeReturn">{{__('front.rental.Notes')}}</label>
                                    <textarea class="form-control"  name="Notes"></textarea>
                                  </div>
                                </div>
                              </fieldset>
                              <fieldset class="row jumbotron">
                                <legend class="col-12 h6 mb-0 font-weight-bold">{{__('front.rental.or_share_location')}}</legend>
                                <div class="col-12  text-center">
                                  <div class="form-group mt-2">
                                    <button type="button" id="bookingLocationPopUp" class="btn btn-sm alert-info">
                                      {{__('front.rental.bookingLocation')}}
                                      <span class="icon">
                                        <svg width="14px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 512 512" xml:space="preserve">
                                          <g>
                                            <g>
                                              <path fill="#0c5460" d="M256,0C161.896,0,85.333,76.563,85.333,170.667c0,28.25,7.063,56.26,20.49,81.104L246.667,506.5
                                                c1.875,3.396,5.448,5.5,9.333,5.5s7.458-2.104,9.333-5.5l140.896-254.813c13.375-24.76,20.438-52.771,20.438-81.021
                                                C426.667,76.563,350.104,0,256,0z M256,256c-47.052,0-85.333-38.281-85.333-85.333c0-47.052,38.281-85.333,85.333-85.333
                                                s85.333,38.281,85.333,85.333C341.333,217.719,303.052,256,256,256z"/>
                                            </g>
                                          </g>
                                        </svg>
                                      </span>
                                    </button>
                                  </div>
                                </div>
                              </fieldset>

                              <input name="lat" hidden="hidden" id="lat">
                              <input name="lng" hidden="hidden" id="lng"> 
                              <div class="clear"></div>


                              <div class="form-group d-flex justify-content-between">
                                <button type="button" class="btn btn-sm px-3 bs-stepper-prev rounded">{{__('front.prev')}}</button>
                                <button type="submit" class="btn btn-sm search_car_btn rounded saveBtnRight">{{__('front.send')}}</button>
                                <!-- <div class="clear"></div> -->
                              </div>
                            </div>

                          </div>
                        </div>
                      </form>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer_home">
         <div class="container">
            <div class="row">
              <div class="col-lg-6">
                <ul class="foot_nav">
                  <li>عن الشركة</li>
                  <li>رؤيتنا</li>
                  <li>رسالتنا</li>
                  <li>الاسئلة المتكررة</li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="social_list">
                  <li>Facebook</li>
                  <li>Instgram</li>
                  <li>Twitter</li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>



<div class="modal" tabindex="-1" role="dialog" id="addNewCar">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
         <form method="POST" class="df-personal-info" id="profileAddCar">
      <div class="modal-header">
        <h5 class="modal-title">{{__('front.roadhelp.addNewCar')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
                            <div class="form-group">
                                  <label for="size">{{__('profile.country')}}</label>
                                  <select class="form-control" id="country" name="country">
                                          <option>{{__('profile.chooseCountry')}}</option>
                                      @foreach($countries as $country)
                                          <option value="{{$country->countryObj->id}}">{{$country->countryObj->name}}</option>
                                      @endforeach
                                  </select>
                            </div>
                                <div class="form-group">
                                      <label for="brand">{{__('profile.brand')}}</label>
                                      <select class="form-control" id="brand" name="brand">
                                          <option>{{__('profile.chooseBrand')}}</option>
                                      </select>
                            </div>
                            <div class="form-group">
                                      <label for="category">{{__('profile.category')}}</label>
                                      <select class="form-control" id="category" name="category">
                                        <option>{{__('profile.chooseCategory')}}</option>
                                      </select>
                        </div>
          @php 
          $carsColors = \App\Models\carsColors::all();
          @endphp
                           
                            <div class="form-group">
                                 <label for="category">{{__('profile.year')}}</label>
                              <select class="form-control" name="year">
                                <option value="">{{__('profile.chooseYear')}}</option>
                                     @for($i = 2000 ; $i <= \Carbon\Carbon::now()->format('Y'); $i++)
                                      <option value="{{$i}}" @if(Request::get("year") == $i) selected @endif>
                                         {{$i}}
                                      </option>
                                      @endfor
                              </select>
                            </div>
                         
                            <div class="form-group">
                                 <label for="category">{{__('profile.color')}}</label>
                              <select class="form-control" name="color">
                                <option value="">{{__('profile.chooseColor')}}</option>
                                      @foreach($carsColors as $color)
                                      <option value="{{$color->id}}" @if(Request::get("color") == $color->id) selected @endif>
                                         {{$color->name}}
                                      </option>
                                  @endforeach
                              </select>
                            </div>
                       
                      <div class="form-group">
                          <label for="car">{{__('profile.car')}}</label>
                          <select class="form-control car" name="car">
                           <option>{{__('profile.chooseCar')}}</option>
                            </select>
                </div>
		<div class="car_response" style="display:none"></div>

    
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">{{__('sign.save')}}</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('sign.cancel')}}</button>
      </div>
             </form>
    </div>
  </div>
</div>


 <div class="modal fade" id="place" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{__('front.roadhelp.addPlace')}}</h4>
        </div>
        <div class="modal-body">
              <div id="pac-container">
        <input id="pac-input" type="text"
            placeholder="{{__('front.rental.Enteralocation')}}"><div id="location-error"></div>
      </div>
            <div id="map" class="map_deliver_location"></div>
             <button onclick="getLocation()" class="btn btn-light" style="height: 42px;margin-top: 20px;" type="button">
        <img draggable="false" height="22" src="{{asset('assets/images/location.png')}}" width="22"> {{__('front.rental.DetectMyLocation')}}</button>
    </div>
   
        </div>
      </div>
    </div>
 <div class="modal fade" id="place_detected" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
              <h5 class="place_detected_h5">{{__('front.place_detected')}}</h5>
            <i class="fa fa-check-circle place_detected_icon"></i>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('front.close')}}</button>
      </div>
        </div>
      </div>
    </div>
  
  @extends('rental.footer')
  @section('script')
  <script>
        $('#bookingLocationPopUp').focusin(function(){
          $('#place').modal('show');
      });
  $('#car').change(function(){
      if( $(this).val() == 'addcarToProfile'){
          $('#addNewCar').modal('show');
      }
  });
       var language = '{!! app()->getLocale() !!}';
       $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });
      $('#country').change(function (){
           $.ajax({
          url: "{!! route('profile.get.brands') !!}",
          type: 'post',
          dataType: 'json',
          data: {country : $('#country').val()},
          success: function(data) {
              if(data.status == 'done'){
                      var html = '<option>'+'{!! __("profile.chooseBrand") !!}'+'</option>';
                      data.brands.forEach(function(brand){
                          html += '<option value="'+brand.brand_obj.id+'">'+brand.brand_obj['name_' +language]+'</option>';
                      });
                      $('#brand').html(html);
              }
            
          
           }
           });
      });
      $('#brand').change(function (){
           $.ajax({
          url: "{!! route('profile.get.cats') !!}",
          type: 'post',
          dataType: 'json',
          data: { brand : $('#brand').val()},
          success: function(data) {
              if(data.status == 'done'){
              var html = '<option>'+'{!! __("profile.chooseCategory") !!}'+'</option>';
              data.categories.forEach(function(cat){
                  html += '<option value="'+cat.category_obj.id+'">'+cat.category_obj['name_' +language]+'</option>';
              });
              $('#category').html(html);
              }
           }
           });
      });
      $('#category').change(function (){
           $.ajax({
          url: "{!! route('profile.get.cars') !!}",
          type: 'post',
          dataType: 'json',
          data: {country : $('#country').val() , brand : $('#brand').val() , category : $('#category').val()},
          success: function(data) {
              if(data.status == 'done'){
              var html = '<option>'+'{!! __("profile.chooseCar") !!}'+'</option>';
              data.cars.forEach(function(car){
                  html += '<option value="'+car.id+'">'+car['name_' +language]+'</option>';
              });
              $('.car').html(html);
              console.log(html);
              }
           }
           });
      });
      $('#profileAddCar').submit(function(e){
          e.preventDefault();
          $('.car_response').html('').hide();
            $.ajax({
          url: "{!! route('profile.insertcar') !!}",
          type: 'post',
          dataType: 'json',
          data: $('#profileAddCar').serialize(),
          success: function(data) {
              if(data.status == 'failed'){
                                 var html= '';
                                 data.errors.forEach(function(error){
                                     html += "<p>"+error+"</p>";
                                 });
                                 $('.car_response').html("<div class='alert alert-danger danger-errors'>"+html+"</div>").show();
                             }
                               if(data.status == 'done'){
                                 $('.car_response').html("<div class='alert alert-success danger-errors'><p>"+data.message+"</p></div>").show();
                                 $('#profileAddCar').trigger('reset');   
                                   setTimeout(function(){window.location.href = '{!! route("road.help.get") !!}';},2000);
                             }
           }
           });
      });
  </script>
  <script>
         var marker;
      var map;
      function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position){
          var lat = document.getElementById('lat');
          var lng = document.getElementById('lng');
          lat.value = position.coords.latitude;
          lng.value = position.coords.longitude;
          var centerCoordinates = {lat: position.coords.latitude , lng: position.coords.longitude};
          map = new google.maps.Map(document.getElementById('map'), {
          center: centerCoordinates,
          zoom: 12
          });
          marker = new google.maps.Marker({
              position : centerCoordinates,
            map: map , draggable: true
          });
           google.maps.event.addListener(marker, 'dragend', function (event) {
                              var lat = document.getElementById('lat');
                              var lng = document.getElementById('lng');
                              lat.value = this.getPosition().lat();
                              lng.value = this.getPosition().lng();

  });
           $('#place').modal('hide');
          $('#place_detected').modal('show');
      });
    } else { 
      x.innerHTML = "Geolocation is not supported by this browser.";
    }
  }
      function initMap() {
      	var centerCoordinates = {lat: 29.3279856 , lng: 47.7808492};
          map = new google.maps.Map(document.getElementById('map'), {
          center: centerCoordinates,
          zoom: 12
          });
     
          var card = document.getElementById('pac-card');
          var input = document.getElementById('pac-input');
          
          map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
         

          var autocomplete = new google.maps.places.Autocomplete(input);

         
          
           marker = new google.maps.Marker({
              position : centerCoordinates,
            map: map , draggable: true
          });
     

          autocomplete.addListener('place_changed', function() {
   	        document.getElementById("location-error").style.display = 'none';
              marker.setVisible(false);
          		var place = autocomplete.getPlace();
          		if (!place.geometry) {
          		  	document.getElementById("location-error").style.display = 'inline-block';
          		  	document.getElementById("location-error").innerHTML = "Cannot Locate '" + input.value + "' on map";
          		    return;
          		}
          		
          		map.fitBounds(place.geometry.viewport);
          		marker.setPosition(place.geometry.location);
          		marker.setVisible(true);
                  var lat = document.getElementById('lat');
                  var lng = document.getElementById('lng');
                  lat.value = place.geometry.location.lat();
                  lng.value = place.geometry.location.lng();
          });
                  google.maps.event.addListener(marker, 'dragend', function (event) {
                              var lat = document.getElementById('lat');
                              var lng = document.getElementById('lng');
                              lat.value = this.getPosition().lat();
                              lng.value = this.getPosition().lng();

  });
    
      }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key={{env('MAP')}}&libraries=places&callback=initMap" async defer></script>
@endsection