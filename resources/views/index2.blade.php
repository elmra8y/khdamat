<!DOCTYPE html>
<html style="direction: rtl;">
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('/assets')}}/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{asset('/assets')}}/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{asset('/assets')}}/css/animate.min.css">
        <link rel="stylesheet" href="{{asset('/assets')}}/css/style.css">
        <meta name="csrf-token" content="{{csrf_token()}}">
    </head>
    <body class="home {{app()->getLocale()}}">
       <header class="leftside">
          <div class="topMenu"><img src="{{asset('/assets')}}/images/menu.png"></div>
         <div class="bottomMenu">
            <ul class="socialnav">
           <li>Facebook</li>
           <li>Instgram</li>
           <li>Twitter</li>
           </ul>
           </div>
        </header>
        <div class="rightSide">
                    <nav class="nav-custom-style">
              <span class="navbar-brand services_drop" href="#">خدماتنا 
                  <div class="main_drop_container">
                   <ul>
                     <li><a href="{{route('rental.show')}}">{{__('front.rental part')}}</a></li>
                     <li><a href="{{route('road.help.get')}}">{{__('front.help')}}</a></li>
                     <li><a href="{{route('carwash.get')}}">{{__('front.car wash')}}</a></li>
                          <li><a href="{{route('garage.get')}}">{{__('front.garage_text')}}</a></li>
                          <li><a href="{{route('Spare_parts.get')}}">{{__('front.Spare_parts_text')}}</a></li>
                                          </ul>
                </div>
                  </span>
              <a class="navbar-brand" href="#">طلب سريع</a>
            <div class="profile_clickable">
            <img src="{{asset('/assets')}}/images/user.png">
                 <div class="profile_dropdown">
                   <ul>
                       @if(auth()->guard('users')->check())
                   <li><a href="{{route('myaccount')}}">{{__('profile.my_profile')}}</a></li>
                   <li><a href="{{route('logout')}}">{{__('profile.logout')}}</a></li>
                       @else
                        <li><a href="{{route('login.show')}}">{{__('profile.login')}}</a></li>
                        <li><a href="{{route('signup.show')}}">{{__('profile.signup')}}</a></li>
                       @endif
                   </ul>
                </div> 
            </div>
            <div class="clear"></div>
</nav>
            
            <ul class="allservices_ul_home_page">
                      <li>
                          <a @if(auth()->guard('users')->check()) href="{{route('rental.show')}}" @else data-toggle="modal" data-target="#login_alert" data="rental.show" @endif>
                         <img src="{{asset('/assets')}}/images/rental.png" class="services_img rental">
                          <p>{{__('front.home.rental')}}</p>
                          </a>
                         </li>
                         <li>
                             <a  @if(auth()->guard('users')->check()) href="{{route('road.help.get')}}" @else data-toggle="modal" data-target="#login_alert" data="road.help.get" @endif>
                         <img src="{{asset('/assets')}}/images/roadHelp.png" class="services_img roadHelp">
                          <p>{{__('front.home.roadHelp')}}</p>
                             </a>
                         </li>
                         <li>
                             <a  @if(auth()->guard('users')->check()) href="{{route('carwash.get')}}" @else data-toggle="modal" data-target="#login_alert" data="carwash.get" @endif>
                         <img src="{{asset('/assets')}}/images/wash.png" class="services_img wash">
                          <p>{{__('front.home.wash')}}</p>
                             </a>
                         </li>
                         <li>
                             <a @if(auth()->guard('users')->check()) href="{{route('garage.get')}}" @else data-toggle="modal" data-target="#login_alert" data="garage.get" @endif>
                         <img src="{{asset('/assets')}}/images/car-repair.png" class="services_img car-repair">
                          <p>{{__('front.home.repair')}}</p>
                             </a>
                         </li>
                         <li>
                             <a  @if(auth()->guard('users')->check()) href="{{route('Spare_parts.get')}}" @else data-toggle="modal" data-target="#login_alert" data="Spare_parts.get" @endif>
                         <img src="{{asset('/assets')}}/images/spare.png" class="services_img spare">
                          <p>{{__('front.home.spare')}}</p>
                             </a>
                         </li>
                     </ul>
        </div>
<!-- Modal -->
<div class="modal fade" id="login_alert" tabindex="-1" role="dialog" aria-labelledby="login_alert" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5 class="pop_login_signup_alert_text">قم بتسجيل الدخول اولا او  قم بانشاء حساب .</h5>
        <div class="col-12 mb-3 d-flex justify-content-center">
						<ul class="col-lg-12 nav nav-tabs login_alert_pop">
			        <li class="col-6 nav-item px-0">
			          <a href="{{route('login.show')}}" class="d-block text-center nav-link tab left-tab active">تسجيل دخول</a>
			        </li>
			        <li class="col-6 nav-item px-0">
			          <a href="{{route('signup.show')}}" class="d-block text-center nav-link tab right-tab">انشاء حساب</a>
			        </li>
						</ul>
					</div>
      </div>
    </div>
  </div>
</div>
        <script src="{{asset('/assets')}}/js/jquery.min.js"></script>
        <script src="{{asset('/assets')}}/js/bootstrap.min.js"></script> 
         <script>
        var language = '{!! app()->getLocale() !!}';
     $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                 $('.allservices_ul_home_page a').click(function (){
                     var route = $(this).attr('data');
         $.ajax({
        url: "{!! route('register.route.before.login') !!}",
        type: 'post',
        dataType: 'json',
        data: {route : route},
        success: function(data) {
         }
         });
    });
        </script>
         </body>
</html>














