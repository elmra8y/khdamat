@include('rental.header')
@include('rental.nav')
   
         
  <style type="text/css">
    .active .bs-stepper-circle{
      background-color: var(--main-color-light)
    }
  </style>
   
      <div class="container">
        <div class="order_details_box">
        


          <div class="row">
            <div class="bs-stepper linear w-100">
              <div class="bs-stepper-header" role="tablist">
                <!-- your steps here -->
                <div class="step" data-target="#init-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="init-part" id="init-part-trigger">
                    <span class="bs-stepper-circle">1</span>
                  </button>
                </div>
                
                <div class="line"></div>
                <div class="step" data-target="#review-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="secont-part" id="review-part-trigger">
                    <span class="bs-stepper-circle">2</span>
                  </button>
                </div>
                
                <div class="line"></div>
                <div class="step" data-target="#confirm-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="secont-part" id="confirm-part-trigger">
                    <span class="bs-stepper-circle">3</span>
                  </button>
                </div>
              </div>

              <div class="bs-stepper-content p-0">
                <!-- your steps content here -->

                  @if($garage_Request->status == 2)
            <form action="{{route('garage.request.confirmOrPay',$garage_Request->id)}}" method="post">
              @csrf()
          @endif
                <div id="init-part" class="content active dstepper-block" role="tabpanel" aria-labelledby="init-part-trigger">
                  @if($garage_Request->status == 0 || $garage_Request->status == 1 || $garage_Request->status == 2)
                    <div class="col-12 mb-4">
                    <h4 class="order_details_txt">تفاصيل الطلب</h4>
                <p class="service_type">{{__('front.garage.service')}} : <span>{{__('front.garage.serviceTitle')}}</span></p>
                <p class="service_type">{{__('front.garage.serviceType')}} : <span>{{($garage_Request->type == 1) ? $garage_Request->serviceObj->name : 'custom service'}}</span></p>
                <p class="service_type">{{__('front.carwash.mycars')}} : <span>{{$garage_Request->carObj->carObj->name}}</span></p>
             @if($garage_Request->status == 0)  <p class="service_type">{{__('front.garage.waitToResponse')}}</p>@endif
            @if($garage_Request->status == 2) <p class="service_type">{{__('front.garage.servicePrice')}} : <span>{{$garage_Request->amount}}</span></p> @endif
            @if($garage_Request->status == 5) <p class="service_type">{{__('front.garage.servicePricePaid')}} : <span>{{$garage_Request->amount}}</span></p> @endif
               @if($garage_Request->status == 2) <p class="service_type">{{__('front.garage.paymentWay')}} :</p>
            <div class="paymentChoise">
              <div class="form-check">
              <input class="form-check-input" type="radio" name="paymentType" id="keynet" value="keynet" checked>
              <label class="form-check-label" for="keynet">
                {{__('front.garage.kenet')}}
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="paymentType" id="cash" value="cash">
              <label class="form-check-label" for="cash">
                {{__('front.garage.cash')}}
              </label>
            </div>
            </div>
            @endif
                        @elseif($garage_Request->status  != 0)
                      <h4 class="order_details_txt">اكتمال الطلب</h4>
                     <div class="rental_conditions">تم اكتمال طلبك بنجاح .</div>
                  @endif
                  <!-- STATUS -->
                  <div class="col-12">
                    <div class="mb-2 d-flex align-items-center">
                      <span class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="var(--main-color-light)" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_material-done" data-name="Icon material-done" d="M10.827,19.241l-4.3-4.3L5.1,16.377,10.827,22.1,23.1,9.832,21.668,8.4Z" transform="translate(1465.9 314.6)" fill="var(--main-color-light)"/>
                          </g>
                        </svg>
                           
                      </span>
                      <span class="mx-2"></span>
                      <span class="h5">
                        {{__('front.rental.request_received')}}
                      </span>
                    </div>

                     <div class="mb-2 d-flex align-items-center">
                    <span class="icon">
                         @if($garage_Request->status  != 0)
                          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="var(--main-color-light)" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_material-done" data-name="Icon material-done" d="M10.827,19.241l-4.3-4.3L5.1,16.377,10.827,22.1,23.1,9.832,21.668,8.4Z" transform="translate(1465.9 314.6)" fill="var(--main-color-light)"/>
                          </g>
                        </svg>
                          @else
                           <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="#4a4d51" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_open-question-mark" data-name="Icon open-question-mark" d="M4.97,0A5.2,5.2,0,0,0,1.187,1.328,4.39,4.39,0,0,0,0,3.9l2.012.262a2.438,2.438,0,0,1,.624-1.388A2.988,2.988,0,0,1,4.97,2.012,3.55,3.55,0,0,1,7.425,2.7a1.682,1.682,0,0,1,.563,1.328c0,1.67-.684,2.133-1.69,3.018a5.735,5.735,0,0,0-2.334,4.527v.5H5.976v-.5c0-1.67.624-2.133,1.63-3.018A5.77,5.77,0,0,0,10,4.024,3.865,3.865,0,0,0,8.813,1.187,5.561,5.561,0,0,0,4.97,0ZM3.964,14.085V16.1H5.976V14.085Z" transform="translate(1475 322)" fill="#4a4d51"/>
                          </g>
                        </svg>
                          @endif
                      </span>
                      <span class="mx-2"></span>
                      <span class="h5">
                        {{__('front.rental.request_confirm_and_pay')}}
                      </span>
                    </div>
                       <div class="mb-2 d-flex align-items-center">
                      <span class="icon">
                           @if($garage_Request->status  != 0 && $garage_Request->status  != 1 && $garage_Request->status  != 2)
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="var(--main-color-light)" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_material-done" data-name="Icon material-done" d="M10.827,19.241l-4.3-4.3L5.1,16.377,10.827,22.1,23.1,9.832,21.668,8.4Z" transform="translate(1465.9 314.6)" fill="var(--main-color-light)"/>
                          </g>
                        </svg>
                         @else
                           <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="#4a4d51" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_open-question-mark" data-name="Icon open-question-mark" d="M4.97,0A5.2,5.2,0,0,0,1.187,1.328,4.39,4.39,0,0,0,0,3.9l2.012.262a2.438,2.438,0,0,1,.624-1.388A2.988,2.988,0,0,1,4.97,2.012,3.55,3.55,0,0,1,7.425,2.7a1.682,1.682,0,0,1,.563,1.328c0,1.67-.684,2.133-1.69,3.018a5.735,5.735,0,0,0-2.334,4.527v.5H5.976v-.5c0-1.67.624-2.133,1.63-3.018A5.77,5.77,0,0,0,10,4.024,3.865,3.865,0,0,0,8.813,1.187,5.561,5.561,0,0,0,4.97,0ZM3.964,14.085V16.1H5.976V14.085Z" transform="translate(1475 322)" fill="#4a4d51"/>
                          </g>
                        </svg>
                          @endif
                      </span>
                      <span class="mx-2"></span>
                      <span class="h5">
                        {{__('front.rental.request_complete')}}
                      </span>
                    </div>
                  </div>
                  <!-- END STATUS -->

                  <div class="col-12 d-flex justify-content-center">
                      @if($garage_Request->status  == 2)
                          <button type="submit" class="btn mt-5 px-5 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</button>
                      @endif
                  </div>
                </div>
                <!-- End Init Part -->



          @if($garage_Request->status == 1)
            </form>
          @endif
        </div>
      </div>
          </div>
            </div></div></div>
  @if($garage_Request->status != 0) 
<div class="container">
    <hr>
<div class="chat_container">
    @php
       $messages =  \App\Models\garage_request_messages::where(['request' => $garage_Request->id])->get();
      $lastmessage =  \App\Models\garage_request_messages::where(['request' => $garage_Request->id])->orderBy('id','desc')->first();
    if($lastmessage){
      $lastmessage = $lastmessage->id;
    }
    @endphp
    @foreach($messages as $message)
            
            @if($message->parent == auth()->guard('users')->user()->id  && $message->parentModel == 'user')
    <div class="single_message user">
                  <img src="{{asset('assets/users/'.$message->userAsParentObj->image)}}" class="user_image">
                  <div class="username_and_date">
                   <p>{{$message->userAsParentObj->name}}</p>
                   <p>{{\Carbon\Carbon::parse($message->created_at)->format('d / m / Y')}}</p>
                  </div>
                  <div class="clear"></div>
                  <div class="message">{{$message->message}}</div>
                  <div class="clear"></div>
                </div>
    @elseif($message->child == auth()->guard('users')->user()->id  && $message->parentModel == 'admin')
     <div class="single_message admin">
                  <img src="{{asset('assets/users/'.$message->adminAsParentObj->image)}}" class="user_image">
                  <div class="username_and_date">
                   <p>{{$message->adminAsParentObj->name}}</p>
                   <p>{{\Carbon\Carbon::parse($message->created_at)->format('d / m / Y')}}</p>
                  </div>
                  <div class="clear"></div>
                  <div class="message">{{$message->message}}</div>
                  <div class="clear"></div>
                </div>
    @endif
             
             @endforeach
                </div>
      <div class="message_write">
    	 <textarea class="form-control" id="message_content" rows="6" placeholder="{{__('front.garage.write_message')}}"></textarea>
		 <div class="clearfix"></div>
		 <div class="chat_bottom">
             <a href="#" class="pull-left upload_btn"><i class="fa fa-cloud-upload" aria-hidden="true"></i>
                    {{__('front.garage.Add_Files')}}
             </a>
 <button id="send_garage_message" class="pull-right btn btn-success">
    <span><i class='fa fa-spinner fa-spin '></i></span> {{__('front.garage.send_message')}}
             </button>
          @if($garage_Request->status == 2) </form> @endif
        </div>
		 </div>
</div> @endif


     @extends('rental.footer')
@section('script')
<script>
$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
</script>
@if($garage_Request->status == 1)
<script>
    var last = '{!! $lastmessage !!}';
   $('#send_garage_message').on('click',function (){
       if($('#message_content').val().length > 0){
           
       
         $.ajax({
        url: "{!! route('garage.request.folow-up.send.message') !!}",
        type: 'post',
        dataType: 'json',
        beforeSend: function(){
            $('#send_garage_message span').show();
            $('#message_content').val('');
            
        },
        data: {message : $('#message_content').val() , requestid : {!! $garage_Request->id!!}},
        success: function(data) {
            $('#send_garage_message span').hide();
            if(data.status == 'done'){
                $('.chat_container').append('<div class="single_message user"><img src="'+data.value.user_obj.image+'" class="user_image"><div class="username_and_date"><p>'+data.value.user_obj.name+'</p><p>'+data.value.date+'</p></div><div class="clear"></div><div class="message">'+data.value.message+'</div><div class="clear"></div></div>');
           last = data.value.id;
            }
            console.log(data);
         }
         });
       }
   });
    setInterval(function(){
         $.ajax({
        url: "{!! route('garage.request.folow-up.getMessages') !!}",
        type: 'post',
        dataType: 'json',
        data: {last : last , requestid : {!! $garage_Request->id!!}},
        success: function(data) {
           if(data.status == 'done'){
               if(data.last != null){
                   last = data.last;
               }
               
               data.messages.forEach(function(message){
                $('.chat_container').append('<div class="single_message admin"><img src="'+message.admin_as_parent_obj.image+'" class="user_image"><div class="username_and_date"><p>'+message.admin_as_parent_obj.name+'</p><p>'+message.date+'</p></div><div class="clear"></div><div class="message">'+message.message+'</div><div class="clear"></div></div>');
               });
              
           }
            console.log(data);
         }
         });
    },3000);
</script>
@endif
@if($garage_Request->status == 0)
<script>
  setInterval(function(){
         $.ajax({
        url: "{!! route('garage.request.folow-up.check') !!}",
        type: 'post',
        dataType: 'json',
        data: {requestid : {!! $garage_Request->id !!}},
        success: function(data) {
           
            if(data.status == 'done' && data.request_status == '1'){
                window.location.href = "{!! route('garage.request.follow',$garage_Request->id) !!}";
            }
         }
         });
    },3000);
</script>
@endif
@if($garage_Request->status == 1)
<script>
  setInterval(function(){
         $.ajax({
        url: "{!! route('garage.request.folow-up.check') !!}",
        type: 'post',
        dataType: 'json',
        data: {requestid : {!! $garage_Request->id !!}},
        success: function(data) {
           
            if(data.status == 'payment' && data.request_status == '2'){
                window.location.href = "{!! route('garage.request.follow',$garage_Request->id) !!}";
            }
         }
         });
    },3000);
</script>
@endif
@endsection
