            <!-- <div class="services_section" style="background-image:url('{{asset('/assets')}}/images/services.png')">
               <div class="container py-5">
                  <div class="row justify-content-center">
                    <div class="col-lg-8">
                      <h4 class="text-center text-white font-weight-bold mb-0">خدماتنا</h4>
                      <p class="text-center text-white py-3">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna</p>
                      <ul class="allservices_ul list-unstyled px-0 m-0 row justify-content-center">
                        <li class="px-2 text-center mb-3">
                          <div class="service_li p-3">
                            <img src="{{asset('/assets')}}/images/car.png" class="services_img mt-2">
                            <p class="my-2">قطع غيار</p>
                          </div>
                        </li>
                        <li class="px-2 text-center mb-3">
                          <div class="service_li p-3">
                            <img src="{{asset('/assets')}}/images/car.png" class="services_img mt-2">
                            <p class="my-2">قطع غيار</p>
                          </div>
                        </li>
                        <li class="px-2 text-center mb-3">
                          <div class="service_li p-3">
                            <img src="{{asset('/assets')}}/images/car.png" class="services_img mt-2">
                            <p class="my-2">قطع غيار</p>
                          </div>
                        </li>
                        <li class="px-2 text-center mb-3">
                          <div class="service_li p-3">
                            <img src="{{asset('/assets')}}/images/car.png" class="services_img mt-2">
                            <p class="my-2">قطع غيار</p>
                          </div>
                        </li>
                        <li class="px-2 text-center mb-3">
                          <div class="service_li p-3">
                            <img src="{{asset('/assets')}}/images/car.png" class="services_img mt-2">
                            <p class="my-2">قطع غيار</p>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
            </div> -->
       {{-- <div class="footer">
         <div class="container">
            <div class="row">
              <div class="col-lg-3">
                  <div class="footer_nav_container">
                  <ul class="footer_nav">
                       <h4 class="footer_nav_head">روابط</h4>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                  </ul>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="footer_nav_container">
                  <ul class="footer_nav">
                       <h4 class="footer_nav_head">روابط</h4>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                  </ul>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="footer_nav_container">
                  <ul class="footer_nav">
                       <h4 class="footer_nav_head">روابط</h4>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                  </ul>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="footer_nav_container">
                  <ul class="footer_nav">
                       <h4 class="footer_nav_head">روابط</h4>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                       <li>الصفحة الرئيسية</li>
                  </ul>
                  </div>
                </div>
             </div>
            </div>
        </div>  --}}
        <div class="container-fluid bg-dark text-light py-1 ">
          <div class="d-flex flex-md-row flex-column justify-content-md-between justify-content-center align-items-center">
            <div class="small my-1">
              <span>جميع الحقوق محفوظة © 2020</span>
            </div>
            <div class="small my-1">
              <span>شركة خدمات</span>
            </div>
          </div>
        </div>
        <script src="{{asset('/assets')}}/js/jquery.min.js"></script>
        <script src="{{asset('/assets')}}/js/popper.min.js"></script> 
        <script src="{{asset('/assets')}}/js/bootstrap.min.js"></script> 
        <script src="{{asset('/assets')}}/js/jquery.fancybox.min.js"></script> 
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bs-stepper/dist/js/bs-stepper.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function () {
            var stepper = new Stepper($('.bs-stepper')[0])
            $('.bs-stepper-next').click(function () {
              stepper.next()
              $('.step.active').prevAll().addClass('active')
            })
            $('.bs-stepper-prev').click(function () {
              $('.step.active').prevAll().removeClass('active')
              stepper.previous()
            })
          })
        </script>

           @yield('script')
 </body>
</html>