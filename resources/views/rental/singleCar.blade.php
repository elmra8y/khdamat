@include('rental.header')
  @include('rental.nav')
    <div class="main_section mt-5">
      <div class="container">
        <div class="row pt-5 mb-5">
          <div class="col-lg-8 mb-5 mb-lg-0">
            <div class="row mx-0">   
              <div class="col-lg-9 mb-3 mb-lg-0">
                <h1 class="car_single_name font-weight-bold h5">{{$car->carObj->name}}</h1>
                <h3 class="car_single_cat h6 mb-1"><span>{{$car->carObj->countryObj->name}}</span> - <span>{{$car->carObj->brandObj->name}}</span> - {{$car->carObj->categoryObj->name}} - {{$car->colorObj->name}} - {{$car->year}}</h3>
                <h3 class="car_single_avaiblity h6 mb-1">
                  <a href="/office/{{ $car->carObj->categoryObj->id }}">{{__('front.office')}} : <span class="text-primary">{{$car->officeObj->name}}</span></a>
                </h3>
                <h3 class="car_single_avaiblity h6 mb-1">{{__('front.rental.car_'.$car->status)}}</h3>
              </div>
              <div class="col-lg-3 px-0 pb-1 pb-lg-0 d-lg-block d-flex justify-content-between">
                <span class="car_single_price d-inline-block">
                  {{$car->price}} {{__('front.KWD')}} / {{__('front.day')}}
                </span>
              </div>
            </div>
            <hr>
            <div class="single_car_desc">
              {!! $car->description !!}
            </div>
          </div>
          <div class="col-lg-4">
            <a class="fancyImage d-block" href="{{asset('assets/carsImages/rental/' . $car->carObj->image)}}"> <img src="{{asset('assets/carsImages/rental/' . $car->carObj->image)}}" class="car_image_preview shadow"></a>
          </div>
        </div>
        <div class="row mb-5">

          <div class="col-lg-8">

            @if($errors->any())
            <div class="alert alert-danger danger-errors">
              @foreach ($errors->all() as $error)
              <p>{{ $error }}</p>
              @endforeach
            </div>
            @endif
            @if(session()->has('message'))
             <div class="alert alert-success danger-errors">
              <p>{!! session()->get('message') !!}</p>
            </div>
            @endif
            @if(session()->has('error'))
             <div class="alert alert-danger danger-errors">
              <p>{!! session()->get('error') !!}</p>
            </div>
            @endif
                @if(!auth()->guard('users')->check())
             <div class="alert alert-danger danger-errors">
              <a href="{{route('login.show')}}" style="    display: block;
    text-align: right;">{{__('front.please_login_to_can_book')}}</a>
            </div>
            @endif

            @if($car->status != 'notavailable' && auth()->guard('users')->check())
            <form class="row mx-0 rentalBookingForm" action="{{route('rental.book.car.firstStep',$car->id)}}" method="post">
              @csrf()
              <fieldset class="col-lg-12 row mb-3">
                <legend>تاريخ الحجز</legend>
                <div class="form-group col-6">
                  <label for="staticEmail" class="reserv_now_lable">من</label>
                  <input type="date" name="from" class="form-control form-control-sm">
                </div>
                <div class="form-group col-6">
                  <label for="staticEmail" class="">الي</label>
                  <input type="date" name="to" class="form-control form-control-sm">
                </div>
              </fieldset>

              <fieldset class="col-lg-12 row mb-3">
                <legend>وقت الحجز</legend>
                <div class="form-group col-6">
                  <label for="bookingTimeDelivery" class="reserv_now_lable">من</label>
                  <input type="time" class="form-control" id='bookingTimeDelivery' name="bookingTimeDelivery" />
                </div>
                <div class="form-group col-6">
                  <label for="bookingTimeReturn" class="">الي</label>
                  <input type="time" class="form-control" id="bookingTimeReturn" name="bookingTimeReturn">
                </div>
              </fieldset>

              <fieldset class="col-lg-12 row mb-3">
                <legend class="bg-light py-3 px-3 dropdown-toggle d-flex justify-content-between align-items-center" data-toggle="collapse" href="#place_1" role="button" aria-expanded="false" aria-controls="place_1">مكان التسليم</legend>
                <div class="row mx-0 collapse" id="place_1">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="Governorate1">{{__('front.rental.Governorate')}}</label>
                      <input type="text" class="form-control" id="Governorate1" name="Governorate">
                    </div>
                  </div>  
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="Region1">{{__('front.rental.Region')}}</label>
                      <input type="text" class="form-control" id="Region1" name="Region">
                    </div>
                  </div>
          
             
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="piece1">{{__('front.rental.piece')}}</label>
                      <input type="text" class="form-control" id="piece1" name="piece">
                    </div>
                  </div>
          
             
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="street1">{{__('front.rental.street')}}</label>
                      <input type="text" class="form-control" id="street1" name="street">
                    </div>
                  </div>
                 
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="Avenue1">{{__('front.rental.Avenue')}}</label>
                      <input type="text" class="form-control" id="Avenue1" name="Avenue">
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="Home1">{{__('front.rental.Home')}}</label>
                      <input type="text" class="form-control" id="Home1" name="Home">
                    </div>
                  </div>

                  <div class="col-lg-12">
                    <div class="form-group">
                      <label for="Notes2">{{__('front.rental.Notes')}}</label>
                      <textarea class="form-control" id="Notes2" name="Notes"></textarea>
                    </div>
                  </div>

                  <fieldset class="row mx-0 col-12">
                    <legend class="col-12 h6 mb-0 font-weight-bold">{{__('front.rental.or_share_location')}}</legend>
                    <div class="col-12 px-0 text-center">
                      <div class="form-group mt-2 jumbotron">
                        <button type="button" id="bookingDeliveryAddress" name="bookingDeliveryAddress" class="btn btn-sm alert-info">
                          {{__('front.rental.bookingLocation')}}
                          <span class="icon">
                            <svg width="14px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 512 512" xml:space="preserve">
                              <g>
                                <g>
                                  <path fill="#0c5460" d="M256,0C161.896,0,85.333,76.563,85.333,170.667c0,28.25,7.063,56.26,20.49,81.104L246.667,506.5
                                    c1.875,3.396,5.448,5.5,9.333,5.5s7.458-2.104,9.333-5.5l140.896-254.813c13.375-24.76,20.438-52.771,20.438-81.021
                                    C426.667,76.563,350.104,0,256,0z M256,256c-47.052,0-85.333-38.281-85.333-85.333c0-47.052,38.281-85.333,85.333-85.333
                                    s85.333,38.281,85.333,85.333C341.333,217.719,303.052,256,256,256z"/>
                                </g>
                              </g>
                            </svg>
                          </span>
                        </button>
                        <input name="lat1" hidden="hidden" id="lat1">
                        <input name="lng1" hidden="hidden" id="lng1">
                      </div>
                    </div>
                  </fieldset>
                </div>
              </fieldset>

              <fieldset class="col-lg-12 row mb-3">
                <legend class="bg-light py-3 px-3 dropdown-toggle d-flex justify-content-between align-items-center" data-toggle="collapse" href="#place_2" role="button" aria-expanded="false" aria-controls="place_2">مكان الاستلام</legend>
                <div class="row mx-0 collapse" id="place_2">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="bookingTimeReturn">{{__('front.rental.Governorate')}}</label>
                      <input type="text" class="form-control"  name="Governorate2">
                    </div>
                  </div>  
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="Region2">{{__('front.rental.Region')}}</label>
                      <input type="text" class="form-control" id="Region2" name="Region2">
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="piece2">{{__('front.rental.piece')}}</label>
                      <input type="text" class="form-control" id="piece2" name="piece2">
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="street2">{{__('front.rental.street')}}</label>
                      <input type="text" class="form-control" id="street2" name="street2">
                    </div>
                  </div>
                 
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="Avenue2">{{__('front.rental.Avenue')}}</label>
                      <input type="text" class="form-control" id="Avenue2" name="Avenue2">
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <label for="Home2">{{__('front.rental.Home')}}</label>
                      <input type="text" class="form-control" id="Home2" name="Home2">
                    </div>
                  </div>

                  <div class="col-lg-12">
                    <div class="form-group">
                      <label for="Notes2">{{__('front.rental.Notes')}}</label>
                      <textarea class="form-control" id="Notes2" name="Notes2"></textarea>
                    </div>
                  </div>

                  <fieldset class="row mx-0 col-12">
                    <legend class="col-12 h6 mb-0 font-weight-bold">{{__('front.rental.or_share_location')}}</legend>
                    <div class="col-12 px-0 text-center">
                      <div class="form-group mt-2 jumbotron">
                        <button type="button" id="bookingreturnCarAddress" name="bookingreturnCarAddress" class="btn btn-sm alert-info">
                          {{__('front.rental.bookingLocation')}}
                          <span class="icon">
                            <svg width="14px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 512 512" xml:space="preserve">
                              <g>
                                <g>
                                  <path fill="#0c5460" d="M256,0C161.896,0,85.333,76.563,85.333,170.667c0,28.25,7.063,56.26,20.49,81.104L246.667,506.5
                                    c1.875,3.396,5.448,5.5,9.333,5.5s7.458-2.104,9.333-5.5l140.896-254.813c13.375-24.76,20.438-52.771,20.438-81.021
                                    C426.667,76.563,350.104,0,256,0z M256,256c-47.052,0-85.333-38.281-85.333-85.333c0-47.052,38.281-85.333,85.333-85.333
                                    s85.333,38.281,85.333,85.333C341.333,217.719,303.052,256,256,256z"/>
                                </g>
                              </g>
                            </svg>
                          </span>
                        </button>
                        <input name="lat2" hidden="hidden" id="lat2">
                        <input name="lng2" hidden="hidden" id="lng2">
                      </div>
                    </div>
                  </fieldset>
                </div>
              </fieldset>

              <div class="col-lg-12 row mb-3">
                <div class="col-lg-12 d-flex justify-content-start">
                  <button type="submit" class="reserve_btn">حجز</button>
                </div>
              </div>

            </form>
            @endif
          </div>



          <!-- <div class="col-lg-8">
            
            @if($car->status != 'notavailable')
            <form class="rentalBookingForm" action="{{route('rental.book.car.firstStep',$car->id)}}" method="post">
                 @csrf()
           <div class="row">
             <div class="col-lg-6">
                <div class="form-group">
                  <label for="staticEmail" class="reserv_now_lable">الحجز بتاريخ</label>
                  <input type="date" name="from" class="form-control form-control-sm">
                </div>
              </div>
             <div class="col-lg-6">
                  <div class="form-group">
                <label for="staticEmail" class="">الي</label>
              <input type="date" name="to" class="form-control form-control-sm">
              </div>
                                   </div>



               <div class="col-lg-6">
                 <div class="form-group">
                <label for="bookingTimeDelivery">{{__('front.rental.bookingTimeDelivery')}}</label>
                     <input type="time" class="form-control" id='bookingTimeDelivery' name="bookingTimeDelivery" />
               
              </div>
               </div>
                <div class="col-lg-6">
                 <div class="form-group">
            <div class="form-group">
            <label for="bookingTimeReturn">{{__('front.rental.bookingTimeReturn')}}</label>
            <input type="time" class="form-control" id="bookingTimeReturn" name="bookingTimeReturn">
          </div>
                    </div>
               </div>



                <div class="col-lg-6">
                  <div class="form-group">
                    <div class="form-group">
                      <label for="bookingDeliveryAddress">{{__('front.rental.bookingDeliveryAddress')}}</label>
                      <input type="text" class="form-control" id="bookingDeliveryAddress" name="bookingDeliveryAddress" readonly>
                    </div>
                  </div>
               </div>

                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="bookingreturnCarAddress">{{__('front.rental.bookingreturnCarAddress')}}</label>
                    <input type="text" class="form-control" id="bookingreturnCarAddress" name="bookingreturnCarAddress" readonly>
                  </div>
                 <input name="lat1" hidden="hidden" id="lat1">
                 <input name="lat2" hidden="hidden" id="lat2">
                 <input name="lng1" hidden="hidden" id="lng1">
                 <input name="lng2" hidden="hidden" id="lng2">
                </div>


                <div class="col-lg-6">
                 <div class="form-group">
            <div class="form-group">
            <label for="bookingTimeReturn">{{__('front.rental.Governorate')}}</label>
            <input type="text" class="form-control"  name="Governorate">
          </div>
                    </div>
               </div>
                  <div class="col-lg-6">
                 <div class="form-group">
            <div class="form-group">
            <label for="bookingTimeReturn">{{__('front.rental.Region')}}</label>
            <input type="text" class="form-control"  name="Region">
          </div>
                    </div>
               </div>
                 <div class="col-lg-6">
                 <div class="form-group">
            <div class="form-group">
            <label for="bookingTimeReturn">{{__('front.rental.piece')}}</label>
            <input type="text" class="form-control"  name="piece">
          </div>
                    </div>
               </div>
                <div class="col-lg-6">
                 <div class="form-group">
            <div class="form-group">
            <label for="bookingTimeReturn">{{__('front.rental.street')}}</label>
            <input type="text" class="form-control"  name="street">
          </div>
                    </div>
               </div>
               <div class="col-lg-6">
                 <div class="form-group">
            <div class="form-group">
            <label for="bookingTimeReturn">{{__('front.rental.Avenue')}}</label>
            <input type="text" class="form-control"  name="Avenue">
          </div>
                    </div>
               </div>
               <div class="col-lg-6">
                 <div class="form-group">
            <div class="form-group">
            <label for="bookingTimeReturn">{{__('front.rental.Home')}}</label>
            <input type="text" class="form-control"  name="Home">
          </div>
                    </div>
               </div>
               
             <div class="col-lg-12"><button style="float: right;" type="submit" class="reserve_btn">حجز</button></div>
           </div>
             </form>
             @endif
           </div> -->


        </div>
      </div>



      <div class="container anoter_cars_container">
        <div class="row mb-5">
          <div class="col-lg-12">
            <h3 class="h5 font-weight-bold text-right mb-0">{{__('front.showanothercars')}}</h3>
          </div>
        </div>
        <div class="row eachCar mb-5">
          @foreach($deafultCars as $car)
          <div class="col-lg-4 mb-4 pb-2">
            <a class="carHrefPage d-block" href="{{route('rental.show.single.car',$car->id)}}">
              <div class="another_car_single shadow-dark">
                <div class="another_cars_image" style="background-image:url('{{asset('assets/carsImages/rental/' . $car->carObj->image)}}')"></div>
                <div class="another_car_single_texts col-12 py-3">
                  <h2 class="another_car_single_name small mb-0">{{$car->carObj->name}}</h2>
                  <h3 class="another_car_single_price h6 mt-3 mb-0 font-weight-bold">{{$car->price}} {{__('front.KWD')}} / {{__('front.day')}} </h3>
                  <div class="another_car_single_cat small my-1"><span>{{$car->carObj->sizeObj->name}}</span> - <span>{{$car->carObj->brandObj->name}}</span> - {{$car->carObj->categoryObj->name}} - {{$car->carObj->groupObj->name}} - {{$car->carObj->year}}</div>
                  <div class="d-flex justify-content-between align-items-center">
                      <span class="small">{{__('front.rental.car_'.$car->status)}}</span>
                      <div class="stars_container_another_car_single text-warning">
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star-o"></span>
                          <span class="fa fa-star-o"></span>
                      </div>
                    </div>
                </div>
              </div>
            </a>
          </div>
          @endforeach
        </div>

        <div class="readMoreContainer" @if(count($deafultCars) == 0 || $deafultCars->nextPageUrl() == null) style="display:none" @endif>
          <p class="readMore">{{__('front.readMore')}}</p>
          <img src="{{asset('/assets')}}/images/readmore.png" class="readMoreImg">
        </div>
      </div>
           
        </div>
 <!-- Modal -->
  <div class="modal fade" id="deliver_place" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{__('front.rental.bookingDeliveryAddress')}}</h4>
        </div>
        <div class="modal-body">
              <div id="pac-container">
        <input id="pac-input" type="text"
            placeholder="{{__('front.rental.Enteralocation')}}"><div id="location-error"></div>
      </div>
            <div id="map" class="map_deliver_location"></div>
             <button onclick="getLocation1()" class="btn btn-light" style="height: 42px;margin-top: 20px;" type="button">
        <img draggable="false" height="22" src="{{asset('assets/images/location.png')}}" width="22"> {{__('front.rental.DetectMyLocation')}}</button>
    </div>
   
        </div>
      </div>
    </div>
   <div class="modal fade" id="back_place" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{__('front.rental.bookingreturnCarAddress')}}</h4>
        </div>
        <div class="modal-body">
              <div id="pac-container2">
        <input id="pac-input2" type="text"
            placeholder="{{__('front.rental.Enteralocation')}}"><div id="location-error2"></div>
      </div>
            <div id="map2" class="map_back_location"></div>
             <button onclick="getLocation2()" class="btn btn-light" style="height: 42px;margin-top: 20px;" type="button">
        <img draggable="false" height="22" src="{{asset('assets/images/location.png')}}" width="22"> {{__('front.rental.DetectMyLocation')}}</button>
    </div>
   
        </div>
      </div>
    </div>
   <div class="modal fade" id="place_detected" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
              <h5 class="place_detected_h5">{{__('front.place_detected')}}</h5>
            <i class="fa fa-check-circle place_detected_icon"></i>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('front.close')}}</button>
      </div>
        </div>
      </div>
    </div>


     @extends('rental.footer')
@section('script')
<script>
    $('#bookingDeliveryAddress').focusin(function(){
        $('#deliver_place').modal('show');
    });
      $('#bookingreturnCarAddress').focusin(function(){
        $('#back_place').modal('show');
    });
    
$(".fancyImage").fancybox();
          $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
var nextUrl = '{!! $deafultCars->nextPageUrl() !!}';
      var language = '{!! app()->getLocale() !!}';
    $(".readMoreContainer").click(function (){
            $.ajax({
            url: nextUrl,
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                if(data.status == 'done'){
                   nextUrl = data.data.next_page_url;
                    if(nextUrl == null){
                        $(".readMoreContainer").hide();
                    }
                    data.data.data.forEach(function(car){
                        $('.eachCar').append(' <div class="col-lg-4"><a class="carHrefPage" href="/car/rental/'+car.car_obj.id+'"><div class="another_car_single"><div class="another_cars_image" style="background-image:url('+'{!! asset("/assets/carsimages/rental")!!}/'+car.car_obj.image+')"></div><div class="another_car_single_texts"><h6 class="another_car_single_price">'+"{!! __('front.day') !!}"+' / '+car.price+' {!! __("front.KWD") !!}'+'</h6><h6 class="another_car_single_name">'+car.car_obj["name_"+language]+'</h6><h6 class="another_car_single_cat"><span>'+car.size_obj["name_"+language]+'</span> - <span>'+car.brand_obj["name_"+language]+'</span> - '+car.category_obj["name_"+language]+' - '+car.group_obj["name_"+language]+' - '+car.year+'</h6><div class="stars_container_another_car_single"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></div><h6 class="another_car_single_availabilty">متاح الان</h6></div></div></a></div>');
                    });
                }
             }
         });
    });
    var marker;
    var marker2;
    var map;
    var map2;
    var bookingDeliveryAddress = document.getElementById('bookingDeliveryAddress');
    var bookingreturnCarAddress = document.getElementById('bookingreturnCarAddress');
    function getLocation1() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position){
        var lat = document.getElementById('lat1');
        var lng = document.getElementById('lng1');
        lat.value = position.coords.latitude;
        lng.value = position.coords.longitude;
        bookingDeliveryAddress.value = position.coords.latitude + ' , ' + position.coords.longitude;
        var centerCoordinates = {lat: position.coords.latitude , lng: position.coords.longitude};
        map = new google.maps.Map(document.getElementById('map'), {
        center: centerCoordinates,
        zoom: 12
        });
        marker = new google.maps.Marker({
            position : centerCoordinates,
          map: map , draggable: true
        });
         google.maps.event.addListener(marker, 'dragend', function (event) {
                            var lat = document.getElementById('lat1');
                            var lng = document.getElementById('lng1');
                            lat.value = this.getPosition().lat();
                            lng.value = this.getPosition().lng();
             bookingDeliveryAddress.value = this.getPosition().lat() + ' , ' + this.getPosition().lng();

});
        $('#deliver_place').modal('hide');
        $('#place_detected').modal('show');
    });
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}
      function getLocation2() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position){
        var lat = document.getElementById('lat2');
        var lng = document.getElementById('lng2');
        lat.value = position.coords.latitude;
        lng.value = position.coords.longitude;
        bookingreturnCarAddress.value = position.coords.latitude + ' , ' + position.coords.longitude;
        var centerCoordinates = {lat: position.coords.latitude , lng: position.coords.longitude};
        map2 = new google.maps.Map(document.getElementById('map2'), {
        center: centerCoordinates,
        zoom: 12
        });
        marker2 = new google.maps.Marker({
            position : centerCoordinates,
          map: map2 , draggable: true
        });
         google.maps.event.addListener(marker2, 'dragend', function (event) {
                            var lat = document.getElementById('lat2');
                            var lng = document.getElementById('lng2');
                            lat.value = this.getPosition().lat();
                            lng.value = this.getPosition().lng();
             bookingreturnCarAddress.value = this.getPosition().lat() + ' , ' + this.getPosition().lng();

});
        $('#back_place').modal('hide');
        $('#place_detected').modal('show');
    });
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}
    function initMap() {
    	var centerCoordinates = {lat: 29.3279856 , lng: 47.7808492};
        map = new google.maps.Map(document.getElementById('map'), {
        center: centerCoordinates,
        zoom: 12
        });
         map2 = new google.maps.Map(document.getElementById('map2'), {
        center: centerCoordinates,
        zoom: 12
        });
        var card = document.getElementById('pac-card');
        var card2 = document.getElementById('pac-card2');
        var input = document.getElementById('pac-input');
        var input2 = document.getElementById('pac-input2');
        
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
        map2.controls[google.maps.ControlPosition.TOP_RIGHT].push(card2);

        var autocomplete = new google.maps.places.Autocomplete(input);
        var autocomplete2 = new google.maps.places.Autocomplete(input2);
       
        
         marker = new google.maps.Marker({
            position : centerCoordinates,
          map: map , draggable: true
        });
          marker2 = new google.maps.Marker({
            position : centerCoordinates,
          map: map2 , draggable: true
        });

        autocomplete.addListener('place_changed', function() {
 	        document.getElementById("location-error").style.display = 'none';
            marker.setVisible(false);
        		var place = autocomplete.getPlace();
        		if (!place.geometry) {
        		  	document.getElementById("location-error").style.display = 'inline-block';
        		  	document.getElementById("location-error").innerHTML = "Cannot Locate '" + input.value + "' on map";
        		    return;
        		}
        		
        		map.fitBounds(place.geometry.viewport);
        		marker.setPosition(place.geometry.location);
        		marker.setVisible(true);
                var lat = document.getElementById('lat1');
                var lng = document.getElementById('lng1');
                lat.value = place.geometry.location.lat();
                lng.value = place.geometry.location.lng();
            bookingDeliveryAddress.value = place.geometry.location.lat() + ' , ' + place.geometry.location.lng();
        });
                google.maps.event.addListener(marker, 'dragend', function (event) {
                            var lat = document.getElementById('lat1');
                            var lng = document.getElementById('lng1');
                            lat.value = this.getPosition().lat();
                            lng.value = this.getPosition().lng();
                    bookingDeliveryAddress.value = this.getPosition().lat() + ' , ' + this.getPosition().lng();

});
          autocomplete2.addListener('place_changed', function() {
 	        document.getElementById("location-error2").style.display = 'none';
            marker2.setVisible(false);
        		var place = autocomplete2.getPlace();
        		if (!place.geometry) {
        		  	document.getElementById("location-error2").style.display = 'inline-block';
        		  	document.getElementById("location-error2").innerHTML = "Cannot Locate '" + input2.value + "' on map";
        		    return;
        		}
        		
        		map2.fitBounds(place.geometry.viewport);
        		marker2.setPosition(place.geometry.location);
        		marker2.setVisible(true);
                var lat = document.getElementById('lat2');
                var lng = document.getElementById('lng2');
                lat.value = place.geometry.location.lat();
                lng.value = place.geometry.location.lng();
              bookingreturnCarAddress.value = place.geometry.location.lat() + ' , ' + place.geometry.location.lng();
        });
                google.maps.event.addListener(marker2, 'dragend', function (event) {
                            var lat = document.getElementById('lat2');
                            var lng = document.getElementById('lng2');
                            lat.value = this.getPosition().lat();
                            lng.value = this.getPosition().lng();
                    bookingreturnCarAddress.value = this.getPosition().lat() + ' , ' + this.getPosition().lng();

});
    }

</script>
  <script src="https://maps.googleapis.com/maps/api/js?key={{env('MAP')}}&libraries=places&callback=initMap"
        async defer></script>
@endsection
