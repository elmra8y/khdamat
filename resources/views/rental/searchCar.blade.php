@include('rental.header')
    @include('rental.nav')
    

    <div class="home position-relative">
      <div class="overlay">
        <div class="overlay_content">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-6 col-md-10 animate__animated animate__fadeInRight">
                <div class="container_services py-4 px-4 my-5 my-lg-0">
                  <h4 class="mb-3 mt-3 h5 font-weight-bold">{{__('front.choose_car')}}</h4>
                  <div class="px-2">
                    <div class="px-3 mb-3 mt-4">
                      <form action="{{route('rental.show')}}" method="get">
                        <div class="row">
                            @php
                            $countries = \App\Models\cat_country::all();
                            $brands = \App\Models\cat_brand::all();
                            $categories = \App\Models\cat_category::all();
                            $carsColors = \App\Models\carsColors::all();
                            @endphp
                          <div class="col-lg-12">
                            <div class="form-group">
                              <select class="form-control" name="country">
                                <option value="">{{__('front.rental.country')}}</option>
                                  @foreach($countries as $country)
                                  <option value="{{$country->id}}"  @if(Request::get("country") == $country->id) selected @endif>
                                     {{$country->name}}
                                  </option>
                              @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <div class="form-group">
                              <select class="form-control" name="brand">
                                 <option value="">{{__('front.rental.brand')}}</option>
                                  @foreach($brands as $brand)
                                  <option value="{{$brand->id}}" @if(Request::get("brand") == $brand->id) selected @endif>
                                     {{$brand->name}}
                                  </option>
                              @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <div class="form-group">
                              <select class="form-control" name="category">
                                <option value="">{{__('front.rental.category')}}</option>
                                  @foreach($categories as $category)
                                      <option value="{{$category->id}}" @if(Request::get("category") == $category->id) selected @endif>
                                         {{$category->name}}
                                      </option>
                                  @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <div class="form-group">
                              <select class="form-control" name="year">
                                <option value="">{{__('front.rental.year')}}</option>
                                     @for($i = 2000 ; $i <= \Carbon\Carbon::now()->format('Y'); $i++)
                                      <option value="{{$i}}" @if(Request::get("year") == $i) selected @endif>
                                         {{$i}}
                                      </option>
                                      @endfor
                              </select>
                            </div>
                          </div>
                               <div class="col-lg-12">
                            <div class="form-group">
                              <select class="form-control" name="color">
                                <option value="">{{__('front.rental.color')}}</option>
                                      @foreach($carsColors as $color)
                                      <option value="{{$color->id}}" @if(Request::get("color") == $color->id) selected @endif>
                                         {{$color->name}}
                                      </option>
                                  @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <button type="submit" class="search_car_btn btn-block">{{__('front.search')}}</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer_home">
         <div class="container">
            <div class="row">
              <div class="col-lg-6">
                <ul class="foot_nav">
                  <li>عن الشركة</li>
                  <li>رؤيتنا</li>
                  <li>رسالتنا</li>
                  <li>الاسئلة المتكررة</li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="social_list">
                  <li>Facebook</li>
                  <li>Instgram</li>
                  <li>Twitter</li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>

    <div class="container anoter_cars_container">
      <div class="row mb-4 pb-2">
        <div class="col-lg-12">
          <h2 class="sort_by_text font-weight-bold mb-3">{{__('front.rental.sort by')}}</h2>
        </div>
        <div class="col-12">
          <form class="d-md-flex" action="{{route('rental.show')}}" method="get">
            <div class="">
              <div class="form-group">
                <select class="form-control bg-light border-0 shadow-sm" name="size">
                  <option value="">السعر لليوم الواحد</option>
                </select>
              </div>
            </div>
            <span class="mx-2 d-none d-lg-block"></span>
            <div class="">
              <div class="form-group">
                <select class="form-control bg-light border-0 shadow-sm" name="size">
                  <option value="">تقييم المستخدمين</option>
                </select>
              </div>
            </div>
            <span class="mx-2 d-none d-lg-block"></span>
            <div class="">
              <div class="form-group">
                <select class="form-control bg-light border-0 shadow-sm" name="size">
                  <option value="">التاريخ</option>
                </select>
              </div>
            </div>
            <span class="mx-2 d-none d-lg-block"></span>

            <div class="">
              <div class="form-group">
                <button type="submit" class="search_car_btn btn-block px-5 shadow-sm">{{__('front.search')}}</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <!-- Cars -->
      <div class="row eachCar mb-5">
          @foreach($deafultCars as $car)
          <div class="col-lg-4 mb-4 pb-2 hvr-grow">
            <a class="carHrefPage d-block" href="{{route('rental.show.single.car',$car->id)}}">
              <div class="another_car_single shadow-dark">
                <div class="another_cars_image" style="background-image:url('{{asset('assets/carsImages/rental/' . $car->carObj->image)}}')"></div>
                <div class="another_car_single_texts col-12 py-3">
                  <h2 class="another_car_single_name small mb-0">{{$car->carObj->name}}</h2>
                  <h3 class="another_car_single_price h6 mt-3 mb-0 font-weight-bold">{{$car->price}} {{__('front.KWD')}} / {{__('front.day')}} </h3>
                  <div class="another_car_single_cat small my-1"><span>{{$car->carObj->countryObj->name}}</span> - <span>{{$car->carObj->brandObj->name}}</span> - {{$car->carObj->categoryObj->name}}  - {{$car->carObj->year}} - {{$car->carObj->color}}</div>
                  <div class="d-flex justify-content-between align-items-center">
                      <span class="small">{{__('front.rental.car_'.$car->status)}}</span>
                    </div>
                </div>
              </div>
            </a>
          </div>
          @endforeach
      </div>

      <div class="readMoreContainer" @if(count($deafultCars) == 0 || $deafultCars->nextPageUrl() == null) style="display:none" @endif>  
        <p class="readMore">{{__('front.readMore')}}</p>
        <img src="{{asset('/assets')}}/images/readmore.png" class="readMoreImg">
      </div>

      <div class="row py-5 mb-5">
        <div class="col-12">
          <div class="alert-light text-center">
            <span>You reached the end</span>
          </div>
        </div>
      </div>

    </div>
    
    @extends('rental.footer')
    @section('script')
    <script>
          $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    var nextUrl = '{!! $deafultCars->nextPageUrl() !!}';
          var language = '{!! app()->getLocale() !!}';
        $(".readMoreContainer").click(function (){
                $.ajax({
                url: nextUrl,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    if(data.status == 'done'){
                       nextUrl = data.data.next_page_url;
                        if(nextUrl == null){
                            $(".readMoreContainer").hide();
                        }
                        data.data.data.forEach(function(car){
                            $('.eachCar').append(' <div class="col-lg-4"><a class="carHrefPage" href="/car/rental/'+car.car_obj.id+'"><div class="another_car_single"><div class="another_cars_image" style="background-image:url('+'{!! asset("/assets/carsimages/rental")!!}/'+car.car_obj.image+')"></div><div class="another_car_single_texts"><h6 class="another_car_single_price">'+"{!! __('front.day') !!}"+' / '+car.price+' {!! __("front.KWD") !!}'+'</h6><h6 class="another_car_single_name">'+car.car_obj["name_"+language]+'</h6><h6 class="another_car_single_cat"><span>'+car.size_obj["name_"+language]+'</span> - <span>'+car.brand_obj["name_"+language]+'</span> - '+car.category_obj["name_"+language]+' - '+car.group_obj["name_"+language]+' - '+car.year+'</h6><div class="stars_container_another_car_single"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></div><h6 class="another_car_single_availabilty">متاح الان</h6></div></div></a></div>');
                        });
                    }
                 }
             });
        });
    </script>
    @endsection
