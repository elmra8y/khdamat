<!--       <nav class="main_nav">
          <a href="{{url('/')}}"><div class="menu_clickable"><img src="{{asset('/assets')}}/images/menu.png"></div></a>
              <nav class="nav_container">
              <span class="navbar-brand services_drop" href="#">خدماتنا 
                  <div class="main_drop_container">
                   <ul>
                     <li><a href="{{route('rental.show')}}">{{__('front.rental part')}}</a></li>
                     <li><a href="{{route('road.help.get')}}">{{__('front.help')}}</a></li>
                     <li><a href="{{route('carwash.get')}}">{{__('front.car wash')}}</a></li>
                          <li><a href="{{route('garage.get')}}">{{__('front.garage_text')}}</a></li>
                       <li><a href="{{route('Spare_parts.get')}}">{{__('front.Spare_parts_text')}}</a></li>
                                          </ul>
                </div>
                  </span>
              <a class="navbar-brand" href="#">طلب سريع</a>
            <div class="profile_clickable">
            <img src="{{asset('/assets')}}/images/user.png">
                 <div class="profile_dropdown">
                   <ul>
                       @if(auth()->guard('users')->check())
                   <li><a href="{{route('myaccount')}}">{{__('profile.my_profile')}}</a></li>
                   <li><a href="{{route('logout')}}">{{__('profile.logout')}}</a></li>
                       @else
                        <li><a href="{{route('login.show')}}">{{__('profile.login')}}</a></li>
                        <li><a href="{{route('signup.show')}}">{{__('profile.signup')}}</a></li>
                       @endif
                   </ul>
                </div> 
            </div>
            <div class="clear"></div>
</nav>
        <div class="clear"></div>
        </nav> -->



<nav class="navbar navbar-expand-lg navbar-white bg-light navbar-light shadow-sm">
  <div class="container">
    <a class="navbar-brand py-2 px-3" href="/"><img src="{{asset('/assets/images/khadmaty.jpeg')}}" style="width: 90px;"></a>
    <a class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto flex-lg-row flex-column-reverse justify-content-center align-items-center">
        @if(auth()->guard('users')->check())
        <li class="nav-item nav-link"><a class="px-1" href="{{route('myaccount')}}">{{__('profile.my_profile')}}</a></li>
        <li class="nav-item nav-link"><a class="px-1" href="{{route('logout')}}">{{__('profile.logout')}}</a></li>
        @else
        <li class="nav-item nav-link"><a class="px-1" href="{{route('login.show')}}">{{__('profile.login')}}</a></li>
        <li class="nav-item nav-link"><a class="px-1" href="{{route('signup.show')}}">{{__('profile.signup')}}</a></li>
        @endif
        <li class="nav-item nav-link">
          <a class="px-1" href="#">{{__('front.contact_us')}} </a>
        </li>
        <li class="nav-item nav-link">
          <a class="px-1" href="#">{{__('front.posts')}} </a>
        </li>
        <li class="nav-item nav-link dropdown d-none d-lg-block">
          <a class="px-1 d-lg-flex justify-content-center align-items-center dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{__('front.services')}}
          </a>
          <div class="dropdown-menu animate slideIn" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{route('rental.show')}}">{{__('front.rental part')}}</a>
            <a class="dropdown-item" href="{{route('road.help.get')}}">{{__('front.help')}}</a>
            <a class="dropdown-item" href="{{route('carwash.get')}}">{{__('front.car wash')}}</a>
            <a class="dropdown-item" href="{{route('garage.get')}}">{{__('front.garage_text')}}</a>
            <a class="dropdown-item" href="{{route('Spare_parts.get')}}">{{__('front.Spare_parts_text')}}</a>
            <a class="dropdown-item" href="{{route('sell.get')}}">قسم بيع السيارات</a>
          </div>
        </li>

        <li class="nav-item nav-link w-100 d-lg-none">
          <a class="px-1 d-flex justify-content-center align-items-center dropdown-toggle" data-toggle="collapse" href="#mobileDropdown" role="button" aria-expanded="false" aria-controls="collapseExample">
            {{__('front.services')}}
          </a>
          <div class="collapse" id="mobileDropdown">
            <div class="bg-light border-0 mb-0">
              <a class="dropdown-item d-block text-center" href="{{route('rental.show')}}">{{__('front.rental part')}}</a>
              <a class="dropdown-item d-block text-center" href="{{route('road.help.get')}}">{{__('front.help')}}</a>
              <a class="dropdown-item d-block text-center" href="{{route('carwash.get')}}">{{__('front.car wash')}}</a>
              <a class="dropdown-item d-block text-center" href="{{route('garage.get')}}">{{__('front.garage_text')}}</a>
              <a class="dropdown-item d-block text-center" href="{{route('Spare_parts.get')}}">{{__('front.Spare_parts_text')}}</a>
            </div>
          </div>
        </li>

        <li class="nav-item active">
          <a class="px-1" href="/">{{__('front.home.main')}} <span class="sr-only">(current)</span></a>
        </li>
      </ul>
    </div>
  </div>
</nav>