@include('rental.header')
@include('rental.nav')

  <style type="text/css">
    .active .bs-stepper-circle{
      background-color: var(--main-color-light)
    }
  </style>
   
      <div class="container">
        <div class="order_details_box">
        


          <div class="row">
            <div class="bs-stepper linear w-100">
              <div class="bs-stepper-header" role="tablist">
                <!-- your steps here -->
                <div class="step" data-target="#init-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="init-part" id="init-part-trigger">
                    <span class="bs-stepper-circle">1</span>
                  </button>
                </div>
                
                <div class="line"></div>
                <div class="step" data-target="#review-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="secont-part" id="review-part-trigger">
                    <span class="bs-stepper-circle">2</span>
                  </button>
                </div>
                
                <div class="line"></div>
                <div class="step" data-target="#confirm-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="secont-part" id="confirm-part-trigger">
                    <span class="bs-stepper-circle">3</span>
                  </button>
                </div>

                <div class="line"></div>
                <div class="step" data-target="#final-part">
                  <button type="button" class="step-trigger" role="tab" aria-controls="secont-part" id="final-part-trigger">
                    <span class="bs-stepper-circle">4</span>
                  </button>
                </div>
              </div>

              <div class="bs-stepper-content p-0">
                <!-- your steps content here -->

                  @if($new_request->delivery_status == 'confirm')
            <form action="{{route('rental.book.confirmandPay',$new_request->id)}}" method="post">
              @csrf()
          @endif
                <div id="init-part" class="content active dstepper-block" role="tabpanel" aria-labelledby="init-part-trigger">
                  @if($new_request->delivery_status == 'init' || $new_request->delivery_status == 'confirm')
                    <div class="col-12 mb-4">
                    <h4 class="order_details_txt">تفاصيل الطلب</h4>
                    <p class="service_type">نوع الخدمة : <span>{{__('front.rental.booking.' . $new_request->request_type)}}</span></p>
                    <p class="car_info">اسم السيارة : <span>{{$new_request->rentalCarObj->carObj->name}}</span></p>
                    <p class="car_info">تفاصيل السيارة : <span><span>{{$new_request->rentalCarObj->carObj->countryObj->name}}</span> - <span>{{$new_request->rentalCarObj->carObj->brandObj->name}}</span> - {{$new_request->rentalCarObj->carObj->categoryObj->name}}  - {{$new_request->rentalCarObj->year}} - {{$new_request->rentalCarObj->colorObj->name}}</span></p>
                    <p class="car_info">عدد الايام : <span>{{$new_request->days}} / يوم</span></p>  
                    <p class="order_date">تاريخ الطلب : <span>{{\Carbon\Carbon::parse($new_request->created_at)->format('Y / m / d')}}</span></p>
                    <p class="order_start_date">تاريخ تنفيذ الخدمة : <span>{{\Carbon\Carbon::parse($new_request->request_datetime_start)->format('Y / m / d')}}</span></p>
                               @if($new_request->delivery_status == 'confirm') <p class="service_type">{{__('front.roadhelp.paymentWay')}} :</p>
            <div class="paymentChoise">
              <div class="form-check">
              <input class="form-check-input" type="radio" name="paymentType" id="keynet" value="keynet" checked>
              <label class="form-check-label" for="keynet">
                {{__('front.roadhelp.kenet')}}
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="paymentType" id="cash" value="cash">
              <label class="form-check-label" for="cash">
                {{__('front.carwash.cash')}}
              </label>
            </div>
            </div>
            @endif
                  </div>
                    @elseif($new_request->delivery_status == 'review')
                    <div class="col-12 mb-4">
                     <h4 class="order_details_txt">الشروط والاحكام</h4>
                     <div class="rental_conditions">{!! $new_request->officeObj->terms !!}</div>
                  </div>
                    @elseif($new_request->delivery_status == 'paid' || $new_request->delivery_status == 'cash')
                     <h4 class="order_details_txt">اكتمال الطلب</h4>
                     <div class="rental_conditions">تم اكتمال طلبك بنجاح .</div>
                  @endif
                  <!-- STATUS -->
                  <div class="col-12">
                    <div class="mb-2 d-flex align-items-center">
                      <span class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="var(--main-color-light)" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_material-done" data-name="Icon material-done" d="M10.827,19.241l-4.3-4.3L5.1,16.377,10.827,22.1,23.1,9.832,21.668,8.4Z" transform="translate(1465.9 314.6)" fill="var(--main-color-light)"/>
                          </g>
                        </svg>
                           
                      </span>
                      <span class="mx-2"></span>
                      <span class="h5">
                        {{__('front.rental.request_received')}}
                      </span>
                    </div>

                    <div class="mb-2 d-flex align-items-center">
                    <span class="icon">
                         @if($new_request->delivery_status  == 'review' || $new_request->delivery_status  == 'confirm' || $new_request->delivery_status == 'paid' || $new_request->delivery_status == 'cash')
                          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="var(--main-color-light)" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_material-done" data-name="Icon material-done" d="M10.827,19.241l-4.3-4.3L5.1,16.377,10.827,22.1,23.1,9.832,21.668,8.4Z" transform="translate(1465.9 314.6)" fill="var(--main-color-light)"/>
                          </g>
                        </svg>
                          @else
                           <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="#4a4d51" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_open-question-mark" data-name="Icon open-question-mark" d="M4.97,0A5.2,5.2,0,0,0,1.187,1.328,4.39,4.39,0,0,0,0,3.9l2.012.262a2.438,2.438,0,0,1,.624-1.388A2.988,2.988,0,0,1,4.97,2.012,3.55,3.55,0,0,1,7.425,2.7a1.682,1.682,0,0,1,.563,1.328c0,1.67-.684,2.133-1.69,3.018a5.735,5.735,0,0,0-2.334,4.527v.5H5.976v-.5c0-1.67.624-2.133,1.63-3.018A5.77,5.77,0,0,0,10,4.024,3.865,3.865,0,0,0,8.813,1.187,5.561,5.561,0,0,0,4.97,0ZM3.964,14.085V16.1H5.976V14.085Z" transform="translate(1475 322)" fill="#4a4d51"/>
                          </g>
                        </svg>
                          @endif
                      </span>
                      <span class="mx-2"></span>
                      <span class="h5">
                        {{__('front.rental.request_rental_policies')}}
                      </span>
                    </div>
                      
                    <div class="mb-2 d-flex align-items-center">
                      <span class="icon">
                        @if($new_request->delivery_status  == 'confirm' || $new_request->delivery_status == 'paid' || $new_request->delivery_status == 'cash')
                          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="var(--main-color-light)" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_material-done" data-name="Icon material-done" d="M10.827,19.241l-4.3-4.3L5.1,16.377,10.827,22.1,23.1,9.832,21.668,8.4Z" transform="translate(1465.9 314.6)" fill="var(--main-color-light)"/>
                          </g>
                        </svg>
                         @else
                           <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="#4a4d51" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_open-question-mark" data-name="Icon open-question-mark" d="M4.97,0A5.2,5.2,0,0,0,1.187,1.328,4.39,4.39,0,0,0,0,3.9l2.012.262a2.438,2.438,0,0,1,.624-1.388A2.988,2.988,0,0,1,4.97,2.012,3.55,3.55,0,0,1,7.425,2.7a1.682,1.682,0,0,1,.563,1.328c0,1.67-.684,2.133-1.69,3.018a5.735,5.735,0,0,0-2.334,4.527v.5H5.976v-.5c0-1.67.624-2.133,1.63-3.018A5.77,5.77,0,0,0,10,4.024,3.865,3.865,0,0,0,8.813,1.187,5.561,5.561,0,0,0,4.97,0ZM3.964,14.085V16.1H5.976V14.085Z" transform="translate(1475 322)" fill="#4a4d51"/>
                          </g>
                        </svg>
                          @endif
                      </span>
                      <span class="mx-2"></span>
                      <span class="h5">
                        {{__('front.rental.request_confirm_and_pay')}}
                      </span>
                    </div>

                    <div class="mb-2 d-flex align-items-center">
                      <span class="icon">
                           @if($new_request->delivery_status == 'paid' || $new_request->delivery_status == 'cash')
                            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="var(--main-color-light)" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_material-done" data-name="Icon material-done" d="M10.827,19.241l-4.3-4.3L5.1,16.377,10.827,22.1,23.1,9.832,21.668,8.4Z" transform="translate(1465.9 314.6)" fill="var(--main-color-light)"/>
                          </g>
                        </svg>
                         @else
                           <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30">
                          <g id="Group_64" data-name="Group 64" transform="translate(-1465 -315)">
                            <g id="Ellipse_9" data-name="Ellipse 9" transform="translate(1465 315)" fill="none" stroke="#4a4d51" stroke-width="2">
                              <circle cx="15" cy="15" r="15" stroke="none"/>
                              <circle cx="15" cy="15" r="14" fill="none"/>
                            </g>
                            <path id="Icon_open-question-mark" data-name="Icon open-question-mark" d="M4.97,0A5.2,5.2,0,0,0,1.187,1.328,4.39,4.39,0,0,0,0,3.9l2.012.262a2.438,2.438,0,0,1,.624-1.388A2.988,2.988,0,0,1,4.97,2.012,3.55,3.55,0,0,1,7.425,2.7a1.682,1.682,0,0,1,.563,1.328c0,1.67-.684,2.133-1.69,3.018a5.735,5.735,0,0,0-2.334,4.527v.5H5.976v-.5c0-1.67.624-2.133,1.63-3.018A5.77,5.77,0,0,0,10,4.024,3.865,3.865,0,0,0,8.813,1.187,5.561,5.561,0,0,0,4.97,0ZM3.964,14.085V16.1H5.976V14.085Z" transform="translate(1475 322)" fill="#4a4d51"/>
                          </g>
                        </svg>
                          @endif
                      </span>
                      <span class="mx-2"></span>
                      <span class="h5">
                        {{__('front.rental.request_complete')}}
                      </span>
                    </div>
                  </div>
                  <!-- END STATUS -->

                  <div class="col-12 d-flex justify-content-center">
                      @if($new_request->delivery_status  == 'confirm')
                          <button type="submit" class="btn mt-5 px-5 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</button>
                      @elseif($new_request->delivery_status == 'init')
                          <a href="{{route('rental.book.stepTwo',$new_request->id)}}" class="btn mt-5 px-5 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</a>
                       @elseif($new_request->delivery_status == 'review')
                          <a href="{{route('rental.book.stepThree',$new_request->id)}}" class="btn mt-5 px-5 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</a>
                      @endif
                  </div>
                </div>
                <!-- End Init Part -->



          @if($new_request->delivery_status == 'confirm')
            </form>
          @endif
        </div>
      </div>
          </div>
          </div></div>
     @include('rental.footer')
