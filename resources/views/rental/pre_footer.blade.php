<div class="services_section" style="background-image:url('{{asset('/assets')}}/images/services.png')">
 <div class="container py-5">
    <div class="row justify-content-center">
      <div class="col-lg-8">
        <h4 class="text-center text-white font-weight-bold mb-0">خدماتنا</h4>
        <p class="text-center text-white py-3">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna</p>
        <ul class="allservices_ul list-unstyled px-0 m-0 row justify-content-center">
          <li class="px-2 text-center mb-3">
            <div class="service_li p-3">
              <img src="{{asset('/assets')}}/images/car.png" class="services_img mt-2">
              <p class="my-2">قطع غيار</p>
            </div>
          </li>
          <li class="px-2 text-center mb-3">
            <div class="service_li p-3">
              <img src="{{asset('/assets')}}/images/car.png" class="services_img mt-2">
              <p class="my-2">قطع غيار</p>
            </div>
          </li>
          <li class="px-2 text-center mb-3">
            <div class="service_li p-3">
              <img src="{{asset('/assets')}}/images/car.png" class="services_img mt-2">
              <p class="my-2">قطع غيار</p>
            </div>
          </li>
          <li class="px-2 text-center mb-3">
            <div class="service_li p-3">
              <img src="{{asset('/assets')}}/images/car.png" class="services_img mt-2">
              <p class="my-2">قطع غيار</p>
            </div>
          </li>
          <li class="px-2 text-center mb-3">
            <div class="service_li p-3">
              <img src="{{asset('/assets')}}/images/car.png" class="services_img mt-2">
              <p class="my-2">قطع غيار</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>