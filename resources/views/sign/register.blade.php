@include('rental.header')
@include('rental.nav')
       
    <main class="signupForm">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{__('sign.signup')}}</div>
                        <div class="card-body">
                            <form name="my-form"  action="{{route('signup.post')}}" method="post">
                                                   {{csrf_field()}}
                                   @if($errors->any())
                        <div class="alert alert-danger danger-errors">
                          @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                                @endforeach
                        </div>
                        @endif
                        @if(session()->has('message'))
                         <div class="alert alert-success danger-errors">
                          <p>{!! session()->get('message') !!}</p>
                        </div>
                        @endif
                                <div class="form-group row">
                                    <label for="full_name" class="col-md-4 col-form-label text-md-right">{{__('sign.name')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" id="full_name" class="form-control" name="name">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email_address" class="col-md-4 col-form-label text-md-right">{{__('sign.email')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" id="email_address" class="form-control" name="email">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="user_name" class="col-md-4 col-form-label text-md-right">{{__('sign.phone')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" id="phone" class="form-control" name="phone">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{__('sign.password')}}</label>
                                    <div class="col-md-6">
                                        <input type="password" id="password" class="form-control" name="password">
                                    </div>
                                </div>

                          
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                        {{__('sign.Register')}}
                                        </button>
                                    </div>
                               
                            </form>
                        </div>
                    </div>
        </div>
        </div>
        </div>

</main>
     @include('rental.footer')
