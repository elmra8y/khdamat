@include('rental.header')
    @include('rental.nav')
    

    <div class="home position-relative">
      <div class="overlay">
        <div class="overlay_content">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-6 col-md-10 animate__animated animate__fadeInRight">
                <div class="container_services py-4 px-4 my-5 my-lg-0">
                  <h4 class="mb-3 mt-3 h5 font-weight-bold">{{__('front.choose_car')}}</h4>
                  <div class="px-2">
                    <div class="px-3 mb-3 mt-4">
                      <form action="" method="get">
                        <div class="row">
                            
                          <div class="col-lg-12">
                            <div class="form-group">
                              <select class="form-control" name="size">
                                <option value="">{{__('front.rental.size')}}</option>
                                  <option value=""  >
                                  </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <div class="form-group">
                              <select class="form-control" name="brand">
                                 <option value="">{{__('front.rental.brand')}}</option>
                                  <option value="" >
                                     
                                  </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <div class="form-group">
                              <select class="form-control" name="category">
                                <option value="">{{__('front.rental.category')}}</option>
                                      <option value="">
                                      </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <div class="form-group">
                              <select class="form-control" name="group">
                                <option value="">{{__('front.rental.group')}}</option>
                                      <option value="" >
                                      </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <div class="form-group">
                              <select class="form-control" name="year">
                                <option value="">{{__('front.rental.year')}}</option>
                                      <option value="" >
                                      </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <button type="submit" class="search_car_btn btn-block">{{__('front.search')}}</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer_home">
         <div class="container">
            <div class="row">
              <div class="col-lg-6">
                <ul class="foot_nav">
                  <li>عن الشركة</li>
                  <li>رؤيتنا</li>
                  <li>رسالتنا</li>
                  <li>الاسئلة المتكررة</li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="social_list">
                  <li>Facebook</li>
                  <li>Instgram</li>
                  <li>Twitter</li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>

    <div class="container anoter_cars_container">
      <div class="row mb-4 pb-2">
        <div class="col-lg-12">
          <h2 class="sort_by_text font-weight-bold mb-3">{{__('front.rental.sort by')}}</h2>
        </div>
        <div class="col-12">
          <form class="d-md-flex" action="" method="get">
            <div class="">
              <div class="form-group">
                <select class="form-control bg-light border-0 shadow-sm" name="size">
                  <option value="">السعر لليوم الواحد</option>
                </select>
              </div>
            </div>
            <span class="mx-2 d-none d-lg-block"></span>
            <div class="">
              <div class="form-group">
                <select class="form-control bg-light border-0 shadow-sm" name="size">
                  <option value="">تقييم المستخدمين</option>
                </select>
              </div>
            </div>
            <span class="mx-2 d-none d-lg-block"></span>
            <div class="">
              <div class="form-group">
                <select class="form-control bg-light border-0 shadow-sm" name="size">
                  <option value="">التاريخ</option>
                </select>
              </div>
            </div>
            <span class="mx-2 d-none d-lg-block"></span>

            <div class="">
              <div class="form-group">
                <button type="submit" class="search_car_btn btn-block px-5 shadow-sm">{{__('front.search')}}</button>
              </div>
            </div>
          </form>
        </div>
      </div>

      <!-- Cars -->
      <div class="row eachCar mb-5">
          <div class="col-lg-4 mb-4 pb-2 hvr-grow">
            <a class="carHrefPage d-block" href="{{route('sell.single')}}">
              <div class="another_car_single shadow-dark">
                <div class="another_cars_image" style="background-image:url('')"></div>
                <div class="another_car_single_texts col-12 py-3">
                  <h2 class="another_car_single_name small mb-0"></h2>
                  <h3 class="another_car_single_price h6 mt-3 mb-0 font-weight-bold"> </h3>
                  <div class="another_car_single_cat small my-1"><span></span> - <span></span> -  - </div>
                  <!-- <div class="d-flex justify-content-between align-items-center">
                      <span class="small"></span>
                      <div class="stars_container_another_car_single text-warning">
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star-o"></span>
                          <span class="fa fa-star-o"></span>
                      </div>
                    </div> -->
                </div>
              </div>
            </a>
          </div>
      </div>

      <div class="readMoreContainer">  
        <p class="readMore">{{__('front.readMore')}}</p>
        <img src="{{asset('/assets')}}/images/readmore.png" class="readMoreImg">
      </div>

      <div class="row py-5 mb-5">
        <div class="col-12">
          <div class="alert-light text-center">
            <span>You reached the end</span>
          </div>
        </div>
      </div>

    </div>
    
    @extends('rental.footer')
    @section('script')
    <script>

    </script>
    @endsection
