@include('rental.header')
  @include('rental.nav')
    <div class="main_section mt-5">
      <div class="container">
        <div class="row pt-5 mb-5">
          <div class="col-lg-8 mb-5 mb-lg-0">
            <div class="row mx-0">   
              <div class="col-lg-9 mb-3 mb-lg-0">
                <h1 class="car_single_name font-weight-bold h5">اسم السيارة</h1>
                <h3 class="car_single_cat h6 mb-1"><span>اسم السيارة</span> - <span>براند</span> - قسم - جروب - 2020</h3>
                <h3 class="car_single_avaiblity h6 mb-1">
                  <a href="/office/">{{__('front.office')}} : <span class="text-primary">مكتب</span></a>
                </h3>
                <h3 class="car_single_avaiblity h6 mb-1">{{__('front.rental.car_')}}</h3>
              </div>
              <div class="col-lg-3 px-0 pb-1 pb-lg-0 d-lg-block d-flex justify-content-between">
                <span class="car_single_price d-inline-block">
                  52 {{__('front.KWD')}} / {{__('front.day')}}
                </span>
                <div class="stars_container text-warning">
                  <span class="fa fa-star"></span>
                  <span class="fa fa-star"></span>
                  <span class="fa fa-star"></span>
                  <span class="fa fa-star-o"></span>
                  <span class="fa fa-star-o"></span>
                </div>
              </div>
            </div>
            <hr>
            <div class="single_car_desc">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </div>

            <div class="">
                <div class="user-control d-flex align-items-center justify-content-end my-3">
                  <div class="dropdown">
                    <a href="#" class="rounded-pill border py-2 px-3 dropdown-toggle" type="button" id="filter2-heading-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="    border-radius: 6px;">
                      <span class="mx-3 selected">تحكم في الإعلان</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="filter2-heading-1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 42px, 0px); top: 0px; left: 0px; will-change: transform;">

                    <div class="dropdown-item user-control-sec">
                      <a href="{{route('sellEdite')}}" class="icon d-flex">
                        <span>تعديل</span>
                      </a>
                    </div>

                    <div class="dropdown-item user-control-sec">
                      <a href="#" class="icon d-flex">
                        <span>ايقاف مؤقت</span>
                      </a>
                    </div>

                    <div class="dropdown-item user-control-sec">
                      <a href="#" class="icon d-flex" data-toggle="modal" data-target="#delete-single">
                        <span>حذف</span>
                      </a>

                      
                    </div>
                    </div>

                  <!-- Modal -->
                  <div class="modal fade" id="delete-single" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-body text-danger text-center py-5">
                          هل أنت متأكد من حذف الإعلان : اسم الإعلان
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                          <button type="button" class="btn btn-sm btn-danger">نعم</button>
                          <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">إلغاء</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>


            </div>
          </div>
          <div class="col-lg-4">
            <a class="fancyImage d-block" href="{{asset('assets/carsImages/rental/')}}"> <img src="{{asset('assets/carsImages/rental/')}}" class="car_image_preview shadow"></a>
          </div>
        </div>
        <div class="row mb-5">

          <div class="col-lg-8">
          </div>

        </div>
      </div>



      <div class="container anoter_cars_container">
        <div class="row mb-5">
          <div class="col-lg-12">
            <h3 class="h5 font-weight-bold text-right mb-0">{{__('front.showanothercars')}}</h3>
          </div>
        </div>
        <div class="row eachCar mb-5">
          <div class="col-lg-4 mb-4 pb-2">
            <a class="carHrefPage d-block" href="{{route('sellSingle')}}">
              <div class="another_car_single shadow-dark">
                <div class="another_cars_image" style="background-image:url('{{asset('assets/carsImages/rental/')}}')"></div>
                <div class="another_car_single_texts col-12 py-3">
                  <h2 class="another_car_single_name small mb-0">اسمالسيارة</h2>
                  <h3 class="another_car_single_price h6 mt-3 mb-0 font-weight-bold">52 {{__('front.KWD')}} / {{__('front.day')}} </h3>
                  <div class="another_car_single_cat small my-1"><span>اسم السيارة</span> - <span>براند</span> - قسم - جروب - 2020</div>
                  <div class="d-flex justify-content-between align-items-center">
                      <span class="small">{{__('front.rental.car_')}}</span>
                      <div class="stars_container_another_car_single text-warning">
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star-o"></span>
                          <span class="fa fa-star-o"></span>
                      </div>
                    </div>
                </div>
              </div>
            </a>
          </div>
        </div>

        <div class="readMoreContainer">
          <p class="readMore">{{__('front.readMore')}}</p>
          <img src="{{asset('/assets')}}/images/readmore.png" class="readMoreImg">
        </div>
      </div>
           
        </div>

     @extends('rental.footer')
@section('script')

  <script src="https://maps.googleapis.com/maps/api/js?key={{env('MAP')}}&libraries=places&callback=initMap"
        async defer></script>
@endsection
