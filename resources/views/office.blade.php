@include('rental.header')
@include('rental.nav')
   
      <div class="container my-5 text-right">
        <div class="row py-4 px-4 bg-white" style="border-radius: 1.5rem">
          <div class="col-12">
            <h2 class="h5 font-weight-bold border-bottom pb-2 d-inline-block">اسم المكتب</h2>
          </div>
          <div class="col-12">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
          <div class="col-12">
            <table class="table table-responsive table-borderless office-table">
              <tbody>
                <tr>
                  <td>العنوان</td>
                  <td>العنوان</td>
                </tr>
                <tr>
                  <td>هاتف</td>
                  <td dir="ltr">+25123456</td>
                </tr>
                <tr>
                  <td>العنوان</td>
                  <td>عنوان</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <!-- Cars -->

        <div class="row eachCar mb-5">
          <div class="col-lg-12 mb-5">
            <h3 class="h6 font-weight-bold text-right px-3 mb-0">{{__('front.show_office_cars')}}</h3>
          </div>
          @foreach($cars as $car)
          <div class="col-lg-4 mb-4 pb-2">
            <a class="carHrefPage d-block" href="{{route('rental.show.single.car',$car->id)}}">
              <div class="another_car_single shadow-dark">
                <div class="another_cars_image" style="background-image:url('{{asset('assets/carsImages/rental/' . $car->carObj->image)}}')"></div>
                <div class="another_car_single_texts col-12 py-3">
                  <h2 class="another_car_single_name small mb-0">{{$car->carObj->name}}</h2>
                  <h3 class="another_car_single_price h6 mt-3 mb-0 font-weight-bold">{{$car->price}} {{__('front.KWD')}} / {{__('front.day')}} </h3>
                  <div class="another_car_single_cat small my-1"><span>{{$car->carObj->sizeObj->name}}</span> - <span>{{$car->carObj->brandObj->name}}</span> - {{$car->carObj->categoryObj->name}} - {{$car->carObj->groupObj->name}} - {{$car->carObj->year}}</div>
                  <div class="d-flex justify-content-between align-items-center">
                      <span class="small">{{__('front.rental.car_'.$car->status)}}</span>
                      <div class="stars_container_another_car_single text-warning">
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star"></span>
                          <span class="fa fa-star-o"></span>
                          <span class="fa fa-star-o"></span>
                      </div>
                    </div>
                </div>
              </div>
            </a>
          </div>
          @endforeach
        </div>
      </div>
     @include('rental.footer')
