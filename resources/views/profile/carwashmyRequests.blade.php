@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>{{__('profile.carwash Request')}}</h4>
</div>
<div class="card__content">
<table class="table  table-hover custom_table_subscriptions">
        <thead>
        <tr>
        <th>{{__('profile.carwash.carName')}}</th>
        <th>{{__('profile.carwash.carBoardno')}}</th>
        <th>{{__('profile.carwash.created_at')}}</th>
        <th>{{__('profile.carwash.amount')}}</th>
        <th>{{__('profile.carwash.requestStatus')}}</th>
    
        </tr>
        </thead>
<tbody>
    @foreach(auth()->guard('users')->user()->carwashRequests as $carWashrequest)
      <tr>
  <td>{{$carWashrequest->carObj->carObj->name}}</td>
  <td>''</td>
  <td>{{$carWashrequest->created_at}}</td>
  <td>{{100}}</td>
  <td>{{__('profile.carwash.Requeststatus_'.$carWashrequest->status)}}</td>
  
 
          @endforeach
</tr>
</table>
    <a href="{{route('carwash.get')}}" class="btn btn-primary mainbtnstyle">{{__('profile.carwash.addRequest')}}</a>
    </div>
</div>

</div>
</div>

</div>
@endsection