@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>تعديل الإعلان</h4>
</div>
<div class="card__content">


<form class="rtl text-right needs-validation">
<div class="bs-stepper linear">
  <div class="bs-stepper-header" role="tablist">
    <!-- your steps here -->
    <div class="step" data-target="#first-part">
      <button type="button" class="step-trigger d-none" role="tab" aria-controls="first-part" id="first-part-trigger">
        <span class="bs-stepper-circle">1</span>
      </button>
    </div>
    <div class="line d-none"></div>
    <div class="step" data-target="#second-part">
      <button type="button" class="step-trigger d-none" role="tab" aria-controls="secont-part" id="secont-part-trigger">
        <span class="bs-stepper-circle">2</span>
      </button>
    </div>
    <div class="line d-none"></div>
    <div class="step" data-target="#third-part">
      <button type="button" class="step-trigger d-none" role="tab" aria-controls="third-part" id="third-part-trigger">
        <span class="bs-stepper-circle">3</span>
      </button>
    </div>
    <div class="line d-none"></div>
    <div class="step" data-target="#fourth-part">
      <button type="button" class="step-trigger d-none" role="tab" aria-controls="fourth-part" id="fourth-part-trigger">
        <span class="bs-stepper-circle">4</span>
      </button>
    </div>
    <div class="line d-none"></div>
    <div class="step" data-target="#fivth-part">
      <button type="button" class="step-trigger d-none" role="tab" aria-controls="fivth-part" id="fivth-part-trigger">
        <span class="bs-stepper-circle">5</span>
      </button>
    </div>
    <div class="line d-none"></div>
    <div class="step" data-target="#package-part">
      <button type="button" class="step-trigger d-none" role="tab" aria-controls="package-part" id="package-part-trigger">
        <span class="bs-stepper-circle">5</span>
      </button>
    </div>
    <div class="line d-none"></div>
    <div class="step" data-target="#sixth-part">
      <button type="button" class="step-trigger d-none" role="tab" aria-controls="sixth-part" id="sixth-part-trigger">
        <span class="bs-stepper-circle">6</span>
      </button>
    </div>

    <!-- <div class="line d-none"></div>
    <div class="step" data-target="#last-part">
      <button type="button" class="step-trigger d-none" role="tab" aria-controls="last-part" id="last-part-trigger">
        <span class="bs-stepper-circle">3</span>
      </button>
    </div> -->
  </div>

  <div class="bs-stepper-content p-0">
    <!-- your steps content here -->

    <!-- First -->
    <div id="first-part" class="content" role="tabpanel" aria-labelledby="first-part-trigger">
      <label class="mb-3">بلد المنشأ</label>
      <div class="form-group row mx-0">
        <div class="type_input_item col-lg-4 col-md-3 col-6">
          <input type="radio" name="type" value="1" id="type1" data-text="النوع الأول" class="type_input type">
          <label for="type1" class="px-0 py-0 my-0 badge-light col-12 place">
            <img src="/assets/images/places/usa.jpg" class="mw-100 rounded">
            <span class="place_label">بلد المنشأ</span>
          </label>
        </div>
        <div class="type_input_item col-lg-4 col-md-3 col-6">
          <input type="radio" name="type" value="2" id="type2" data-text="النوع الثاني" class="type_input type">
          <label for="type2" class="px-0 py-0 my-0 badge-light col-12 place">
            <img src="/assets/images/places/usa.jpg" class="mw-100 rounded">
            <span class="place_label">بلد المنشأ</span>
          </label>
        </div>
        <div class="type_input_item col-lg-4 col-md-3 col-6">
          <input type="radio" name="type" value="3" id="type3" data-text="النوع الثالث" class="type_input type">
          <label for="type3" class="px-0 py-0 my-0 badge-light col-12 place">
            <img src="/assets/images/places/usa.jpg" class="mw-100 rounded">
            <span class="place_label">بلد المنشأ</span>
          </label>
        </div>
      </div>

      <div class="form-group d-flex justify-content-end">
        <button type="button" class="btn btn-sm  px-3 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</button>
      </div>
      <div id="car_response" style="display:none"></div>
    </div>

    <!-- second -->
    <div id="second-part" class="content" role="tabpanel" aria-labelledby="second-part-trigger">
      <label class="mb-3">البراند</label>
      <div class="form-group row mx-0">
        <div class="type_input_item col-lg-4 col-md-3 col-6">
          <input type="radio" name="brand" value="1" id="brand1" data-text="كيا" class="type_input brand">
          <label for="brand1" class="px-0 py-0 my-0 badge-light col-12 brand">
            <img src="/assets/images/brands/kia.png" class="mw-100 rounded">
            <!-- <span class="brand_label">كيا</span> -->
          </label>
        </div>
        <div class="type_input_item col-lg-4 col-md-3 col-6">
          <input type="radio" name="brand" value="2" id="brand2" data-text="هايونداي" class="type_input brand">
          <label for="brand2" class="px-0 py-0 my-0 badge-light col-12 brand">
            <img src="/assets/images/brands/kia.png" class="mw-100 rounded">
            <!-- <span class="brand_label">هايونداي</span> -->
          </label>
        </div>
        <div class="type_input_item col-lg-4 col-md-3 col-6">
          <input type="radio" name="brand" value="3" id="brand3" data-text="مرسيدس" class="type_input brand">
          <label for="brand3" class="px-0 py-0 my-0 badge-light col-12 brand">
            <img src="/assets/images/brands/kia.png" class="mw-100 rounded">
            <!-- <span class="brand_label">مرسيدس</span> -->
          </label>
        </div>
      </div>

      <div class="form-group d-flex justify-content-between">
        <button type="button" class="btn btn-sm px-3 bs-stepper-prev rounded">{{__('front.prev')}}</button>
        <button type="button" class="btn btn-sm  px-3 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</button>
      </div>
      <div id="car_response" style="display:none"></div>
    </div>

    <!-- third -->
    <div id="third-part" class="content" role="tabpanel" aria-labelledby="third-part-trigger">
      <label>الموديل</label>
      <div class="form-group">
        <select name="model" class="form-control model">
          <option value="0" selected disabled>اختر الموديل</option>
          <option value="1">موديل  أول</option>
          <option value="2">موديل  ثاني</option>
        </select>
      </div>

      <label>سنة الصنع</label>
      <div class="form-group">
        <select name="model" class="form-control model">
          <option value="0" selected disabled>سنة الصنع</option>
          <option value="1">2020</option>
          <option value="2">2021</option>
        </select>
      </div>

      <div class="form-group d-flex justify-content-between">
        <button type="button" class="btn btn-sm px-3 bs-stepper-prev rounded">{{__('front.prev')}}</button>
        <button type="button" class="btn btn-sm  px-3 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</button>
      </div>
      <div id="car_response" style="display:none"></div>
    </div>

    <!-- fourth -->
    <div id="fourth-part" class="content" role="tabpanel" aria-labelledby="fourth-part-trigger">
      <label>قم برفع صورة</label>
      <div class="form-group">
        <input type="file" class="form-control-file" name="">
      </div>

      <div class="form-group d-flex justify-content-between">
        <button type="button" class="btn btn-sm px-3 bs-stepper-prev rounded">{{__('front.prev')}}</button>
        <button type="button" class="btn btn-sm  px-3 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</button>
      </div>
      <div id="car_response" style="display:none"></div>
    </div>

    <!-- fivth -->
    <div id="fivth-part" class="content" role="tabpanel" aria-labelledby="fivth-part-trigger">
      <label>تفاصيل السيارة</label>
      <div class="form-group">
        <label>الاسم</label>
        <input type="text" class="form-control py-2 px-3 title" name="" placeholder="اسم السيارة">
      </div>
      <div class="form-group">
        <label>تفاصيل</label>
        <textarea class="form-control desc" rows="5" placeholder="وصف قصير"></textarea>
      </div>

      <div class="form-group d-flex justify-content-between">
        <button type="button" class="btn btn-sm px-3 bs-stepper-prev rounded">{{__('front.prev')}}</button>
        <button type="button" class="btn btn-sm  px-3 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</button>
      </div>
      <div id="car_response" style="display:none"></div>
    </div>

    <!-- package -->
    <div id="package-part" class="content" role="tabpanel" aria-labelledby="package-part-trigger">
      <label>اختر الباقة</label>

      <div class="form-group">
        <!-- <label></label> -->
        <select class="form-control">
          <option value="" name="">الباقة الأولى</option>
          <option value="" name="">الباقة الثانية</option>
          <option value="" name="">الباقة الثالثة</option>
        </select>
      </div>

      <div class="form-group d-flex justify-content-between">
        <button type="button" class="btn btn-sm px-3 bs-stepper-prev rounded">{{__('front.prev')}}</button>
        <button type="button" class="btn btn-sm  px-3 search_car_btn bs-stepper-next rounded">{{__('front.next')}}</button>
      </div>
      <div id="car_response" style="display:none"></div>
    </div>

    <!-- sixth -->
    <div id="sixth-part" class="content" role="tabpanel" aria-labelledby="sixth-part-trigger">
      <label>مراجعة التفاصيل</label>
      <div class="mb-3">
        <label class="mb-0 font-weight-bold">نوع السيارة</label>
        <div class="mb-2" id="brief-type"> - </div>
      </div>

      <div class="mb-3">
        <label class="mb-0 font-weight-bold">البراند</label>
        <div class="mb-2" id="brief-brand"> - </div>
      </div>

      <div class="mb-3">
        <label class="mb-0 font-weight-bold">الموديل</label>
        <div class="mb-2" id="brief-model"> - </div>
      </div>
      <div class="mb-3">
        <label class="mb-0 font-weight-bold">تفاصيل السيارة</label>
        <div class="mb-2" id="brief-desc">
          <div>
            <span>الاسم : </span>
            <span id="title"></span>
          </div>
          <div>
            <span>سنة الصنع : </span>
            <span id="year"></span>
          </div>
          <div>
            <span>وصف قصير : </span><br>
            <span id="desc"></span>
          </div>
        </div>
      </div>

      <div class="form-group d-flex justify-content-between">
        <button type="button" class="btn btn-sm px-3 bs-stepper-prev rounded">{{__('front.prev')}}</button>
        <button type="submit" class="btn btn-sm search_car_btn rounded saveBtnRight">{{__('front.send')}}</button>
      </div>
      <div id="car_response" style="display:none"></div>
    </div>

    <!-- Send -->
    <!-- <div id="last-part" class="content" role="tabpanel" aria-labelledby="last-part-trigger">
      <div class="form-group d-flex justify-content-between">
        <button type="button" class="btn btn-sm px-3 bs-stepper-prev rounded">{{__('front.prev')}}</button>
        <button type="submit" class="btn btn-sm search_car_btn rounded saveBtnRight">{{__('front.send')}}</button>
      </div>
    </div> -->

  </div>
</div>
</form>




</div>
</div>

</div>
</div>

</div>
@endsection


@section('script')

<script type="text/javascript">
  $(function () {
    $('input.type').change(function () {
      $("#brief-type").text($(this).attr('data-text'))
    })
    $('input.brand').change(function () {
      $("#brief-brand").text($(this).attr('data-text'))
    })
    $('select.model').change(function () {
      $("#brief-model").text($(this).find( "option:selected" ).text())
    })



    $('.title').keyup(function () {
      $('#title').text($(this).val())
    })
    $('.year').keyup(function () {
      $('#year').text($(this).val())
    })
    $('.desc').keyup(function () {
      $('#desc').text($(this).val())
    })

  })
</script>

@endsection