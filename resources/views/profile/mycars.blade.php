@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>{{__('profile.mycars')}}</h4>
</div>
<div class="card__content">
<table class="table  table-hover custom_table_subscriptions">
        <thead>
        <tr>
        <th>{{__('profile.carName')}}</th>
        <th>{{__('profile.carSize')}}</th>
        <th>{{__('profile.carBrand')}}</th>
        <th>{{__('profile.carCat')}}</th>
        <th>{{__('profile.carGroup')}}</th>
        <th>{{__('profile.carYear')}}</th>
    
        </tr>
        </thead>
<tbody>
    @foreach(auth()->guard('users')->user()->usersCars as $car)
      <tr>
  <td>{{$car->carObj->name}}</td>
  <td>{{$car->carObj->sizeObj->name}}</td>
  <td>{{$car->carObj->brandObj->name}}</td>
  <td>{{$car->carObj->categoryObj->name}}</td>
  <td>{{$car->carObj->groupObj->name}}</td>
  <td>{{$car->carObj->year}}</td>
 
          @endforeach
</tr>
</table>
    <a href="{{route('profile.addCar')}}" class="btn btn-primary mainbtnstyle">{{__('profile.addCarbtn')}}</a>
    </div>
</div>

</div>
</div>

</div>
@endsection