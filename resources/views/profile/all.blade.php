@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>إعلاناتي</h4>
</div>
<div class="card__content rtl text-right">

  <div class="mb-5">
    <h5 class="h6">الإعلانات السابقة</h5>
    

    <div class="container anoter_cars_container">

      <!-- Cars -->
      <div class="row eachCar mb-5">
          <div class="col-lg-4 mb-4 pb-2 hvr-grow">
            <a class="carHrefPage d-block" href="{{route('sellSingle')}}">
              <div class="another_car_single shadow-dark">
                <div class="another_cars_image" style="background-image:url('{{asset('assets/carsImages/rental/')}}')"></div>
                <div class="another_car_single_texts col-12 py-3">
                  <h2 class="another_car_single_name small mb-0">اسم السيارة</h2>
                  <h3 class="another_car_single_price h6 mt-3 mb-0 font-weight-bold">50 {{__('front.KWD')}} / {{__('front.day')}} </h3>
                  <div class="another_car_single_cat small my-1"><span>اسم</span> - <span>تاني</span> - اسم السيارة  - 2020 - لون</div>
                  <div class="d-flex justify-content-between align-items-center">
                      <span class="small">{{__('front.rental.car_')}}</span>
                    </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 mb-4 pb-2 hvr-grow">
            <a class="carHrefPage d-block" href="{{route('sellSingle')}}">
              <div class="another_car_single shadow-dark">
                <div class="another_cars_image" style="background-image:url('{{asset('assets/carsImages/rental/')}}')"></div>
                <div class="another_car_single_texts col-12 py-3">
                  <h2 class="another_car_single_name small mb-0">اسم السيارة</h2>
                  <h3 class="another_car_single_price h6 mt-3 mb-0 font-weight-bold">50 {{__('front.KWD')}} / {{__('front.day')}} </h3>
                  <div class="another_car_single_cat small my-1"><span>اسم</span> - <span>تاني</span> - اسم السيارة  - 2020 - لون</div>
                  <div class="d-flex justify-content-between align-items-center">
                      <span class="small">{{__('front.rental.car_')}}</span>
                    </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 mb-4 pb-2 hvr-grow">
            <a class="carHrefPage d-block" href="{{route('sellSingle')}}">
              <div class="another_car_single shadow-dark">
                <div class="another_cars_image" style="background-image:url('{{asset('assets/carsImages/rental/')}}')"></div>
                <div class="another_car_single_texts col-12 py-3">
                  <h2 class="another_car_single_name small mb-0">اسم السيارة</h2>
                  <h3 class="another_car_single_price h6 mt-3 mb-0 font-weight-bold">50 {{__('front.KWD')}} / {{__('front.day')}} </h3>
                  <div class="another_car_single_cat small my-1"><span>اسم</span> - <span>تاني</span> - اسم السيارة  - 2020 - لون</div>
                  <div class="d-flex justify-content-between align-items-center">
                      <span class="small">{{__('front.rental.car_')}}</span>
                    </div>
                </div>
              </div>
            </a>
          </div>
      </div>
    </div>




  </div>


</div>
</div>

</div>
</div>

</div>
@endsection


@section('script')

<script type="text/javascript">
</script>

@endsection