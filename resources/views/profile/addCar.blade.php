@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>{{__('profile.addNewCar')}}</h4>
</div>
<div class="card__content">
<form method="POST" class="df-personal-info" id="profileAddCar">
                            <div class="form-group">
                                  <label for="size">{{__('profile.size')}}</label>
                                  <select class="form-control" id="size" name="size">
                                          <option>{{__('profile.chooseSize')}}</option>
                                      @foreach($sizes as $size)
                                          <option value="{{$size->sizeObj->id}}">{{$size->sizeObj->name}}</option>
                                      @endforeach
                                  </select>
                            </div>
                                <div class="form-group">
                                      <label for="brand">{{__('profile.brand')}}</label>
                                      <select class="form-control" id="brand" name="brand">
                                          <option>{{__('profile.chooseBrand')}}</option>
                                        @foreach($brands as $brand)
                                          <option value="{{$brand->brandObj->id}}">{{$brand->brandObj->name}}</option>
                                      @endforeach
                                      </select>
                            </div>
                            <div class="form-group">
                                      <label for="category">{{__('profile.category')}}</label>
                                      <select class="form-control" id="category" name="category">
                                        <option>{{__('profile.chooseCategory')}}</option>
                                      </select>
                        </div>
                    <div class="form-group">
                              <label for="group">{{__('profile.group')}}</label>
                              <select class="form-control" id="group" name="group">
                             <option>{{__('profile.chooseGroup')}}</option>
                              </select>
                            </div>
                            <div class="form-group">
                                      <label for="year">{{__('profile.year')}}</label>
                                      <select class="form-control" id="year" name="year">
                                       <option>{{__('profile.chooseYear')}}</option>
                                      </select>
                            </div>
                      <div class="form-group">
                          <label for="car">{{__('profile.car')}}</label>
                          <select class="form-control" id="car" name="car">
                           <option>{{__('profile.chooseCar')}}</option>
                            </select>
                </div>
		<div id="car_response" style="display:none"></div>

     <button type="submit" class="btn btn-primary saveBtnRight" style="margin-top: 25px;">{{__('sign.save')}}</button>
       <div class="clear"></div>
    </form>
    </div>
</div>

</div>
</div>

</div>
@endsection
@section('script')
<script>
      var language = '{!! app()->getLocale() !!}';
     $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
    $('#brand,#size').change(function (){
         $.ajax({
        url: "{!! route('profile.get.cats') !!}",
        type: 'post',
        dataType: 'json',
        data: {size : $('#size').val() , brand : $('#brand').val()},
        success: function(data) {
            if(data.status == 'done'){
                    var html = '<option>'+'{!! __("profile.chooseCategory") !!}'+'</option>';
                    data.categories.forEach(function(cat){
                        html += '<option value="'+cat.category_obj.id+'">'+cat.category_obj['name_' +language]+'</option>';
                    });
                    $('#category').html(html);
            }
          
        
         }
         });
    });
      $('#category').change(function (){
         $.ajax({
        url: "{!! route('profile.get.groups') !!}",
        type: 'post',
        dataType: 'json',
        data: {size : $('#size').val() , brand : $('#brand').val() , category : $('#category').val()},
        success: function(data) {
            if(data.status == 'done'){
            var html = '<option>'+'{!! __("profile.chooseGroup") !!}'+'</option>';
            data.groups.forEach(function(group){
                html += '<option value="'+group.group_obj.id+'">'+group.group_obj['name_' +language]+'</option>';
            });
            $('#group').html(html);
            console.log(html);
            }
         }
         });
    });
      $('#group').change(function (){
         $.ajax({
        url: "{!! route('profile.get.years') !!}",
        type: 'post',
        dataType: 'json',
        data: {size : $('#size').val() , brand : $('#brand').val() , category : $('#category').val() , group : $('#group').val()},
        success: function(data) {
            if(data.status == 'done'){
            var html = '<option>'+'{!! __("profile.chooseYear") !!}'+'</option>';
            data.years.forEach(function(year){
                html += '<option value="'+year.year+'">'+year.year+'</option>';
            });
            $('#year').html(html);
            console.log(html);
            }
         }
         });
    });
       $('#year').change(function (){
         $.ajax({
        url: "{!! route('profile.get.cars') !!}",
        type: 'post',
        dataType: 'json',
        data: {size : $('#size').val() , brand : $('#brand').val() , category : $('#category').val() , group : $('#group').val() , year : $('#year').val()},
        success: function(data) {
            if(data.status == 'done'){
            var html = '<option>'+'{!! __("profile.chooseCar") !!}'+'</option>';
            data.cars.forEach(function(car){
                html += '<option value="'+car.id+'">'+car['name_' +language]+'</option>';
            });
            $('#car').html(html);
            console.log(html);
            }
         }
         });
    });
    $('#profileAddCar').submit(function(e){
        e.preventDefault();
        $('#car_response').html('').hide();
          $.ajax({
        url: "{!! route('profile.insertcar') !!}",
        type: 'post',
        dataType: 'json',
        data: $('#profileAddCar').serialize(),
        success: function(data) {
            if(data.status == 'failed'){
                               var html= '';
                               data.errors.forEach(function(error){
                                   html += "<p>"+error+"</p>";
                               });
                               $('#car_response').html("<div class='alert alert-danger danger-errors'>"+html+"</div>").show();
                           }
                             if(data.status == 'done'){
                               $('#car_response').html("<div class='alert alert-success danger-errors'><p>"+data.message+"</p></div>").show();
                               $('#profileAddCar').trigger('reset');     
                           }
         }
         });
    });
     
</script>
@endsection