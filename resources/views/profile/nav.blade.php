      <nav class="main_nav">
        <a href="{{url('/')}}"><div class="menu_clickable"><img src="{{asset('/assets')}}/images/menu.png"></div></a>
              <nav class="nav_container">
              <span class="navbar-brand services_drop" href="#">خدماتنا 
                  <div class="main_drop_container">
                   <ul>
                     <li><a href="{{route('rental.show')}}">{{__('front.rental part')}}</a></li>
                     <li><a href="{{route('road.help.get')}}">{{__('front.help')}}</a></li>
                     <li><a href="{{route('carwash.get')}}">{{__('front.car wash')}}</a></li>
                     <li><a href="{{route('garage.get')}}">{{__('front.garage_text')}}</a></li>
                        <li><a href="{{route('Spare_parts.get')}}">{{__('front.Spare_parts_text')}}</a></li>
                                          </ul>
                </div>
                  </span>
              <a class="navbar-brand" href="#">طلب سريع</a>
            <div class="profile_clickable">
            <img src="{{asset('/assets')}}/images/user.png">
                 <div class="profile_dropdown">
                   <ul>
                       @if(auth()->guard('users')->check())
                   <li><a href="{{route('myaccount')}}">{{__('profile.my_profile')}}</a></li>
                   <li><a href="{{route('logout')}}">{{__('profile.logout')}}</a></li>
                       @else
                        <li><a href="{{route('login.show')}}">{{__('profile.login')}}</a></li>
                        <li><a href="{{route('signup.show')}}">{{__('profile.signup')}}</a></li>
                       @endif
                   </ul>
                </div> 
            </div>
            <div class="clear"></div>
</nav>
        <div class="clear"></div>
        </nav>