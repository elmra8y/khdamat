@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>{{__('profile.rentalBookedCars')}}</h4>
</div>
<div class="card__content">
<table class="table  table-hover custom_table_subscriptions">
        <thead>
        <tr>
        <th>{{__('profile.rental.booked_at')}}</th>
        <th>{{__('profile.rental.book_start_date')}}</th>
        <th>{{__('profile.rental.booked_end_date')}}</th>
        <th>{{__('profile.rental.booked_days')}}</th>
        <th>{{__('profile.rental.booked_status')}}</th>
        </tr>
        </thead>
<tbody>
    @foreach(auth()->guard('users')->user()->Bookings as $book)
      <tr>
  <td>{{$book->created_at}}</td>
  <td>{{$book->request_date_from}}</td>
  <td>{{$book->request_date_to}}</td>
  <td>{{$book->days}} / {{__('profile.rental.day')}}</td>
  <td>{{__('profile.rental.'.$book->delivery_status)}}</td>
 
          @endforeach
</tr>
</table>
    </div>
</div>

</div>
</div>

</div>
@endsection