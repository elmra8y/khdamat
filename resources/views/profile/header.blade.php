<!DOCTYPE html>
<html style="direction: rtl;">
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('/assets')}}/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{asset('/assets')}}/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{asset('/assets')}}/css/animate.min.css">
        <link rel="stylesheet" href="{{asset('/assets')}}/css/style.css">
        <meta name="csrf-token" content="{{csrf_token()}}">
    </head>
    <body class="{{app()->getLocale()}}">