@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>الباقات</h4>
</div>
<div class="card__content rtl text-right">

  <div class="mb-5">
    <h5 class="h6">باقات أنت مشترك بها</h5>
    <table class="table border">
      <thead>
        <tr>
          <td>اسم الباقة</td>
          <td>السعر</td>
          <td>المدة</td>
          <td>عدد الإعلانات</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>اسم </td>
          <td>50.00 KD</td>
          <td>3</td>
          <td>1</td>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="mb-5">
    <h5 class="h6">الباقات المتاحة</h5>
    <table class="table border">
      <thead>
        <tr>
          <td>اسم الباقة</td>
          <td>السعر</td>
          <td>المدة</td>
          <td>عدد الإعلانات</td>
          <td>شراء</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>اسم </td>
          <td>50.00 KD</td>
          <td>3</td>
          <td>1</td>
          <td>
            <a href="" class="btn btn-sm text-white" style="background-color: var(--main-color-light);">ادفع</a>
          </td>
        </tr>
      </tbody>
    </table>
  </div>


</div>
</div>

</div>
</div>

</div>
@endsection


@section('script')

<script type="text/javascript">
</script>

@endsection