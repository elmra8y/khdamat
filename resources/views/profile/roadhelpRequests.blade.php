@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>{{__('profile.roadhelp Request')}}</h4>
</div>
<div class="card__content">
<table class="table  table-hover custom_table_subscriptions">
        <thead>
        <tr>
        <th>{{__('profile.roadhelp.carName')}}</th>
        <th>{{__('profile.roadhelp.carBoardno')}}</th>
        <th>{{__('profile.roadhelp.created_at')}}</th>
        <th>{{__('profile.roadhelp.amount')}}</th>
        <th>{{__('profile.roadhelp.requestStatus')}}</th>
    
        </tr>
        </thead>
<tbody>
    @foreach(auth()->guard('users')->user()->roadHelpRequests as $Roadrequest)
      <tr>
  <td>{{$Roadrequest->carObj->carObj->name}}</td>
  <td>''</td>
  <td>{{$Roadrequest->created_at}}</td>
  <td>{{100}}</td>
  <td>{{__('profile.roadhelp.Requeststatus_'.$Roadrequest->status)}}</td>
  
 
          @endforeach
</tr>
</table>
    <a href="{{route('road.help.get')}}" class="btn btn-primary mainbtnstyle">{{__('profile.roadhelp.addRequest')}}</a>
    </div>
</div>

</div>
</div>

</div>
@endsection