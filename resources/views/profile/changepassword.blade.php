@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>{{__('profile.passwordCahnge')}}</h4>
</div>
<div class="card__content">
<form method="POST" class="df-personal-info" action="{{route('myaccountChangePassword.update')}}">
    @csrf()
           @if($errors->any())
                        <div class="alert alert-danger danger-errors">
                          @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                                @endforeach
                        </div>
                        @endif
                        @if(session()->has('message'))
                         <div class="alert alert-success danger-errors">
                          <p>{!! session()->get('message') !!}</p>
                        </div>
                        @endif
                                 @if(session()->has('error'))
                         <div class="alert alert-danger danger-errors">
                          <p>{!! session()->get('error') !!}</p>
                        </div>
                        @endif
                              <div class="form-group">
									<label>{{__('sign.currentPassword')}}</label>
									<input type="password" name="current" class="form-control" required="">
								</div>
								
                      	<div class="form-group">
									<label>{{__('sign.newPassword')}}</label>
									<input type="password" name="new" class="form-control" required="">
								</div>
     	<div class="form-group">
									<label>{{__('sign.repeatNewPassword')}}</label>
            
									<input type="password" name="confirm" class="form-control" required="">
								</div>

     <button type="submit" class="btn btn-primary-inverse" style="margin-top: 25px;">{{__('sign.save')}}</button>

    </form>
    </div>
</div>

</div>
</div>

</div>
@endsection