@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>{{__('profile.Spare_parts_myRequest')}}</h4>
</div>
<div class="card__content">
<table class="table  table-hover custom_table_subscriptions">
        <thead>
        <tr>
        <th>{{__('profile.Spare_parts.carName')}}</th>
        <th>{{__('profile.Spare_parts.carBoardno')}}</th>
        <th>{{__('profile.Spare_parts.partName')}}</th>
        <th>{{__('profile.Spare_parts.created_at')}}</th>
        <th>{{__('profile.Spare_parts.amount')}}</th>
        <th>{{__('profile.Spare_parts.requestStatus')}}</th>
    
        </tr>
        </thead>
<tbody>
    @foreach(auth()->guard('users')->user()->sparePartRequests as $sparePartRequest)
      <tr>
  <td>{{$sparePartRequest->carObj->carObj->name}}</td>
  <td>''</td>
  <td>{{$sparePartRequest->spartName}}</td>
  <td>{{$sparePartRequest->created_at}}</td>
  <td>{{$sparePartRequest->amount}} / {{__('front.KWD')}}</td>
  <td>{{__('profile.Spare_parts.Requeststatus_'.$sparePartRequest->status)}}</td>
  
 
          @endforeach
</tr>
</table>
    <a href="{{route('Spare_parts.get')}}" class="btn btn-primary mainbtnstyle">{{__('profile.Spare_parts.addRequest')}}</a>
    </div>
</div>

</div>
</div>

</div>
@endsection