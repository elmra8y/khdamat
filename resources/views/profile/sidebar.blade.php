<style type="text/css">
  .acc-navigation-collapse [data-toggle="collapse"]:after {
    display: inline-block;
    margin-left: .255em;
    vertical-align: 0;
    content: "";
    border-top: 0;
    width: 7px;
    height: 7px;
    border: none;
    border-right: 1px solid;
    border-bottom: 1px solid;
    transform: rotate(135deg);
    float: left;
    transition: .3s;
  }

    .acc-navigation-collapse [data-toggle="collapse"].collapsed:after {
      transform: rotate(45deg);
    }

  .another_cars_image{
    height: 225px
  }
</style>

<div class="df-account-navigation">
<ul>
    <li class="df-account-navigation__link @if(Request::route()->getName() == 'myaccount') active @endif">
           <a href="{{route('myaccount')}}">{{__('profile.personnalInfo')}}</a>
        </li>
    <li class="df-account-navigation__link @if(Request::route()->getName() == 'myaccountChangePassword') active @endif">
           <a href="{{route('myaccountChangePassword')}}">{{__('profile.passwordCahnge')}}</a>
        </li>
     <li class="df-account-navigation__link @if(Request::route()->getName() == 'bookedRentalCars') active @endif">
           <a href="{{route('bookedRentalCars')}}">{{__('profile.rentalBookedCars')}}</a>
        </li>
     <li class="df-account-navigation__link @if(Request::route()->getName() == 'mycars' || Request::route()->getName() == 'profile.addCar') active @endif">
           <a href="{{route('mycars')}}">{{__('profile.mycars')}}</a>
        </li>
     <li class="df-account-navigation__link @if(Request::route()->getName() == 'roadhelp.myRequests') active @endif">
           <a href="{{route('roadhelp.myRequests')}}">{{__('profile.roadhelp Request')}}</a>
        </li>
    <li class="df-account-navigation__link @if(Request::route()->getName() == 'carwash.myRequests') active @endif">
           <a href="{{route('carwash.myRequests')}}">{{__('profile.carwash Request')}}</a>
        </li>
    <li class="df-account-navigation__link @if(Request::route()->getName() == 'garage.myRequests') active @endif">
           <a href="{{route('garage.myRequests')}}">{{__('profile.garage_myRequest')}}</a>
        </li>
      <li class="df-account-navigation__link @if(Request::route()->getName() == 'Spare_parts.myRequests') active @endif">
           <a href="{{route('Spare_parts.myRequests')}}">{{__('profile.Spare_parts_myRequest')}}</a>
        </li>

    <li class="df-account-navigation__link acc-navigation-collapse @if(Request::route()->getName() == 'sellEdite') active @endif">
      <a data-toggle="collapse" href="#navigation-collapse" role="button" aria-expanded="false" aria-controls="navigation-collapse">
        بيع السيارات
      </a>

    </li>
    <ol class="collapse px-0 @if(Request::is('profile/sell/*')) show @endif" id="navigation-collapse">
      <li class="df-account-navigation__link @if(Request::route()->getName() == 'sellNew') active @endif">
        <a href="{{route('sellNew')}}">إضافة سيارة للبيع</a>
      </li>
      <li class="df-account-navigation__link @if(Request::route()->getName() == 'sellAll') active @endif">
        <a href="{{route('sellAll')}}">إعلاناتي</a>
      </li>
      <li class="df-account-navigation__link @if(Request::route()->getName() == 'sellPackages') active @endif">
         <a href="{{route('sellPackages')}}">الباقات</a>
      </li>
    </ol>
    
    
    
   
</ul>
    </div>