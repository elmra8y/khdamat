@extends('profile.index')
@section('content')
<div class="container rtlpage margin60">
<div class="row">
<div class="col-lg-3">

<div class="card">
<div class="card__header">
<h4>{{__('profile.personnalAccount')}}</h4>
</div>
<div class="card__content">
@include('profile.sidebar')
</div>
</div>

</div>
<div class="col-lg-9">

<div class="card card--lg">
<div class="card__header">
<h4>{{__('profile.garage_myRequest')}}</h4>
</div>
<div class="card__content">
<table class="table  table-hover custom_table_subscriptions">
        <thead>
        <tr>
        <th>{{__('profile.garage.carName')}}</th>
        <th>{{__('profile.garage.carBoardno')}}</th>
        <th>{{__('profile.garage.created_at')}}</th>
        <th>{{__('profile.garage.amount')}}</th>
        <th>{{__('profile.garage.requestStatus')}}</th>
    
        </tr>
        </thead>
<tbody>
    @foreach(auth()->guard('users')->user()->garageRequests as $garagerequest)
      <tr>
  <td>{{$garagerequest->carObj->carObj->name}}</td>
  <td>''</td>
  <td>{{$garagerequest->created_at}}</td>
  <td>{{100}}</td>
  <td>{{__('profile.garage.Requeststatus_'.$garagerequest->status)}}</td>
  
 
          @endforeach
</tr>
</table>
    <a href="{{route('garage.get')}}" class="btn btn-primary mainbtnstyle">{{__('profile.garage.addRequest')}}</a>
    </div>
</div>

</div>
</div>

</div>
@endsection