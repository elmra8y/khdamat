@extends('admin.Admin.index')
@section('content')
  <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('admin.rental.addnewoffice')}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile addproductPage">
							<div class="m-portlet__body">
								<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('offices.post')}}">
										<div class="m-portlet__body">
		                                   {{csrf_field()}}
                                   @if($errors->any())
                        <div class="alert alert-danger danger-errors">
                          @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                                @endforeach
                        </div>
                        @endif
                        @if(session()->has('message'))
                         <div class="alert alert-success danger-errors">
                          <p>{{session()->get('message')}}</p>
                        </div>
                        @endif
                                        
											<div class="row">
                                            <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.office.name_en')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{old('name_en')}}">
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.office.name_ar')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_ar" value="{{old('name_ar')}}">
											</div>
                                                   <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.office.email')}}
												</label>
												<input type="email" class="form-control m-input"  name="email" value="{{old('email')}}">
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.office.terms_en')}}
												</label>
                                                    <textarea class="form-control m-input"  name="terms_en" rows="5">{{old('terms_en')}}</textarea>
											</div>
                                                  <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.office.terms_ar')}}
												</label>
                                                    <textarea class="form-control m-input"  name="terms_ar" rows="5">{{old('terms_ar')}}</textarea>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.office.password')}}
												</label>
												<input type="password" class="form-control m-input"  name="password">
											</div>
                                                  <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.office.Confirmpassword')}}
												</label>
												<input type="password" class="form-control m-input"  name="Confirmpassword">
											</div>
                                            </div>
                                           
										</div>
										<div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions">
												<button type="submit" class="btn btn-primary">
													{{__('admin.save')}}
												</button>
												<button type="reset" class="btn btn-secondary">
													{{__('admin.cancel')}}
												</button>
											</div>
										</div>
									</form>
							</div>
						</div>
					</div>
@endsection
@section('footer')

@endsection