@extends('admin.Admin.index')
@section('content')
  <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('admin.rental.addnewcategory')}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile addproductPage">
							<div class="m-portlet__body">
								<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('categories.post')}}">
										<div class="m-portlet__body">
		                                   {{csrf_field()}}
                                   @if($errors->any())
                        <div class="alert alert-danger danger-errors">
                          @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                                @endforeach
                        </div>
                        @endif
                        @if(session()->has('message'))
                         <div class="alert alert-success danger-errors">
                          <p>{{session()->get('message')}}</p>
                        </div>
                        @endif
                                        
											<div class="row">
                                                    @php
                                                    $brands = \App\Models\cat_brand::all();
                                                    @endphp
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.category.brand')}}
												</label>
												<select class="form-control m-select2" id="brand_select" name="brand">
												   @foreach($brands as $brand)
                                                    <option value="{{$brand->id}}">
													   {{$brand->name}}
                                                    </option>
                                                    @endforeach
											</select>
											</div>
                                            <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.category.name_en')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{old('name_en')}}">
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.category.name_ar')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_ar" value="{{old('name_ar')}}">
											</div>
                                            </div>
                                           
										</div>
										<div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions">
												<button type="submit" class="btn btn-primary">
													{{__('admin.save')}}
												</button>
												<button type="reset" class="btn btn-secondary">
													{{__('admin.cancel')}}
												</button>
											</div>
										</div>
									</form>
							</div>
						</div>
					</div>
@endsection
@section('footer')
<script>
   $('#brand_select').select2();
</script>
@endsection