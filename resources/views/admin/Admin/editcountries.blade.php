@extends('admin.Admin.index')
@section('content')
  <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('admin.rental.editcountry')}} - {{$cat_country->name}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile addproductPage">
							<div class="m-portlet__body">
								<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('countries.update',$cat_country->id)}}">
										<div class="m-portlet__body">
		                                   {{csrf_field()}}
                                   @if($errors->any())
                        <div class="alert alert-danger danger-errors">
                          @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                                @endforeach
                        </div>
                        @endif
                        @if(session()->has('message'))
                         <div class="alert alert-success danger-errors">
                          <p>{{session()->get('message')}}</p>
                        </div>
                        @endif
                                        
											<div class="row">
                                            <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.country.name_en')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$cat_country->name_en}}">
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.country.name_ar')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_ar" value="{{$cat_country->name_ar}}">
											</div>
                                            </div>
                                           
										</div>
										<div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions">
												<button type="submit" class="btn btn-primary">
													{{__('admin.save')}}
												</button>
												<button type="reset" class="btn btn-secondary">
													{{__('admin.cancel')}}
												</button>
											</div>
										</div>
									</form>
							</div>
						</div>
					</div>
@endsection
@section('footer')

@endsection