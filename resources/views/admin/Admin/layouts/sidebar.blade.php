	<!-- BEGIN: Aside Menu -->
	<div 
		id="m_ver_menu" 
		class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
		data-menu-vertical="true"
		 data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
		>
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
								<a  href="" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												{{__('rental.dashboard')}}
											</span>
<!--
											<span class="m-menu__link-badge">
												<span class="m-badge m-badge--danger">
													2
												</span>
											</span>
-->
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									{{__('admin.Components')}}
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('admin.rental.rental part')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('countries.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.rental.countries')}}
												</span>
											</a>
										</li>
                                        <li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('brands.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.rental.brands')}}
												</span>
											</a>
										</li>
                                         <li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('categories.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.rental.categories')}}
												</span>
											</a>
										</li>
                                         <li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('cars.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.rental.cars')}}
												</span>
											</a>
										</li>
                                        <li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('offices.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.rental.offices')}}
												</span>
											</a>
										</li>
                                        
									</ul>
								</div>
							</li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('admin.roadhelp.roadhelp')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('admin.roadhelp.services')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.roadhelp.services')}}
												</span>
											</a>
										</li>
                                        		<li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('admin.roadhelp.requests.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.roadhelp.requests')}}
												</span>
											</a>
										</li>
                                 
                                        
									</ul>
								</div>
							</li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('admin.carwash.carwash')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('admin.carwash.services')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.carwash.services')}}
												</span>
											</a>
										</li>
                                        		<li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('admin.carwash.requests.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.carwash.requests')}}
												</span>
											</a>
										</li>
                                 
                                        
									</ul>
								</div>
							</li>
                                <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('admin.garage.garage')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('admin.garage.services')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.garage.services')}}
												</span>
											</a>
										</li>
                                        		<li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('admin.garage.requests.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.garage.requests')}}
												</span>
											</a>
										</li>
                                 
                                        
									</ul>
								</div>
							</li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('admin.Spare_parts.Spare_parts_txt')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
                                        		<li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('admin.Spare_parts.requests.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.Spare_parts.requests')}}
												</span>
											</a>
										</li>
                                 
                                        
									</ul>
								</div>
							</li>
                        
			</ul>
					</div>
					<!-- END: Aside Menu -->