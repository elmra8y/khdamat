@extends('admin.Admin.index')
@section('content')
  <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('admin.rental.addnewcar')}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile addproductPage">
							<div class="m-portlet__body">
								<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('cars.post')}}" id="addCarForm">
										<div class="m-portlet__body">
		                                   {{csrf_field()}}
                                   @if($errors->any())
                        <div class="alert alert-danger danger-errors">
                          @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                                @endforeach
                        </div>
                        @endif
                        @if(session()->has('message'))
                         <div class="alert alert-success danger-errors">
                          <p>{{session()->get('message')}}</p>
                        </div>
                        @endif
                                        
											<div class="row">
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.car.country')}}
												</label>
                                                @php
                                                    $countries = \App\Models\cat_country::all();
                                                    $selected_country = '';
                                                    $selected_brand = '';
                                                    @endphp
												<select class="form-control m-select2" id="country_select" name="country">
                                                    <option value="">{{__('admin.rental.car.choose_country')}}</option>
												   @foreach($countries as $country)
                                                        @php
                                                    if($car->country == $country->id){
                                                        $selected_country = $country;
                                                    }
                                                    
                                                    @endphp
                                                        <option value="{{$country->id}}" @if($car->country == $country->id) selected @endif>
                                                           {{$country->name}}
                                                        </option>
                                                    @endforeach
											</select>
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.car.brand')}}
												</label>
												<select class="form-control m-select2" id="brand_select" name="brand">
                                                    <option value="">{{__('admin.rental.car.choose_brand')}}</option>
                                                     @foreach($selected_country->brands as $brand)
                                                             @php
                                                        if($car->category == $brand->id){
                                                            $selected_brand = $brand;
                                                        }

                                                        @endphp
                                                        <option value="{{$brand->id}}" @if($car->brand == $brand->id) selected @endif>
                                                           {{$brand->name}}
                                                        </option>
                                                    @endforeach
											</select>
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.car.category')}}
												</label>
												<select class="form-control m-select2" id="category_select" name="category">
												  <option>{{__('admin.rental.car.choose_category')}}</option>
                                                         @foreach($selected_brand->categories as $category)
                                                        <option value="{{$category->id}}" @if($car->category == $category->id) selected @endif>
                                                           {{$category->name}}
                                                        </option>
                                                    @endforeach
											</select>
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.car.image')}}
												</label>
												<div class="col-lg-4 col-md-9 col-sm-12 image_uploader_option" style="margin: 10px 0;padding-right: 0;padding-left: 0px;">
                                                          <input hidden="hidden" name="image" class="uploaded_image" id="car_image" type="file">
											<div class="dropzone_gallery" style="background-image:url({{asset('assets/carsImages/rental/'.$car->image)}})">
												<div class="addimagebutton_on_gallery">
                                                    <i class="fa fa-plus"></i>
                                                    <p>{{__('admin.rental.editImage')}}</p>
                                                    </div>
											</div>
										</div>
											</div>
                                            <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.car.name_en')}}
												</label>
												<input type="text" class="form-control m-input" id="name_en"  name="name_en" value="{{$car->name_en}}">
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.car.name_ar')}}
												</label>
												<input type="text" class="form-control m-input" id="name_ar"  name="name_ar" value="{{$car->name_ar}}">
											</div>
                                               <div class="col-lg-12" id="car_response">
												
											</div>
                                            <div class="col-lg-12">
             <div class="progress addCarProgress">
					<div class="progress-bar progress-bar-striped progress-bar-animated  bg-success" id="uploadImageProgress" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
				</div>
                                                </div>
                                            </div>
                                           
										</div>
										<div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions">
												<button type="submit" class="btn btn-primary">
													{{__('admin.save')}}
												</button>
												<button type="reset" class="btn btn-secondary">
													{{__('admin.cancel')}}
												</button>
											</div>
										</div>
									</form>
							</div>
						</div>
					</div>
@endsection
@section('footer')
<script src="{{asset('assets')}}/js/jquery.ajax-progress.js"></script>
<script>
    var language = '{!! app()->getLocale() !!}';
   $('#country_select,#brand_select,#category_select,#group_select,#year_select').select2();
     $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
    $('#country_select').change(function (){
         $.ajax({
        url: "{!! route('admin.rental.getBrandsChildren') !!}",
        type: 'post',
        dataType: 'json',
        data: {country : $('#country_select').val()},
        success: function(data) {
            var html = '<option>'+'{!! __("admin.rental.car.choose_brand") !!}'+'</option>';
            data.forEach(function(brand){
                html += '<option value="'+brand.id+'">'+brand['name_' +language]+'</option>';
            });
            $('#brand_select').html(html);
            console.log(html);
        
         }
         });
    });
      $('#brand_select').change(function (){
         $.ajax({
        url: "{!! route('admin.rental.getCategoriesChildren') !!}",
        type: 'post',
        dataType: 'json',
        data: {brand : $('#brand_select').val()},
        success: function(data) {
            var html = '<option>'+'{!! __("admin.rental.car.choose_category") !!}'+'</option>';
            data.forEach(function(category){
                html += '<option value="'+category.id+'">'+category['name_' +language]+'</option>';
            });
            $('#category_select').html(html);
            console.log(html);
        
         }
         });
    });
     $('#addCarForm').submit(function (e){
         e.preventDefault();
         $('.addCarProgress').hide();
         var imageFile = document.getElementById('car_image');
         var fd=new FormData();
         if(imageFile.files[0]){
             fd.append("image",imageFile.files[0]);
         }
         fd.append("country",$('#country_select').val());
         fd.append("brand",$('#brand_select').val());
         fd.append("category",$('#category_select').val());
         fd.append("name_en",$('#name_en').val());
         fd.append("name_ar",$('#name_ar').val());
         $('#car_response').html('').hide();
         var thisme = $(this);
            $.ajax({
                    type: 'POST',
                    url: "{!! route('cars.update',['id' => $car->id]) !!}",
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data:fd,
                    success: function(data) {
                        if(data.status == 'failed'){
                               var html= '';
                               data.errors.forEach(function(error){
                                   html += "<p>"+error+"</p>";
                               });
                               $('#car_response').html("<div class='alert alert-danger danger-errors'>"+html+"</div>").show();
                           }
                             if(data.status == 'success'){
                               $('#car_response').html("<div class='alert alert-success danger-errors'><p>"+data.message+"</p></div>").show();
                               thisme.trigger('reset');     
                           }
                        $('#uploadImageProgress').width(0 + '%');
                        $('.addCarProgress').hide();
                        
                    },
                    error: function(e) {
                        console.log(e);
                    },
                    progress: function(e) {
                        if(e.lengthComputable) {
                            $('.addCarProgress').show();
                            var pct = (e.loaded / e.total) * 100;
                            $('#uploadImageProgress').width(pct + '%');
                            console.log(pct);
                           
                        } else {
                            console.warn('Content Length not reported!');
                        }
                    }
                });
    });
    $('.dropzone_gallery').click(function (){
        $('#car_image').click();
    });
    $('#car_image').change(function(){
        var imageFile = document.getElementById('car_image');
        var reader = new FileReader();
         reader.onload = function(e) {
        $('.dropzone_gallery').css('background-image','url('+e.target.result+')');
    }
         reader.readAsDataURL(imageFile.files[0]);
         
    });
  
    
        
    
   
</script>
@endsection