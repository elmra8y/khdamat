@extends('admin.Admin.index')
@section('content')
  <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('admin.garage.viewRequest')}} - {{$garage_Request->id}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile addproductPage">
							<div class="m-portlet__body">
					
										<div class="m-portlet__body">
		                
                                        
											<div class="row">
                                            <div class="col-lg-12">
												<label for="name">
													{{__('admin.garage.requestUserName')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$garage_Request->userObj->name}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.garage.requestUserPhone')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$garage_Request->userObj->phone}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.garage.requestUserEmail')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$garage_Request->userObj->email}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.garage.requestCarName')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$garage_Request->carObj->carObj->name}}" disabled>
											</div>
                                                    <div class="col-lg-12">
												<label for="name">
													{{__('admin.garage.requestServiceType')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$garage_Request->serviceObj->name}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.garage.requestDate')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$garage_Request->created_at}}" disabled>
											</div>
                                                       @if($garage_Request->lat && $garage_Request->long)
                                                        <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.map')}}
												</label>
                                                          
												<div id="map_rental"></div>
                                                            
                                                            <div class="form-group row">
                                                            <div class="col-lg-2">
                                                                <button style="margin-top: 20px;" class="btn btn-brand" data-clipboard="true" data-clipboard-target="#locationshare">
                                                                    <i class="la la-copy"></i>
                                                                    {{__('admin.rental.shareLocaltion')}}
                                                                </button>
                                                                </div>
                                                            <div class="col-lg-10">
                                                                 <input style="margin-top: 20px;"  type="text" class="form-control m-input" readonly value="https://maps.google.com/?q={{$garage_Request->lat}},{{$garage_Request->long}}" id="locationshare">
                                                                </div>
                                                            </div>
                                               
                         
											</div>
                                                @endif
                                                  <div class="col-lg-12">
												<label for="name">
													{{__('admin.garage.description')}}
												</label>
                                                    <textarea type="text" class="form-control m-input" disabled>{{$garage_Request->description}}</textarea>
											</div>
                                                   <div class="col-lg-12">
												<label for="name">
													{{__('admin.garage.requestStatus')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{__('profile.garage.Requeststatus_'.$garage_Request->status)}}" disabled>
											</div>
                                                @if($garage_Request->status != 4)
                                                     <div class="col-lg-12">
												<label for="name">
													{{__('admin.garage.options')}}
												</label>
                                                         <div class="clear"></div>
                                                         @if($garage_Request->status == 0 || $garage_Request->status == 3 || $garage_Request->status == 4)
												<button type="button" class="btn btn-primary" id="changeRequestStatus">
														@if($garage_Request->status == 0)
                                                     {{__('admin.garage.Follow_up_request')}}
                                                    @else
                                                    {{__('admin.garage.doneRequest')}}
                                                    @endif
														</button>
                                                        @endif
                                                         <button type="button"  @if($garage_Request->status == 1) style="display:block" @endif class="btn btn-primary" id="start_service_garage">
													           {{__('admin.garage.start_service')}}
														</button>
                                                         
                                                        
											</div>
                                              
                                              @endif
                                            </div>
                                           
                                            <div class="chat_admin"  @if($garage_Request->status == 0) style="display:none" @endif>
                                            <hr>
                                            <div class="chat_container">
              @php
        $messages =  \App\Models\garage_request_messages::where(['request' => $garage_Request->id])->get();
      $lastmessage =  \App\Models\garage_request_messages::where(['request' => $garage_Request->id])->orderBy('id','desc')->first();
    if($lastmessage){
      $lastmessage = $lastmessage->id;
    }
    @endphp
                                            
    @foreach($messages as $message)
            
            @if($message->parent == auth()->guard('admins')->user()->id  && $message->parentModel == 'user')
    <div class="single_message admin">
                  <img src="{{asset('assets/users/'.$message->userAsParentObj->image)}}" class="user_image">
                  <div class="username_and_date">
                   <p>{{$message->userAsParentObj->name}}</p>
                   <p>{{\Carbon\Carbon::parse($message->created_at)->format('d / m / Y')}}</p>
                  </div>
                  <div class="clear"></div>
                  <div class="message">{{$message->message}}</div>
                  <div class="clear"></div>
                </div>
    @elseif($message->child == auth()->guard('admins')->user()->id  && $message->parentModel == 'admin')
     <div class="single_message user">
                  <img src="{{asset('assets/users/'.$message->adminAsParentObj->image)}}" class="user_image">
                  <div class="username_and_date">
                   <p>{{$message->adminAsParentObj->name}}</p>
                   <p>{{\Carbon\Carbon::parse($message->created_at)->format('d / m / Y')}}</p>
                  </div>
                  <div class="clear"></div>
                  <div class="message">{{$message->message}}</div>
                  <div class="clear"></div>
                </div>
    @endif
             
             @endforeach
            </div>
                                                 <div class="message_write">
    	 <textarea class="form-control" id="message_content" rows="6" placeholder="{{__('front.garage.write_message')}}"></textarea>
		 <div class="clearfix"></div>
		 <div class="chat_bottom">
             <a href="#" class="pull-left upload_btn"><i class="fa fa-cloud-upload" aria-hidden="true"></i>
                    {{__('front.garage.Add_Files')}}
             </a>
 <button id="send_garage_message" class="pull-right btn btn-success">
    <span><i class='fa fa-spinner fa-spin '></i></span> {{__('front.garage.send_message')}}
             </button>
        
        </div>
		 </div>
                                                </div>
                                        
										</div>
										
									
                                
							</div>
						</div>
					</div>
<div class="modal" tabindex="-1" role="dialog" id="start_service_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{__('admin.garage.startServiceTitle')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">{{__('admin.garage.amount')}}</label>
                    <div class="col-sm-10">
                      <input type="number" step="1.0" class="form-control" id="amount" placeholder="{{__('admin.garage.amount')}}">
                    </div>
                  </div>
           <div class="form-group row" id="amountInsertedResponseContainer">
                    <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                      <div class="alert alert-success" id="amountInsertedResponse"></div>
                    </div>
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="confirm_amount" style="margin:0 5px;">{{__('admin.garage.confirm')}}</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('admin.garage.cancel')}}</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('footer')
<script>
          var ClipboardDemo = function () {
    
    //== Private functions
    var demos = function () {
        // basic example
        new Clipboard('[data-clipboard=true]').on('success', function(e) {
            e.clearSelection();
            alert('Copied!');
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

jQuery(document).ready(function() {    
    ClipboardDemo.init();
});
    
         function initMap() {
  // The location of Uluru
  var uluru = {lat: parseFloat({!! $garage_Request->lat !!}) , lng: parseFloat({!! $garage_Request->long !!})};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map_rental'), {zoom: 16, center: uluru});
              var marker = new google.maps.Marker({position: uluru, map: map , draggable: false});
}
       $('#changeRequestStatus').click(function (){
         $.ajax({
        url: "{!! route('admin.garage.requestChangeStatus') !!}",
        type: 'post',
        dataType: 'json',
        data: {change : '{!! $garage_Request->id !!}'},
        success: function(data) {
            if(data.status == 'done'){
                $('#changeRequestStatus').html(data.message);
            }
            if(data.code == 1){
                $('.chat_admin').show();
                $('#start_service_garage').show();
                $('#changeRequestStatus').hide();
            }
        
         }
         });
    });
    
     var last = '{!! $lastmessage !!}';
    $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
   $('#send_garage_message').on('click',function (){
       if($('#message_content').val().length > 0){
           
       
         $.ajax({
        url: "{!! route('admin.garage.request.folow-up.send.message') !!}",
        type: 'post',
        dataType: 'json',
        beforeSend: function(){
            $('#send_garage_message span').show();
            $('#message_content').val('');
            
        },
        data: {message : $('#message_content').val() , requestid : {!! $garage_Request->id!!}},
        success: function(data) {
            $('#send_garage_message span').hide();
            if(data.status == 'done'){
                $('.chat_container').append('<div class="single_message user"><img src="'+data.value.user_obj.image+'" class="user_image"><div class="username_and_date"><p>'+data.value.user_obj.name+'</p><p>'+data.value.date+'</p></div><div class="clear"></div><div class="message">'+data.value.message+'</div><div class="clear"></div></div>');
           last = data.value.id;
            }
            console.log(data);
         }
         });
       }
   });
    setInterval(function(){
         $.ajax({
        url: "{!! route('admin.garage.request.folow-up.getMessages') !!}",
        type: 'post',
        dataType: 'json',
        data: {last : last , requestid : {!! $garage_Request->id!!}},
        success: function(data) {
           if(data.status == 'done'){
               if(data.last != null){
                   last = data.last;
               }
               
               data.messages.forEach(function(message){
                $('.chat_container').append('<div class="single_message admin"><img src="'+message.user_as_parent_obj.image+'" class="user_image"><div class="username_and_date"><p>'+message.user_as_parent_obj.name+'</p><p>'+message.date+'</p></div><div class="clear"></div><div class="message">'+message.message+'</div><div class="clear"></div></div>');
               });
              
           }
            console.log(data);
         }
         });
    },3000);
    $('#start_service_garage').on('click',function (){
      $('#start_service_modal').modal('show');
   });
    $('#confirm_amount').on('click',function (){
      $.ajax({
        url: "{!! route('admin.garage.request.assignPrice') !!}",
        type: 'post',
        dataType: 'json',
        beforeSend: function(){
          
            
        },
        data: {amount : $('#amount').val() , requestid : {!! $garage_Request->id!!}},
        success: function(data) {
           if(data.status == 'done'){
               $("#amountInsertedResponseContainer").show();
               $("#amountInsertedResponse").html(data.message);
               setTimeout(function(){
                     $("#amountInsertedResponseContainer").hide();
                     $("#amountInsertedResponse").html('');
                     $('#start_service_modal').modal('hide');
               },3000);
           }
            
            console.log(data);
         }
         });
   });

</script>
<script defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('MAP')}}&callback=initMap">
    </script>
@endsection