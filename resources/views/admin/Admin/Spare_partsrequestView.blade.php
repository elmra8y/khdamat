@extends('admin.Admin.index')
@section('content')
  <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('admin.Spare_parts.viewRequest')}} - {{$sparePart_request->id}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile addproductPage">
							<div class="m-portlet__body">
					
										<div class="m-portlet__body">
		                
                                        
											<div class="row">
                                            <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.requestUserName')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$sparePart_request->userObj->name}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.requestUserPhone')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$sparePart_request->userObj->phone}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.requestUserEmail')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$sparePart_request->userObj->email}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.requestCarName')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$sparePart_request->carObj->carObj->name}}" disabled>
											</div>
                                                  <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.partName')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$sparePart_request->spartName}}" disabled>
											</div> 
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.spare_part_WordPad')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$sparePart_request->spartName}}" disabled>
											</div> 
                                                  <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.spare_part_WordPad')}}
												</label>
												<img class="spare_part_WordPad" src="{{asset('/assets/carsImages/spares/'.$sparePart_request->spare_part_WordPad)}}">
											</div> 
                                                @if($sparePart_request->spare_part_photo)
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.spare_part_photo')}}
												</label>
												<img class="spare_part_photo" src="{{asset('/assets/carsImages/spares/'.$sparePart_request->spare_part_photo)}}">
											</div>
                                                 @endif
                                                   <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.requestStatus')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{__('profile.Spare_parts.Requeststatus_'.$sparePart_request->status)}}" disabled>
											</div>
                                                @if($sparePart_request->status == 5 && $sparePart_request->cancellation()->where('request_type','spares')->latest()->first())
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.cancel_reason')}}
												</label>
                                                    <textarea type="text" class="form-control m-input" disabled>{{$sparePart_request->cancellation()->where('request_type','spares')->latest()->first()->reason}}</textarea>
											</div>
                                                @endif
                                                 @if($sparePart_request->status == 0)
                                         <div class="col-lg-12">
												<label for="name">
													{{__('admin.Spare_parts.options')}}
												</label>
                                          <div class="clear"></div>
                                                <button type="button" class="btn btn-primary" id="changeRequestStatus">{{__('admin.Spare_parts.approveBtn')}}</button>   
                                              <button type="button" class="btn btn-primary" id="cancelRequest">{{__('admin.Spare_parts.cancelBtn')}}</button>   
											</div>
                                                @endif
                                            </div>
               
                                        
										</div>
										
									
                                
							</div>
						</div>
					</div>
<div class="modal" tabindex="-1" role="dialog" id="changeRequestStatus_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{__('admin.Spare_parts.acceptRequest')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">{{__('admin.Spare_parts.amount')}}</label>
                    <div class="col-sm-10">
                      <input type="number" step="1.0" class="form-control" id="amount" placeholder="{{__('admin.Spare_parts.amount')}}">
                    </div>
                  </div>
           <div class="form-group row" id="amountInsertedResponseContainer">
                    <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                      <div class="alert alert-success" id="amountInsertedResponse"></div>
                    </div>
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="confirm_amount" style="margin:0 5px;">{{__('admin.Spare_parts.confirm')}}</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('admin.Spare_parts.cancel')}}</button>
      </div>
    </div>
  </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="cancel_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{__('admin.Spare_parts.cancelRequest')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-3 col-form-label">{{__('admin.Spare_parts.cancel_reason')}}</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" id="reason_calncel"></textarea>
                    </div>
                  </div>
           <div class="form-group row" id="CancellationResponseContainer">
                    <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                      <div class="alert alert-success" id="CancellationResponse"></div>
                    </div>
                  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="Cancellation_confirm" style="margin:0 5px;">{{__('admin.Spare_parts.confirm')}}</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('admin.Spare_parts.cancel')}}</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('footer')
<script>
     $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
    $('#changeRequestStatus').click(function(){
        $('#changeRequestStatus_modal').modal('show');
    });
    $('#cancelRequest').click(function(){
        $('#cancel_modal').modal('show');
    });
    
    $('#confirm_amount').on('click',function (){
      $.ajax({
        url: "{!! route('admin.Spare_parts.request.assignPrice') !!}",
        type: 'post',
        dataType: 'json',
        beforeSend: function(){
          
            
        },
        data: {amount : $('#amount').val() , requestid : {!! $sparePart_request->id!!}},
        success: function(data) {
           if(data.status == 'done'){
               $("#amountInsertedResponseContainer").show();
               $("#amountInsertedResponse").html(data.message);
               setTimeout(function(){
                     $("#amountInsertedResponseContainer").hide();
                     $("#amountInsertedResponse").html('');
                     $('#start_service_modal').modal('hide');
               },3000);
           }
            
            console.log(data);
         }
         });
   });
        $('#Cancellation_confirm').on('click',function (){
      $.ajax({
        url: "{!! route('admin.Spare_parts.request.cancel') !!}",
        type: 'post',
        dataType: 'json',
        beforeSend: function(){
          
            
        },
        data: {reason : $('#reason_calncel').val() , requestid : {!! $sparePart_request->id!!}},
        success: function(data) {
           if(data.status == 'done'){
               $("#CancellationResponseContainer").show();
               $("#CancellationResponse").html(data.message);
               setTimeout(function(){
                     $("#CancellationResponseContainer").hide();
                     $("#CancellationResponse").html('');
                     $('#cancel_modal').modal('hide');
               },3000);
           }
            
            console.log(data);
         }
         });
   });
    
</script>
@endsection