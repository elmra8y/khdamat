@extends('admin.Admin.index')
@section('content')
   <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('admin.roadhelp.roadhelp')}} - {{__('admin.roadhelp.requests')}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__body">
								<!--begin: Search Form -->
								<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
									<div class="row align-items-center">
										<div class="col-xl-8 order-2 order-xl-1">
											<div class="form-group m-form__group row align-items-center">
												
												<div class="col-md-4">
													<div class="m-input-icon m-input-icon--left">
														<input type="text" class="form-control m-input m-input--solid" placeholder="{{__('admin.searchTxt')}}" id="generalSearch">
														<span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
													</div>
												</div>
											</div>
										</div>
                                       
										
                                       
									</div>
								</div>
								<table class="m-datatable" id="html_table" width="100%">
									<thead>
										<tr>
											<th title="Field #1">
												#
											</th>
											<th title="Field #2">
												{{__('admin.roadhelp.carName')}}
											</th>
                                            <th title="Field #2">
												{{__('admin.roadhelp.BoardNo')}}
											</th>
											<th title="Field #3">
												{{__('admin.roadhelp.created_at')}}
											</th>
                                            <th title="Field #3">
												{{__('admin.roadhelp.price')}}
											</th>
                                            <th title="Field #6">
												{{__('admin.roadhelp.status')}}
											</th>
                                            <th title="Field #6">
												{{__('admin.options')}}
											</th>
                                       
										</tr>
									</thead>
									<tbody>
                                      @foreach($roadHelpRequests as $requestSingle)
                                       <tr>
                                        <td>{{$requestSingle->id}}</td>
                                        <td>{{$requestSingle->carObj->carObj->name}}</td>
                                        <td>''</td>
                                        <td>{{$requestSingle->created_at}}</td>
                                        <td>{{$requestSingle->serviceObj->price}}</td>
                                        <td>{{__('profile.roadhelp.Requeststatus_'.$requestSingle->status)}}</td>
                                           <td><a href="{{route('admin.roadhelp.view.request',$requestSingle->id)}}"><i class="la la-eye"></i></a></td>
                                        </tr>
                                        
                                        @endforeach
									</tbody>
								</table>
		     
							</div>
						</div>
					</div>
@endsection
@section('footer')
 <script>
   var datatable = $('.m-datatable').mDatatable({
      data: {
        saveState: {cookie: false},
      },
      search: {
        input: $('#generalSearch'),
      },
      columns: [
        {
          field: 'Deposit Paid',
          type: 'number',
        },
        {
          field: 'Order Date',
          type: 'date',
          format: 'YYYY-MM-DD',
        },
      ],
    });
</script>
@endsection