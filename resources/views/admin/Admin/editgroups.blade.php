@extends('admin.Admin.index')
@section('content')
  <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('admin.rental.editgroups')}} - {{$cat_group->name}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile addproductPage">
							<div class="m-portlet__body">
								<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('groups.update',$cat_group->id)}}">
										<div class="m-portlet__body">
		                                   {{csrf_field()}}
                                   @if($errors->any())
                        <div class="alert alert-danger danger-errors">
                          @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                                @endforeach
                        </div>
                        @endif
                        @if(session()->has('message'))
                         <div class="alert alert-success danger-errors">
                          <p>{{session()->get('message')}}</p>
                        </div>
                        @endif
                                        
											<div class="row">
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.group.size')}}
												</label>
                                                @php
                                                    $sizes = \App\Models\cat_size::all();
                                                    $brands = \App\Models\cat_brand::all();
                                                    $categories = \App\Models\cat_category::all();
                                                    @endphp
												<select class="form-control m-select2" id="size_select" name="size">
												   @foreach($sizes as $size)
                                                        <option value="{{$size->id}}" @if($cat_group->size == $size->id) selected @endif>
                                                           {{$size->name}}
                                                        </option>
                                                    @endforeach
											</select>
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.group.brand')}}
												</label>
												<select class="form-control m-select2" id="brand_select" name="brand" >
												   @foreach($brands as $brand)
                                                    <option value="{{$brand->id}}" @if($cat_group->brand == $brand->id) selected @endif>
													   {{$brand->name}}
                                                    </option>
                                                    @endforeach
											</select>
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.group.category')}}
												</label>
												<select class="form-control m-select2" id="category_select" name="category">
												   @foreach($categories as $category)
                                                    <option value="{{$category->id}}" @if($cat_group->category == $category->id) selected @endif>
													   {{$category->name}}
                                                    </option>
                                                    @endforeach
											</select>
											</div>
                                            <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.category.name_en')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$cat_group->name_en}}">
											</div>
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.category.name_ar')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_ar" value="{{$cat_group->name_ar}}">
											</div>
                                            </div>
                                           
										</div>
										<div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions">
												<button type="submit" class="btn btn-primary">
													{{__('admin.save')}}
												</button>
												<button type="reset" class="btn btn-secondary">
													{{__('admin.cancel')}}
												</button>
											</div>
										</div>
									</form>
							</div>
						</div>
					</div>
@endsection
@section('footer')
<script>
   $('#size_select,#brand_select,#category_select').select2();
</script>
@endsection