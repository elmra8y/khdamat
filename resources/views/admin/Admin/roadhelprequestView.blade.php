@extends('admin.Admin.index')
@section('content')
  <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('admin.roadhelp.viewRequest')}} - {{$roadHelpRequest->id}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile addproductPage">
							<div class="m-portlet__body">
					
										<div class="m-portlet__body">
		                
                                        
											<div class="row">
                                            <div class="col-lg-12">
												<label for="name">
													{{__('admin.roadhelp.requestUserName')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$roadHelpRequest->userObj->name}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.roadhelp.requestUserPhone')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$roadHelpRequest->userObj->phone}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.roadhelp.requestUserEmail')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$roadHelpRequest->userObj->email}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.roadhelp.requestCarName')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$roadHelpRequest->carObj->carObj->name}}" disabled>
											</div>
                                                    <div class="col-lg-12">
												<label for="name">
													{{__('admin.roadhelp.requestServiceType')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$roadHelpRequest->serviceObj->name}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.roadhelp.requestDate')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$roadHelpRequest->created_at}}" disabled>
											</div>
                                                   @if($roadHelpRequest->lat && $roadHelpRequest->long)
                                                        <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.map')}}
												</label>
                                                          
												<div id="map_rental"></div>
                                                            
                                                            <div class="form-group row">
                                                            <div class="col-lg-2">
                                                                <button style="margin-top: 20px;" class="btn btn-brand" data-clipboard="true" data-clipboard-target="#locationshare">
                                                                    <i class="la la-copy"></i>
                                                                    {{__('admin.rental.shareLocaltion')}}
                                                                </button>
                                                                </div>
                                                            <div class="col-lg-10">
                                                                 <input style="margin-top: 20px;"  type="text" class="form-control m-input" readonly value="https://maps.google.com/?q={{$roadHelpRequest->lat}},{{$roadHelpRequest->long}}" id="locationshare">
                                                                </div>
                                                            </div>
                                               
                         
											</div>
                                                @endif
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.roadhelp.requestServicePrice')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$roadHelpRequest->serviceObj->price}}" disabled>
											</div>
                                                   <div class="col-lg-12">
												<label for="name">
													{{__('admin.roadhelp.requestStatus')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{__('profile.roadhelp.Requeststatus_'.$roadHelpRequest->status)}}" disabled>
											</div>
                                                @if($roadHelpRequest->status != 4)
                                                     <div class="col-lg-12">
												<label for="name">
													{{__('admin.roadhelp.options')}}
												</label>
                                                         <div class="clear"></div>
												<button type="button" class="btn btn-primary" id="changeRequestStatus">
														@if($roadHelpRequest->status == 0)
                                                    {{__('admin.roadhelp.confirmRequest')}}
                                                    @elseif($roadHelpRequest->status == 1)
                                                     {{__('admin.roadhelp.waitUserVerify')}}
                                                    @else
                                                    {{__('admin.roadhelp.doneRequest')}}
                                                    @endif
														</button>
                                                        
											</div>
                                              
                                              @endif
                                            </div>
                                           
										</div>
										
									
                                
							</div>
						</div>
					</div>
@endsection
@section('footer')
<script>
    var ClipboardDemo = function () {
    
    //== Private functions
    var demos = function () {
        // basic example
        new Clipboard('[data-clipboard=true]').on('success', function(e) {
            e.clearSelection();
            alert('Copied!');
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

jQuery(document).ready(function() {    
    ClipboardDemo.init();
});
    
         function initMap() {
  // The location of Uluru
  var uluru = {lat: parseFloat({!! $roadHelpRequest->lat !!}) , lng: parseFloat({!! $roadHelpRequest->long !!})};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map_rental'), {zoom: 16, center: uluru});
              var marker = new google.maps.Marker({position: uluru, map: map , draggable: false});
}
       $('#changeRequestStatus').click(function (){
         $.ajax({
        url: "{!! route('admin.roadhelp.requestChangeStatus') !!}",
        type: 'post',
        dataType: 'json',
        data: {change : '{!! $roadHelpRequest->id !!}'},
        success: function(data) {
            if(data.status == 'done'){
                $('#changeRequestStatus').html(data.message);
            }
        
         }
         });
    });

</script>
 <script defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('MAP')}}&callback=initMap">
    </script>
@endsection