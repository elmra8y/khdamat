	<!-- BEGIN: Aside Menu -->
	<div 
		id="m_ver_menu" 
		class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
		data-menu-vertical="true"
		 data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
		>
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
								<a  href="{{route('main')}}" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												{{__('admin.dashboard')}}
											</span>
<!--
											<span class="m-menu__link-badge">
												<span class="m-badge m-badge--danger">
													2
												</span>
											</span>
-->
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									{{__('admin.Components')}}
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li class="m-menu__item  m-menu__item--submenu  @if(Request::is('admin/events') || Request::is('admin/events/*')) m-menu__item--submenu m-menu__item--open m-menu__item--expanded @endif" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('admin.events')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item @if(Request::is('admin/events')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('events')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.ShowEvents')}}
												</span>
											</a>
										</li>
                                        <li class="m-menu__item @if(Request::is('admin/events/add')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('event.add')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.addEvent')}}
												</span>
											</a>
										</li>
								
									</ul>
								</div>
							</li>
                        	<li class="m-menu__item  m-menu__item--submenu  @if(Request::is('admin/products') || Request::is('admin/products/*') || Request::is('admin/orders') || Request::is('admin/orders/*')) m-menu__item--submenu m-menu__item--open m-menu__item--expanded @endif" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('admin.products')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item @if(Request::is('admin/products')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('products')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.showProducts')}}
												</span>
											</a>
										</li>
                                        <li class="m-menu__item @if(Request::is('admin/products/add')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('product.add')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.addProduct')}}
												</span>
											</a>
										</li>
                                          <li class="m-menu__item @if(Request::is('admin/orders')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('orders')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.orders')}}
												</span>
											</a>
										</li>
								
									</ul>
								</div>
							</li>
                            	<li class="m-menu__item  m-menu__item--submenu  @if(Request::is('admin/staff') || Request::is('admin/services') || Request::is('admin/services/*') || Request::is('admin/appointments') || Request::is('admin/service/appointments') ) m-menu__item--submenu m-menu__item--open m-menu__item--expanded @endif" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('admin.Appointment')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item @if(Request::is('admin/services')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('services')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.AppointmentServices')}}
												</span>
											</a>
										</li>
                                        <li class="m-menu__item @if(Request::is('admin/staff')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('staff')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.staff')}}
												</span>
											</a>
										</li>
                                            <li class="m-menu__item @if(Request::is('admin/appointments')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('appointments')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.appointments')}}
												</span>
											</a>
										</li>
								
									</ul>
								</div>
							</li>
                            <li class="m-menu__item  m-menu__item--submenu  @if(Request::is('admin/posts') || Request::is('admin/categories') || Request::is('admin/tags') || Request::is('admin/posts/*')) m-menu__item--submenu m-menu__item--open m-menu__item--expanded @endif" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('admin.blogs')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
									
										 {{--<li class="m-menu__item @if(Request::is('admin/categories')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('categories')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.categories')}}
												</span>
											</a>
										</li>
                                       <li class="m-menu__item @if(Request::is('admin/tags')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('tags')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.tags')}}
												</span>
											</a>
										</li>--}}
                                          <li class="m-menu__item @if(Request::is('admin/posts')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('posts')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.posts')}}
												</span>
											</a>
										</li>
								
									</ul>
								</div>
							</li>
                               <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('admin.Installment')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
									
										<li class="m-menu__item" aria-haspopup="true" >
											<a  href="{{route('installments')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('admin.Installment')}}
												</span>
											</a>
										</li>
                                       
								
									</ul>
								</div>
							</li>
			</ul>
					</div>
					<!-- END: Aside Menu -->