@extends('admin.CarRental.index')
@section('content')
   <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('rental.bookings.bookings')}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile">
							<div class="m-portlet__body">
								<!--begin: Search Form -->
								<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
									<div class="row align-items-center">
										<div class="col-xl-8 order-2 order-xl-1">
											<div class="form-group m-form__group row align-items-center">
												
												<div class="col-md-4">
													<div class="m-input-icon m-input-icon--left">
														<input type="text" class="form-control m-input m-input--solid" placeholder="{{__('admin.searchTxt')}}" id="generalSearch">
														<span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
													</div>
												</div>
											</div>
										</div>
                                       
										
                                       
									</div>
								</div>
								<table class="m-datatable" id="html_table" width="100%">
									<thead>
										<tr>
											<th title="Field #1">
												{{__('rental.bookings.user name')}}
											</th>
											<th title="Field #2">
												{{__('rental.bookings.user email')}}
											</th>
											<th title="Field #3">
												{{__('rental.bookings.book start date')}}
											</th>
											<th title="Field #4">
												{{__('rental.bookings.book end date')}}
											</th>
											<th title="Field #5">
												{{__('rental.bookings.book created date')}}
											</th>
                                            <th title="Field #5">
												{{__('rental.bookings.book status')}}
											</th>
                                            
                                            <th title="Field #6">
												{{__('admin.options')}}
											</th>
                                       
										</tr>
									</thead>
									<tbody>
                                        @foreach($bookings as $book)
                                       <tr>
                                        <td>{{$book->userObj->name}}</td>
                                        <td>{{$book->userObj->email}}</td>
                                        <td>{{$book->request_date_from}}</td>
                                        <td>{{$book->request_date_to}}</td>
                                        <td>{{$book->created_at}}</td>
                                        <td>{{$book->delivery_status}}</td>
                                        <td>
                                            <a href="{{route('rental.bookings.view',$book->id)}}"><i class="la la-eye"></i></a>
                                          
                                           </td>
                                        </tr>
                                        @endforeach
									</tbody>
								</table>
		     
							</div>
						</div>
					</div>
@endsection
@section('footer')
 <script>
   var datatable = $('.m-datatable').mDatatable({
      data: {
        saveState: {cookie: false},
      },
      search: {
        input: $('#generalSearch'),
      },
      columns: [
        {
          field: 'Deposit Paid',
          type: 'number',
        },
        {
          field: 'Order Date',
          type: 'date',
          format: 'YYYY-MM-DD',
        },
      ],
    });
</script>
@endsection