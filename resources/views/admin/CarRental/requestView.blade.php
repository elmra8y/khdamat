@extends('admin.CarRental.index')
@section('content')
  <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('admin.roadhelp.viewRequest')}} - {{$rentalBookRequest->id}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile addproductPage">
							<div class="m-portlet__body">
					
										<div class="m-portlet__body">
		                
                                        
											<div class="row">
                                         
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.requestUserName')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$rentalBookRequest->userObj->name}}" disabled>
											</div>
                                                  <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.requestUserEmail')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$rentalBookRequest->userObj->email}}" disabled>
											</div>
                                                  <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.requestUserPhone')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$rentalBookRequest->userObj->phone}}" disabled>
											</div>
                                                   <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.requestStartDate')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$rentalBookRequest->request_date_from}}" disabled>
											</div>
                                                      <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.requestEndDate')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$rentalBookRequest->request_date_to}}" disabled>
											</div>
                                                      <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.request_datetime_start')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{\Carbon\Carbon::parse($rentalBookRequest->request_datetime_start)->format('Y-m-d h:i a')}}" disabled>
											</div>
                                                       <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.request_datetime_back')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{\Carbon\Carbon::parse($rentalBookRequest->request_datetime_back)->format('Y-m-d h:i a')}}" disabled>
											</div>
                                                        <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.carName')}}
												</label>
                                                           
												<input type="text" class="form-control m-input"  name="name_en" value="{{$rentalBookRequest->rentalCarObj->carObj->name}} -  {{$rentalBookRequest->rentalCarObj->carObj->countryObj->name}} - {{$rentalBookRequest->rentalCarObj->carObj->brandObj->name}} - {{$rentalBookRequest->rentalCarObj->carObj->categoryObj->name}} - {{$rentalBookRequest->rentalCarObj->year}} - {{$rentalBookRequest->rentalCarObj->colorObj->name}}" disabled>
											</div>
                                                @if($rentalBookRequest->delivery_address_lat && $rentalBookRequest->delivery_address_lng)
                                                        <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.map')}}
												</label>
                                                          
												<div id="map_rental"></div>
                                                            
                                                            <div class="form-group row">
                                                            <div class="col-lg-2">
                                                                <button style="margin-top: 20px;" class="btn btn-brand" data-clipboard="true" data-clipboard-target="#locationshare">
                                                                    <i class="la la-copy"></i>
                                                                    {{__('admin.rental.shareLocaltion')}}
                                                                </button>
                                                                </div>
                                                            <div class="col-lg-10">
                                                                 <input style="margin-top: 20px;"  type="text" class="form-control m-input" readonly value="https://maps.google.com/?q={{$rentalBookRequest->delivery_address_lat}},{{$rentalBookRequest->delivery_address_lng}}" id="locationshare">
                                                                </div>
                                                            </div>
                                               
                         
											</div>
                                                @endif
                                                     @if($rentalBookRequest->back_delivery_address_lat && $rentalBookRequest->back_delivery_address_lng)
                                                        <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.map_back')}}
												</label>
                                                          
												<div id="map_rental2"></div>
                                                            
                                                            <div class="form-group row">
                                                            <div class="col-lg-2">
                                                                <button style="margin-top: 20px;" class="btn btn-brand" data-clipboard="true" data-clipboard-target="#locationshare">
                                                                    <i class="la la-copy"></i>
                                                                    {{__('admin.rental.shareLocaltion')}}
                                                                </button>
                                                                </div>
                                                            <div class="col-lg-10">
                                                                 <input style="margin-top: 20px;"  type="text" class="form-control m-input" readonly value="https://maps.google.com/?q={{$rentalBookRequest->back_delivery_address_lat}},{{$rentalBookRequest->back_delivery_address_lng}}" id="locationshare">
                                                                </div>
                                                            </div>
                                               
                         
											</div>
                                                @endif
                                                @if($rentalBookRequest->delivery_address)
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.delivery_address')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$rentalBookRequest->delivery_address}}" disabled>
											</div>
                                                @endif
                                                 @if($rentalBookRequest->back_delivery_address)
                                                   <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.back_delivery_address')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$rentalBookRequest->back_delivery_address}}" disabled>
											</div>
                                                @endif
                                                    <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.days')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$rentalBookRequest->days}} / {{__('admin.days')}}" disabled>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
                                                    @if($rentalBookRequest->delivery_status == 'cash')
                                                    {{__('admin.rental.expectedPrice')}}
                                                    @elseif($rentalBookRequest->delivery_status == 'paid')
													{{__('admin.rental.PaidPrice')}}
                                                    @endif
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{$rentalBookRequest->days * $rentalBookRequest->rentalCarObj->price}} / {{__('front.KWD')}}" disabled>
											</div>
                                                @if($rentalBookRequest->delivery_status == 'cash' | $rentalBookRequest->delivery_status == 'paid' | $rentalBookRequest->delivery_status == 'inprogress')
                                                <div class="col-lg-12">
												<label for="name">
													{{__('admin.options')}}
												</label>
                                                         <div class="clear"></div>
                                                    @if($rentalBookRequest->delivery_status == 'cash' | $rentalBookRequest->delivery_status == 'paid')
                                                     <button type="button" class="btn btn-primary" id="changeRequestStatus">{{__('admin.rental.inprogress')}}</button>
                                                    @elseif($rentalBookRequest->delivery_status == 'inprogress' && \Carbon\Carbon::now() >= \Carbon\Carbon::parse($rentalBookRequest->request_date_to))
                                                     <button type="button" class="btn btn-primary" id="changeRequestStatus">{{__('admin.rental.done')}}</button>
                                                    @endif
               
                                                                
                                                         
                                                        
											</div>
                                                @endif
                                                 @if($rentalBookRequest->delivery_status == 'done')
                                                    <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.requeststatus')}}
												</label>
												<input type="text" class="form-control m-input"  name="name_en" value="{{__('admin.rental.RequestDone')}}" disabled>
											</div>
                                                @endif
                                            </div>
                                           
										</div>
										
									
                                
							</div>
						</div>
					</div>
@endsection
 
@section('footer')
<script>
  var ClipboardDemo = function () {
    
    //== Private functions
    var demos = function () {
        // basic example
        new Clipboard('[data-clipboard=true]').on('success', function(e) {
            e.clearSelection();
            alert('Copied!');
        });
    }

    return {
        // public functions
        init: function() {
            demos(); 
        }
    };
}();

jQuery(document).ready(function() {    
    ClipboardDemo.init();
});
    
         function initMap() {
  // The location of Uluru
  var uluru = {lat: parseFloat({!! $rentalBookRequest->delivery_address_lat !!}) , lng: parseFloat({!! $rentalBookRequest->delivery_address_lng !!})};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map_rental'), {zoom: 16, center: uluru});
              var marker = new google.maps.Marker({position: uluru, map: map , draggable: false});
             
              var uluru2 = {lat: parseFloat({!! $rentalBookRequest->back_delivery_address_lat !!}) , lng: parseFloat({!! $rentalBookRequest->back_delivery_address_lng !!})};
  // The map, centered at Uluru
  var map2 = new google.maps.Map(
      document.getElementById('map_rental2'), {zoom: 16, center: uluru2});
              var marker2 = new google.maps.Marker({position: uluru2, map: map2 , draggable: false});
}
       $('#changeRequestStatus').click(function (){
         $.ajax({
        url: "{!! route('rental.requestChangeStatus') !!}",
        type: 'post',
        dataType: 'json',
        data: {change : '{!! $rentalBookRequest->id !!}'},
        success: function(data) {
            if(data.status == 'done'){
                $('#changeRequestStatus').html(data.message);
            }
        
         }
         });
    });
       

</script>
<script defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('MAP')}}&callback=initMap">
    </script>
@endsection
