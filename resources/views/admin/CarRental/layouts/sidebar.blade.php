	<!-- BEGIN: Aside Menu -->
	<div 
		id="m_ver_menu" 
		class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
		data-menu-vertical="true"
		 data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
		>
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
								<a  href="" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												{{__('rental.dashboard')}}
											</span>
<!--
											<span class="m-menu__link-badge">
												<span class="m-badge m-badge--danger">
													2
												</span>
											</span>
-->
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									{{__('admin.Components')}}
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li class="m-menu__item  m-menu__item--submenu  @if(Request::is('admin/events') || Request::is('admin/events/*')) m-menu__item--submenu m-menu__item--open m-menu__item--expanded @endif" aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-layers"></i>
									<span class="m-menu__link-text">
										{{__('rental.rentalpart')}}
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item @if(Request::is('admin/events')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('rental.cars.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('rental.cars')}}
												</span>
											</a>
										</li>
                                        <li class="m-menu__item @if(Request::is('admin/events')) m-menu__item--active @endif" aria-haspopup="true" >
											<a  href="{{route('rental.bookings.show')}}" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													{{__('rental.bookings.bookings')}}
												</span>
											</a>
										</li>
                                        
									</ul>
								</div>
							</li>
                        
			</ul>
					</div>
					<!-- END: Aside Menu -->