@extends('admin.CarRental.index')
@section('content')
  <!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">
										{{__('rental.editcar')}} - {{$singlecar->carObj->name}}
								</h3>
							
							</div>
						
						</div>
					</div>
					<!-- END: Subheader -->
					<div class="m-content">
					
						<div class="m-portlet m-portlet--mobile addproductPage">
							<div class="m-portlet__body">
								<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('rental.cars.update',$singlecar->id)}}" id="addCarForm">
										<div class="m-portlet__body">
		                                   {{csrf_field()}}
                                   @if($errors->any())
                        <div class="alert alert-danger danger-errors">
                          @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                                @endforeach
                        </div>
                        @endif
                        @if(session()->has('message'))
                         <div class="alert alert-success danger-errors">
                          <p>{{session()->get('message')}}</p>
                        </div>
                        @endif
                        @if(session()->has('errorcar'))
                         <div class="alert alert-danger danger-errors">
                          <p>{{session()->get('errorcar')}}</p>
                        </div>
                        @endif
                                        
											<div class="row">
                                                <div class="col-lg-12">
												<label for="name">
													{{__('rental.car')}}
												</label>
                                                
												<select class="form-control m-select2" id="car_select" name="car">
                                                    @php
                                                    $cars = \App\Models\cars::all();
                                                    @endphp
                                                    <option value="">{{__('rental.cararr.choose car')}}</option>
												   @foreach($cars as $car)
                                                        <option value="{{$car->id}}" @if($singlecar->car == $car->id) selected @endif>
                                                           {{$car->name}}
                                                        </option>
                                                    @endforeach
											</select>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('rental.day price')}}
												</label>
												<input type="number" step=".01" class="form-control m-input" name="price" value="{{$singlecar->price}}">
											</div>
                                                            <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.car.car_qty')}}
												</label>
												<input type="number" step="1" class="form-control m-input" name="num" value="{{$singlecar->price}}">
											</div>
                                                     <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.car.year')}}
												</label>
												<select class="form-control m-select2" id="year_select" name="year">
												  <option>{{__('admin.rental.car.choose_year')}}</option>
                                                         @foreach(range(1975,\Carbon\Carbon::now()->format('Y')) as $year)
                                                        <option value="{{$year}}" @if($singlecar->year == $year) selected @endif>
                                                           {{$year}}
                                                        </option>
                                                    @endforeach
											</select>
											</div>
                                                 <div class="col-lg-12">
												<label for="name">
													{{__('admin.rental.car.color')}}
												</label>
												<select class="form-control m-select2" id="color_select" name="color">
                                                    @php
                                                     $colors = \App\Models\carsColors::all();
                                                    @endphp
												  <option>{{__('admin.rental.car.choose_color')}}</option>
                                                         @foreach($colors as $color)
                                                        <option value="{{$color->id}}" @if($singlecar->color == $color->id) selected @endif>
                                                           {{$color->name}}
                                                        </option>
                                                    @endforeach
											</select>
											</div>
                                                    <div class="col-lg-12">
												<label for="name">
													{{__('rental.status')}}
												</label>
                                                        <div class="clear"></div>
											<span class="m-switch m-switch--primary">
																		<label>
																			<input type="checkbox" @if($singlecar->status == 'available') checked="checked" @endif name="status">
																			<span></span>
																		</label>
																	</span>
											</div>
                                                <div class="col-lg-12">
												<label for="email">
													{{__('rental.small desc')}}
												</label>
                                                        <textarea name="content" id="content"     
                                                    class="summernote">{{$singlecar->description}}</textarea>
                                                                </div>
                                                
                                            </div>
                                           
										</div>
										<div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions">
												<button type="submit" class="btn btn-primary">
													{{__('admin.save')}}
												</button>
												<button type="reset" class="btn btn-secondary">
													{{__('admin.cancel')}}
												</button>
											</div>
										</div>
									</form>
							</div>
						</div>
					</div>
@endsection
@section('footer')
<script>
  $('#car_select,#year_select,#color_select').select2();
    $('.summernote').summernote({
            height: 150, 
               fontNames: ['Cairo']
        });
</script>
@endsection