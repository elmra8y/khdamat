<?php

return [
    'signup' => 'انشاء حساب',
    'name' => 'الاسم',
    'phone' => 'الهاتف',
    'email' => 'البريد الالكتروني',
    'Register' => 'انشاء',
    'password' => 'كلمة المرور',
    'accountCreated' => 'تم انشاء الحساب بنجاح . :signNow',
    'loginNow' => 'يمكنك تسجيل الدخول الان .',
    'login' => 'تسجيل الدخول',
    'passwordNotcorrect' => 'كلمة المرور غير صحيحة .',
    'pleaseLoginFirst' => 'من فضلك قم بتسجيل الدخول اولا .',
    'accountUpdated' => 'تم تغيير البيانات بنجاح .',
    'currentPassword' => 'كلمة المرور الحالية',
    'newPassword' => 'كلمة المرور الجديدة',
    'repeatNewPassword' => 'تاكيد كلمة المرور الجديدة',
    'save' => 'حفظ',
    'cancel' => 'الغاء',
    'newpasswordnotsame' => 'كلمة المرور الجديدة غير مطابقة للتأكيد .',
    'currentpasswordmin' => 'كلمة المرور الحالية يجب الا تقل عن 8 احرف .',
    'newpasswordmin' => 'كلمة المرور الجديدة يجب الا تقل عن 8 احرف .',
    'confirmpasswordmin' => 'كلمة المرور التأكيدية يجب الا تقل عن 8 احرف .',
    '' => '',
    '' => '',
    '' => '',
   
];
   